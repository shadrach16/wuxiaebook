# # pull official base image
# FROM python:3.8.0-alpine

# # set work directory
# WORKDIR /usr/src/wuxiaEbook

# # set environment variables
# ENV PYTHONDONTWRITEBYTECODE 1
# ENV PYTHONUNBUFFERED 1

# # install dependencies
# RUN pip install --upgrade pip
# COPY ./requirements.txt /usr/src/wuxiaEbook/requirements.txt
# RUN pip install -r requirements.txt

# # copy project
# COPY . /usr/src/wuxiaEbook/

FROM python:3.6.9-alpine
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apk update \
    && apk add postgresql-dev gcc python3-dev musl-dev
RUN mkdir /code
WORKDIR /code
RUN pip install --upgrade pip
COPY requirements.txt /code/
RUN pip3 install -r requirements.txt



# copy entrypoint.sh
COPY ./entrypoint.sh /usr/src/wuxiaEbook/entrypoint.sh

# copy project

COPY . /code/

# run entrypoint.sh
ENTRYPOINT ["/usr/src/wuxiaEbook/entrypoint.sh"]


