from django.urls import path
from .views import mainView, ebookView, searchView
# , filterView, searchView




urlpatterns = [
path('', mainView.as_view(), name='main'),
# path('filter/<str:filter>/', filterView.as_view(), name='filter'),
path('search/', searchView.as_view(), name='search'),
path('filterView/<str:filter>/', searchView.as_view(), name='filter'),
path('view/<int:pk>/', ebookView.as_view(), name='view'),

]