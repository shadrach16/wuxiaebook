from django.db import models

# Create your models here.


class ebook(models.Model):
    
    pdf = models.FileField(upload_to='pdf')
    mobi = models.FileField(upload_to='mobi', null=True)
    epub = models.FileField(upload_to='epub', null=True)
    image = models.FileField(upload_to='image', null=True)
    title = models.CharField(max_length = 35)
    description = models.TextField(blank=True)
    genre = models.CharField(max_length=20)
    genretype = models.TextField(blank=True)
    date = models.DateTimeField()
    views = models.IntegerField(default = 0)
    last_chapter = models.TextField(null=True,blank=True)
	
    def __str__(self):
        return "Title: {}, Created: {}".format(self.title,self.date)