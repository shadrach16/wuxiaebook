import requests
from bs4 import BeautifulSoup as bs
import argparse
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import inch
from reportlab.lib.colors import black, green
from main.models import ebook
from django.core.exceptions  import ObjectDoesNotExist
import os
from PIL import Image
from urllib.request import urlretrieve
from datetime import datetime


from reportlab.platypus import SimpleDocTemplate, Spacer, PageBreak,CondPageBreak
from reportlab.platypus.tableofcontents import TableOfContents
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch

from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER
PAGE_HEIGHT=defaultPageSize[1]; PAGE_WIDTH=defaultPageSize[0]

from reportlab.lib.styles import ParagraphStyle as PS

from reportlab.platypus.paragraph import Paragraph
from reportlab.platypus.doctemplate import PageTemplate, BaseDocTemplate

from reportlab.platypus.frames import Frame
from reportlab.lib.units import cm
import hashlib



def handle_documentbegin(canvas, pdf):
	canvas.saveState()


	canvas.setAuthor(str(pdf.author).title())
	canvas.setTitle(pdf.Title.title())		
	canvas.setFont('Times-Bold',16)
	canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-60, pdf.Title)
	# 
	if pdf.image:
		canvas.drawImage(pdf.image, inch,inch,width=6*inch, height=9*inch, mask=None)
	# canvas.setFont('Times-Bold',14)
	# canvas.drawString(self.PAGE_WIDTH/2.0, self.PAGE_HEIGHT-90, self.info)
	canvas.setFont('Times-Bold',10)
	canvas.drawString(inch, 0.75 * inch, "First Page / %s" % pdf.pageinfo)
	canvas.restoreState()



class MyDocTemplate(SimpleDocTemplate):
	def __init__(self, filename, **kw):
		
		self.allowSplitting = 0
		self.mypdf = kw['mypdf']
		SimpleDocTemplate.__init__(self, filename, **kw); self.f=None
		template = PageTemplate('normal', [Frame(2.5*cm, 2.5*cm, 15*cm, 25*cm, id='F1')])
		self.addPageTemplates(template)

	def handle_documentBegin(self):
		print('begin-pdf..........', self.mypdf)
		handle_documentbegin(self.canv, pdf = self.mypdf)
		BaseDocTemplate.handle_documentBegin(self)

	def afterFlowable(self, flowable):
		self.f=flowable
		"Registers TOC entries."
		
		if isinstance(flowable, Paragraph):
			txt = flowable.getPlainText()
			style = flowable.style.name
			
			if style == 'Heading1':
                # ...
				key = 'h1-%s' % self.seq.nextf('heading1')
				self.canv.bookmarkPage(key)
				self.canv.outlinePage(key)
				F =flowable.getSpaceAfter(self)
				print('flowable ',F)
				self.notify('TOCEntry', (0, txt, self.page))

			elif style == 'Heading2':
				# ...
				key = 'h2-%s' % self.seq.nextf('heading2')
				bn=getattr(flowable,'_bookmarkName', None)
				e=[1, txt, self.page]
				if bn is not None:
					e.append(bn)
					print('bnn',bn)

				self.canv.bookmarkPage(key)
				self.canv.addOutlineEntry(txt,key, 0, 0)
				F =flowable.getSpaceAfter()
				print('flowable ',F)
				self.notify('TOCEntry', tuple(e))

	






class saveToPdf():

	def __init__(self, text = '', chaptername='',author='', title = "Hello world",pageinfo = "", image='',info=""):

		self.PAGE_HEIGHT=defaultPageSize[1]
		self.PAGE_WIDTH=defaultPageSize[0]
		self.styles = getSampleStyleSheet()
		self.Title =title
		self.pageinfo = pageinfo if pageinfo else "Download Ebook(s) at (www.wuxiaebooks.com)"
		self.bogustext= text if text else ("This is Paragraph numbers.") *200
		self.chapter= 0
		self.chaptername= chaptername if chaptername else 'chapter'+str(self.chapter)
		self.image= image
		self.info= info
		self.doc = ''
		self.Story = []
		self.toc = ''
		self.author = author

	def myFirstPage(self,canvas, doc):
		canvas.saveState()
		canvas.setAuthor("Oluwamo Shadrach")
		canvas.setTitle(self.Title.title())		
		canvas.setFont('Times-Bold',16)
		canvas.drawCentredString(self.PAGE_WIDTH/2.0, self.PAGE_HEIGHT-60, self.Title)
        # 
		if self.image:
			canvas.drawImage(self.image, inch,inch,width=6*inch, height=9*inch, mask=None)
        # canvas.setFont('Times-Bold',14)
        # canvas.drawString(self.PAGE_WIDTH/2.0, self.PAGE_HEIGHT-90, self.info)
		canvas.setFont('Times-Roman',11)
		canvas.drawString(inch, 0.75 * inch, "First Page / %s" % self.pageinfo)
		canvas.restoreState()

	def myLaterPages(self, canvas, doc):

		if doc.page == 1:
			canvas.saveState()
			canvas.setAuthor("Oluwamo Shadrach")
			canvas.setTitle(self.Title.title())		
			canvas.setFont('Times-Bold',16)
			canvas.drawCentredString(self.PAGE_WIDTH/2.0, self.PAGE_HEIGHT-60, self.Title)
			# 
			if self.image:
				canvas.drawImage(self.image, inch,inch,width=6*inch, height=9*inch, mask=None)
			# canvas.setFont('Times-Bold',14)
			# canvas.drawString(self.PAGE_WIDTH/2.0, self.PAGE_HEIGHT-90, self.info)
			canvas.setFont('Times-Roman',11)
			canvas.drawString(inch, 0.75 * inch, "First Page / %s" % self.pageinfo)
			canvas.restoreState()
		else:
			canvas.saveState()
			canvas.setFont('Times-Roman',11)
			canvas.drawString(inch, 0.75 * inch, "Page %d %s" % (doc.page, self.pageinfo))
			canvas.restoreState()

	def add_paras(self, Story, toc, text, chaptername=''):
		pass
	
	def add_para(self, text, chaptername=''):
		self.chaptername= chaptername
		
		self.bogustext = text.replace('-',' ')
		PS1 =PS(fontName='Times-Bold', alignment=TA_JUSTIFY, fontSize=16, name='Heading',leftIndent=0, firstLineIndent=0, spaceBefore=15, leading=36)
		PS2 =PS(fontName='Times-Bold', alignment=TA_JUSTIFY, fontSize=18, name='Heading2',leftIndent=0, firstLineIndent=0, spaceAfter=0, leading=26)
		BODY =PS(fontName='Times-Roman', alignment=TA_JUSTIFY, fontSize=14, name='Body1',leftIndent=0, firstLineIndent=30, spaceBefore=5, leading=26)

    
		# self.Story.append(PageBreak())
		# toc.addEntry()
		
		
		# self.Story.append(PageBreak())
		# print(style2)
		if self.chaptername:
			bn=hashlib.sha1(bytes(chaptername+PS2.name,encoding='utf-8')).hexdigest()
			h = Paragraph(chaptername +'<a name="%s"/>'%bn, PS2)
			h._bookmarkName=bn
			self.Story.append(h)

			chapter = Paragraph(self.chaptername, PS2)
			# self.Story.append(chapter)
			p = Paragraph(str(self.bogustext), BODY)
		
			self.Story.append(p)

		else:
			p = Paragraph(str(self.bogustext), PS1)
			self.Story.append(p)

		
		self.Story.append(Spacer(1,0.2*inch))
		self.Story.append(CondPageBreak(600))
		

	def create(self,mypdf):
		
		self.doc = MyDocTemplate(str(self.Title.replace(' ', '_')+'.pdf'), mypdf=mypdf)
		print('doc-pdf..........', mypdf)
		self.Story = []  
		# Spacer(1,2*inch)
		self.toc = TableOfContents()
		PS1 =PS(fontName='Times-Bold', alignment=TA_JUSTIFY, fontSize=14, name='Heading1',leftIndent=0, firstLineIndent=30, spaceBefore=5, leading=26)
		PS2 =PS(fontName='Times-Bold', alignment=TA_JUSTIFY, fontSize=14, name='Heading2',leftIndent=0, firstLineIndent=0, spaceAfter=15, leading=26)
		# BODY =PS(fontName='Times-Roman', alignment=TA_JUSTIFY, fontSize=14, name='Body1',leftIndent=0, firstLineIndent=30, spaceBefore=5, leading=26)

		self.toc.levelStyles = [PS1, PS2 ]
		self.Story.append(PageBreak())
		self.Story.append(self.toc)
		self.Story.append(PageBreak())

		

	def saved(self):
		self.doc.multiBuild(self.Story, onFirstPage = self.myFirstPage, onLaterPages = self.myLaterPages, canvasmaker=canvas.Canvas)


class wuxiaEbook():
	def __init__(self, name='',host_url = 'https://www.wuxiaworld.co' ):
		self.name = name
		self.search="Wuxia"
		self.tags = None
		self.allchapterurl=''
		self.site=''
		self.chapter=0
		self.chapterlist=[]
		self.chaptersurl=''
		self.nextchapter=''
		self.ebookinfourl=None
		self.ebookinfo={}
		self.host_url=host_url
		self.author=''
		"""
		sourcelist is a list of source url containing 
		dict of chapterlist url and each chapter url with tuple tags
		of chapterlist and each chapter
		"""
		# self.sourceList = [{'siteurl':('http://127.0.0.1:8000/view/{}'.format(self.name.replace(" ","-").title()),
		# 'http://127.0.0.1:8000/filterView/{}'.format(self.search.replace(" ","-").title()),'http://127.0.0.1:8000/view/{}'.format(self.chapter),{
		# 	'chapterlist':'a,chap_link', 'info':'info,genre,descr,genreType,title', 'content':'div,descr'})},
	
		# ]

		self.sourceList = [
		{'siteurl':('https://www.wuxiaworld.co/{}/'.format(self.name.replace(" ","-")),
		'https://www.wuxiaworld.co/{}/'.format(self.name.replace(" ","-")),'https://www.wuxiaworld.co/{}/{}',{
			'chapterlist':'dd,a', 'info':'fmimg,info,intro', 'content':'div,content'})},
		]



		self.current=-1
		self.currentSource = None

	def getSource(self):

		self.current+=1
		self.currentSource = self.sourceList[self.current]
		return self.currentSource



	def absoluteUrl(self):
		

		if self.current > len(self.sourceList):
			raise AttributeError('End of source list')

		self.site = self.getSource()

		absurl, allchapterurl, chaptersurl, searchtags = self.site['siteurl']
		self.tags = searchtags
		self.allchapterurl = allchapterurl
		self.chaptersurl = chaptersurl
		self.ebookinfourl = absurl
		
		print('site: ', self.ebookinfourl)

		return absurl

	def next_url(self):
		self.nextchapter = self.chapterlist[self.chapter]['url']
		self.chapter+=1
		print('chapter: ', self.nextchapter)
		return self.nextchapter


	def allchapters(self):
		print('allchapter url: ', self.allchapterurl)
		r = requests.get(self.allchapterurl)
		try:

			if r.status_code == 200:
				chap = bs(r.content, 'html.parser')
				
				tag = self.tags['chapterlist'].replace(" ", "").split(',')
				print('tags: ', tag)
				f= chap.find(id='list')
				d = f.find_all("dd")
				print('There is chapter content\n',f)
				chapters=None
				# for chp in d:
				# 	chp.find_all('a', class_='chap_link')[1]['href'])

				# 	chapters .find_all('{}'.format(tag[0]),class_='{}'.format(tag[1]))
				data =[]

				for x in d:
					con = x.find('{}'.format(tag[1]))
				
					c = con['href']
					data.append({'url':self.chaptersurl.format(self.name.replace(" ","-"), c),
					'chapter-name':con.get_text(strip=True)})


					print('chapter: ', con, ' -',c, '\n')

				self.chapterlist = data
				# map(lambda x: str(self.host_url+x), data)
				# print('chapter link :', self.chapterlist, '\n')
				return self.chapterlist
			
			else:
				print('No chapter found/n')
				print('Breaking code\n')
				# self.allchapters()
		except:
			raise AttributeError('ChapterError')
	
	# def saveToPdf(self,c, string, chaptername='', fontsize=12, title="", image=None, save=True):
	# 	from textwrap import wrap

	# 	width, height = A4
	# 	strings =  "\n".join(wrap(string, 80)).replace('-',' ').replace("<br/>","\n").replace('<div id="content">', "\t").replace('<script>', ' ').replace('ChapterMid();', ' ').replace("</script>", " ")





	
	def saveToEpub(self):
		pass

	def saveToMobi(self):
		pass
	

	def modelAvailable(self, title):

		try:
			model=ebook.objects.filter(title__icontains=title)
			print("\t******** Found in model***********\n \t\tUpdating Data")
			return False
		except ObjectDoesNotExist:
			return False

	def saveToModel(self, data):
		pdf = data['pdf']
		epub=data['epub']
		image =data['image']
		title = data['title']
		mobi = data['mobi']
		descr = data['description']
		genre = data['genre']
		genretype = data['author']
		last_chapter = data['last_chapter']

		model=ebook(pdf=pdf,image = image, title=title,
		description = descr,genre = genre, genretype = genretype,
		last_chapter = last_chapter, date=datetime.now())

		if epub:
			model.epub = epub
		if mobi:
			model.mobi = mobi
		
		model.save()
	
	def get_file_url(self, format):
		
		# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
		# BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

		# file_url= os.path.isfile(os.path.join(BASE_DIR, 
		# "{}.{}".format(self.ebookinfo['title'], format)))
		if format == 'pdf':
			return "{}.{}".format(self.ebookinfo['title'].replace(" ","_"), format)
		else:
			return None
		if format == 'epub':
			return "{}.{}".format(self.ebookinfo['title'].replace(" ","_"), format)
		else:
			return None
		if format == 'mobi':
			return "{}.{}".format(self.ebookinfo['title'].replace(" ","_"), format)
		else:
			return None
		

	def get_image_url(self, img_url, locate=False):
		BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
		location = os.path.join(BASE_DIR, "image/")

		image_dir = 'image/'+self.ebookinfo['title'].replace(" ","_")+'.png'


		image, headers = urlretrieve('{}{}'.format(self.host_url,img_url), location+self.ebookinfo['title'].replace(" ","_")+'.png')
		if image:
			print('isimage: ',image)

			if locate:
				print('image url', str(location+self.ebookinfo['title'].replace(" ","_")+'.png'))
				return location+self.ebookinfo['title'].replace(" ","_")+'.png'
			else:
				return image_dir
	
		else:
		  return img_url

	def createData(self):
		data = {}
		data['pdf']=self.get_file_url('pdf')
		data['epub']=self.get_file_url('epub')
		data['mobi']=self.get_file_url('mobi')
		data['image']=self.get_image_url(self.ebookinfo['image'])
		data['title']= self.ebookinfo['title'] if self.ebookinfo['title']  else ''
		data['description']= self.ebookinfo['descr'] if self.ebookinfo['descr'] else ''
		data['genre']= self.ebookinfo['genre'] if self.ebookinfo['genre'] else ''
		data['author']= self.ebookinfo['author'] if self.ebookinfo['author'] else ''
		data['last_chapter']= self.ebookinfo['last_chapter'] if self.ebookinfo['last_chapter'] else ''

		return data

		

		

	
	def get_info(self):
		self.absoluteUrl()

		
		info =requests.get(self.ebookinfourl)

		if info.status_code == 200:
			chap = bs(info.content, 'html.parser')
			tag = self.tags['info'].replace(" ","").split(',')
			u = chap.body.find(id="maininfo")
			genre = chap.body.find(class_="con_top")
			print('tags-data \n', tag)
			
			for x in tag:

				data = u.find(id=str(x))
				if str(x) == 'info':
					children = data.children
					con = []

					for x in children:

						con.append(x)

					title = con[1].get_text(strip = True)
					author=con[3].get_text(strip = True).replace('Author：',"")
					genre = genre.get_text(strip=True).split('>')[1]
					last_chapter = con[9].find('a').get_text(strip = True)

					

				

					self.ebookinfo['title'] = title
					self.ebookinfo['author'] = author
					self.ebookinfo['last_chapter'] = last_chapter
					self.ebookinfo['genre'] = genre

					# genre = https://www.google.com/search?q=Lord+of+the+Mysteries+genre
					
					# print(x,'-: ', data.attrs['src'], '\n')

				elif str(x) == 'intro':
					print('introduction')
					descr = data.get_text()
					self.ebookinfo['descr'] = descr if descr else ''
					print('>>>>>>>>>>>>>>>>>>ebookinfo[descr]:<<<<<<<<<<<<<<<<<<\n ', self.ebookinfo['descr'])

				elif str(x) == 'fmimg':
					data =  chap.body.find(id=x)
					image = data.img
					self.ebookinfo['image'] = image.attrs['src']
					
					# print(x,'-: ', data.get_text(strip=True), '\n')

			return self.ebookinfo

		else:
			
			return None
			
		
	



	def run(self):
		print("***********************************************************\n")
		print('\t\tGetting: ', self.name, '\n')
		print("***********************************************************\n")
		
		
		
		

		if not self.modelAvailable(self.name.replace("-"," ")):
			info = self.get_info()
			
			if info:
				datainfo=''
				for x in self.ebookinfo:
					if x:
						datainfo += "{}: {}<br/>".format(str(x).title(), self.ebookinfo[x])

				image_url =self.get_image_url(self.ebookinfo['image'],locate=True)

				pdf = saveToPdf(title="{}".format(self.ebookinfo['title'].replace(" ","_")),image = image_url,  author=self.ebookinfo['author'])
				
				pdf.create(pdf)
				
				
				

				# self.ebookinfo['image'] = None
				pdf.add_para(datainfo)
	
				chapter= self.allchapters()
				
				
				while self.chapter < len(chapter):
					print('length: ', len(chapter))

					chaptername = self.chapterlist[self.chapter]['chapter-name']
					url= self.next_url()
					
					print('\tPulling chapter  content from next url={} \n'.format(url))
					get_url = requests.get(url)

					if get_url.status_code == 200:

						get_page = bs(get_url.content, 'html.parser')
						# tag = self.tags['content'].split(',')
						get_content = get_page.body.find(id="content")
						print('Main section data scribe \n')
						print('getting Chapter: ', self.chapter)
						
						# get_content = u.find(class_='{}'.format(tag[1]))

						# self.saveToPdf(get_content)
						content = get_content.prettify(formatter="minimal")
					
						pdf.add_para(content, chaptername=chaptername)

					else:
						print("No content found")

				
				print("******Breaking out of chapter loop*******\n")
				pdf.saved()
				print("no more chapter\n")
				print('***********saving to model***********\n')
				self.saveToModel(self.createData())
				print('Data saved\n')
			else:
				print("Ebook: {} not found\n".format(self.name))



if __name__=='__main__':
	parser = argparse.ArgumentParser(description='Process Manga name')
	parser.add_argument('manga_name', nargs='?') 
	getarg = parser.parse_args()
	name = vars(getarg).get('manga_name')
	start = wuxiaEbook(name)
	start.run()


#  WuxiaMiner