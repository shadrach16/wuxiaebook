# Generated by Django 3.0.5 on 2020-05-05 03:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20200505_0234'),
    ]

    operations = [
        migrations.AddField(
            model_name='ebook',
            name='last_chapter',
            field=models.IntegerField(default=0, null=True),
        ),
    ]
