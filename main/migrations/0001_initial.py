# Generated by Django 3.0.5 on 2020-05-05 00:25

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='ebook',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pdf', models.FileField(upload_to='pdf')),
                ('epub', models.FileField(upload_to='epub')),
                ('image', models.ImageField(upload_to='image')),
                ('title', models.CharField(max_length=15)),
                ('description', models.TextField(blank=True)),
                ('genre', models.CharField(max_length=15)),
                ('genretype', models.TextField(blank=True)),
                ('date', models.DateTimeField()),
                ('views', models.IntegerField(default=0)),
            ],
        ),
    ]
