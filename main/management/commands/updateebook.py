from django.core.management.base import BaseCommand, CommandError

from main.models import ebook
from main.wuxiaEbook import wuxiaEbook

from datetime import datetime as date
import requests as r
from bs4 import BeautifulSoup as bs


def getEbookList(url=''):

    get_content = []

    if url:
        

        # try:
        get_url= r.get(url)
        
        if get_url.status_code == 200:
            get_page = bs(get_url.content, 'html.parser')
            u = get_page.body.find_all(class_='novellist3')

            for x in u:
                cont = [a.get_text(strip=True) for a in x.find_all('a')]
                get_content+= cont
                print(cont,'\n')
        # except:
        #     print("No Connection or network\n")
            



        


    return get_content








class Command(BaseCommand):
    help = "This is to pass ebook update"

    def add_arguments(self, parser):
        parser.add_argument('ebook-name', nargs='*', type=str)

    def handle(self, *arg, **options):
        if options['ebook-name']:
            for x in options['ebook-name']:

                start = wuxiaEbook(str(x))
                start.run()
        else:
            for x in getEbookList('https://www.wuxiaworld.co/all/'):
                start = wuxiaEbook(str(x))
                start.run()
                
        self.stdout.write(self.style.SUCCESS("All Wuxia Ebook Found has been successfully created and saved"))