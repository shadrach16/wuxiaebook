
from reportlab.platypus import SimpleDocTemplate, Spacer, PageBreak,CondPageBreak
from reportlab.platypus.tableofcontents import TableOfContents
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch
from  reportlab.pdfgen import canvas
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER
PAGE_HEIGHT=defaultPageSize[1]; PAGE_WIDTH=defaultPageSize[0]

from reportlab.lib.styles import ParagraphStyle as PS

from reportlab.platypus.paragraph import Paragraph
from reportlab.platypus.doctemplate import PageTemplate, BaseDocTemplate

from reportlab.platypus.frames import Frame
from reportlab.lib.units import cm



class MyDocTemplate(SimpleDocTemplate):
    def __init__(self, filename, **kw):
        self.allowSplitting = 0
        SimpleDocTemplate.__init__(self, filename, **kw)
        template = PageTemplate('normal', [Frame(2.5*cm, 2.5*cm, 15*cm, 25*cm, id='F1')])
        self.addPageTemplates(template); self.f=None

    def handle_documentBegin(self):
        myFirstPage(self.canv, self.f)
        BaseDocTemplate.handle_documentBegin(self)
        # doMoreStuff()
    def afterFlowable(self, flowable):
        self.f=flowable
        "Registers TOC entries."

        if isinstance(flowable, Paragraph):
            txt = flowable.getPlainText()
            style = flowable.style.name

            if style == 'Heading1':
                # ...
                key = 'h1-%s' % self.seq.nextf('heading1')
                self.canv.bookmarkPage(key)
                self.canv.outlinePage(key)
                F =flowable.getSpaceAfter(self)
                print('flowable ',F)
                self.notify('TOCEntry', (0, txt, self.page))

            elif style == 'Heading2':
                # ...
                key = 'h2-%s' % self.seq.nextf('heading2')
                bn=getattr(flowable,'_bookmarkName', None)
                e=[1, txt, self.page]
                if bn is not None:
                    e.append(bn)
                    print('bnn',bn)

                self.canv.bookmarkPage(key)
                self.canv.addOutlineEntry(txt,key, 0, 0)
                F =flowable.getSpaceAfter()
                print('flowable ',F)
                self.notify('TOCEntry', tuple(e))

    




styles = getSampleStyleSheet()



Title = "Hello world"
pageinfo = "platypus example"

def myFirstPage(canvas, doc):
    canvas.saveState()

    canvas.setAuthor("Oluwamo Shadrach")
    canvas.setTitle(Title.title())		
    canvas.setFont('Times-Bold',16)  
    canvas.drawCentredString(PAGE_WIDTH/2.0, PAGE_HEIGHT-58, Title)
    canvas.setFont('Times-Roman',9)
    canvas.drawString(inch, 0.75 * inch, "First Page / %s" % pageinfo)
    canvas.restoreState()

def myLaterPages(canvas, doc):
    canvas.saveState()
    canvas.setFont('Times-Roman',9)
    canvas.drawString(inch, 0.75 * inch, "Page %d %s" % (doc.page, pageinfo))
    canvas.restoreState()


def go():
    doc = SimpleDocTemplate("phello.pdf")
    Story = [Spacer(1,2*inch)]
    style = styles["Normal"]
    styleH = styles['Heading1']
    PS1 =PS(fontName='Times-Bold', alignment=TA_JUSTIFY, fontSize=14, name='Heading1',leftIndent=0, firstLineIndent=30, spaceBefore=5, leading=26)
    PS2 =PS(fontName='Times-Bold', alignment=TA_JUSTIFY, fontSize=14, name='Heading2',leftIndent=0, firstLineIndent=0, spaceAfter=15, leading=26)
    BODY =PS(fontName='Times-Roman', alignment=TA_JUSTIFY, fontSize=14, name='Body1',leftIndent=0, firstLineIndent=30, spaceBefore=5, leading=26)

    
    
    toc = TableOfContents()
  
    toc.levelStyles = [PS1, PS2 ]
    Story.append(PageBreak())
    Story.append(toc)
    # toc.addEntry()
    # chapter = Paragraph("Chapter "+str(2), PS2)
    # toc.addEntry(1, chapter,1)
    
    
    Story.append(PageBreak())
    # print(style2)
    import hashlib
    for i in range(20):
        bogustext = ("This is Paragraph number  %s." % i) *200
        text= "Chapter "+str(i)
        bn=hashlib.sha1(bytes(text+PS2.name,encoding='utf-8')).hexdigest()
       
        h = Paragraph(text +'<a name="%s"/>'%bn, PS2)
        # print('hhh',h)

        h._bookmarkName=bn
        Story.append(h)
        chapter= Paragraph(text, PS2)

        # toc.addEntry(1, "chapter "+str(i),i,key=1)
        
        p = Paragraph(bogustext, BODY)
        # Story.append(Spacer(1,0.2*inch))
        # Story.append(chapter)
        Story.append(p)
        Story.append(Spacer(1,0.2*inch))
        Story.append(CondPageBreak(600))
    
    # doc.notify('TOCEntry', (0, 'txt', 1))


    doc = MyDocTemplate('phello.pdf')

    # c= canvas.
    doc.multiBuild(Story, onFirstPage=myFirstPage, onLaterPages=myLaterPages, canvasmaker=canvas.Canvas)






# h1 = PS(name = 'Heading1',
# fontSize = 14,
# leading = 16)
# h2 = PS(name = 'Heading2',
# fontSize = 12,
# leading = 14,
# leftIndent = 15)
# # Build story.
# story = []
# toc = TableOfContents()
# # For conciseness we use the same styles for headings and TOC entries
# toc.levelStyles = [h1, h2]
# story.append(toc)
# story.append(PageBreak())
# story.append(Paragraph('First heading', h1))
# story.append(Paragraph('Text in first heading', PS('body')))
# story.append(Paragraph('First sub heading', h2))
# story.append(Paragraph('Text in first sub heading', PS('body')))
# story.append(PageBreak())
# story.append(Paragraph('Second sub heading', h2))
# story.append(Paragraph('Text in second sub heading', PS('body')))



# story.append(Paragraph('Last heading', h1))









if __name__=='__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Process Manga name')
    parser.add_argument('manga_name', nargs='?') 
    getarg = parser.parse_args()
    name = vars(getarg).get('manga_name')
    go()
    # title = 'Gambit_of_the_Living_Weapon'
    # c= canvas.Canvas("{}.pdf".format(title.replace(" ","_")), pagesize=A4)
    # data=""
    # image ='/image/Debian_Tribe_2.png'
    # image_url =get_image_url(image)
    # saveToPdf(c, data, fontsize=15, title = title, image = image_url)
    # c.save()