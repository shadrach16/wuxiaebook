
import requests
from bs4 import BeautifulSoup as bs
import argparse
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import inch
from reportlab.lib.colors import black, green
# from main.models import ebook
# from django.core.exceptions  import ObjectDoesNotExist
import os
from PIL import Image
from urllib.request import urlretrieve
from datetime import datetime

from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, PageBreak
from reportlab.platypus.tableofcontents import TableOfContents
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.rl_config import defaultPageSize
from reportlab.lib.units import inch
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER


html = """ 
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0048)https://www.wuxiaworld.co/Lord-of-the-Mysteries/ -->
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><script type="text/javascript" src="./Lord of the Mysteries - Index - Wuxiaworld_files/1.txt"></script><script type="text/javascript" src="./Lord of the Mysteries - Index - Wuxiaworld_files/1(1).txt"></script><script src="./Lord of the Mysteries - Index - Wuxiaworld_files/f(5).txt"></script><script type="text/javascript" src="./Lord of the Mysteries - Index - Wuxiaworld_files/moatframe.js"></script>
    
    <meta http-equiv="Cache-Control" content="no-siteapp"><meta http-equiv="Cache-Control" content="no-transform">
    <title>Lord of the Mysteries - Index - Wuxiaworld</title>
    <meta name="keywords" content="Lord of the Mysteries,Lord of the Mysteries novel">
    <meta name="description" content="Lord of the Mysteries is a Fantasy novels, some original, some translated from Chinese. Themes of heroism, of valor, of ascending to Immortality, of combat, of magic, of Eastern mythology and legends. Updated with awesome new content daily. Come join us for a relaxing read that will take you to brave new worlds! A paradise for readers!">
    <meta name="author" content="Cuttlefish That Loves Diving">
    <link rel="stylesheet" type="text/css" href="./Lord of the Mysteries - Index - Wuxiaworld_files/style.css">
    <script async="" src="./Lord of the Mysteries - Index - Wuxiaworld_files/analytics.js"></script><script src="./Lord of the Mysteries - Index - Wuxiaworld_files/osd.js"></script><script src="./Lord of the Mysteries - Index - Wuxiaworld_files/f(6).txt" id="google_shimpl"></script><script type="text/javascript" src="./Lord of the Mysteries - Index - Wuxiaworld_files/jquery.min.js"></script>
    <script type="text/javascript" src="./Lord of the Mysteries - Index - Wuxiaworld_files/read.js"></script>
<script async="" src="./Lord of the Mysteries - Index - Wuxiaworld_files/f(7).txt"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-2853920792116568",
    enable_page_level_ads: true
  });
</script>

<link rel="preload" href="./Lord of the Mysteries - Index - Wuxiaworld_files/f(8).txt" as="script"><script type="text/javascript" src="./Lord of the Mysteries - Index - Wuxiaworld_files/f(8).txt"></script><link rel="preload" href="./Lord of the Mysteries - Index - Wuxiaworld_files/f(9).txt" as="script"><script type="text/javascript" src="./Lord of the Mysteries - Index - Wuxiaworld_files/f(9).txt"></script><script src="./Lord of the Mysteries - Index - Wuxiaworld_files/embed.js" data-timestamp="1588991154748"></script><style type="text/css">.at-icon{fill:#fff;border:0}.at-icon-wrapper{display:inline-block;overflow:hidden}a .at-icon-wrapper{cursor:pointer}.at-rounded,.at-rounded-element .at-icon-wrapper{border-radius:12%}.at-circular,.at-circular-element .at-icon-wrapper{border-radius:50%}.addthis_32x32_style .at-icon{width:2pc;height:2pc}.addthis_24x24_style .at-icon{width:24px;height:24px}.addthis_20x20_style .at-icon{width:20px;height:20px}.addthis_16x16_style .at-icon{width:1pc;height:1pc}#at16lb{display:none;position:absolute;top:0;left:0;width:100%;height:100%;z-index:1001;background-color:#000;opacity:.001}#at_complete,#at_error,#at_share,#at_success{position:static!important}.at15dn{display:none}#at15s,#at16p,#at16p form input,#at16p label,#at16p textarea,#at_share .at_item{font-family:arial,helvetica,tahoma,verdana,sans-serif!important;font-size:9pt!important;outline-style:none;outline-width:0;line-height:1em}* html #at15s.mmborder{position:absolute!important}#at15s.mmborder{position:fixed!important;width:250px!important}#at15s{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpiZGBgaGAgAjAxEAlGFVJHIUCAAQDcngCUgqGMqwAAAABJRU5ErkJggg==);float:none;line-height:1em;margin:0;overflow:visible;padding:5px;text-align:left;position:absolute}#at15s a,#at15s span{outline:0;direction:ltr;text-transform:none}#at15s .at-label{margin-left:5px}#at15s .at-icon-wrapper{width:1pc;height:1pc;vertical-align:middle}#at15s .at-icon{width:1pc;height:1pc}.at4-icon{display:inline-block;background-repeat:no-repeat;background-position:top left;margin:0;overflow:hidden;cursor:pointer}.addthis_16x16_style .at4-icon,.addthis_default_style .at4-icon,.at4-icon,.at-16x16{width:1pc;height:1pc;line-height:1pc;background-size:1pc!important}.addthis_32x32_style .at4-icon,.at-32x32{width:2pc;height:2pc;line-height:2pc;background-size:2pc!important}.addthis_24x24_style .at4-icon,.at-24x24{width:24px;height:24px;line-height:24px;background-size:24px!important}.addthis_20x20_style .at4-icon,.at-20x20{width:20px;height:20px;line-height:20px;background-size:20px!important}.at4-icon.circular,.circular .at4-icon,.circular.aticon{border-radius:50%}.at4-icon.rounded,.rounded .at4-icon{border-radius:4px}.at4-icon-left{float:left}#at15s .at4-icon{text-indent:20px;padding:0;overflow:visible;white-space:nowrap;background-size:1pc;width:1pc;height:1pc;background-position:top left;display:inline-block;line-height:1pc}.addthis_vertical_style .at4-icon,.at4-follow-container .at4-icon{margin-right:5px}html>body #at15s{width:250px!important}#at15s.atm{background:none!important;padding:0!important;width:10pc!important}#at15s_inner{background:#fff;border:1px solid #fff;margin:0}#at15s_head{position:relative;background:#f2f2f2;padding:4px;cursor:default;border-bottom:1px solid #e5e5e5}.at15s_head_success{background:#cafd99!important;border-bottom:1px solid #a9d582!important}.at15s_head_success a,.at15s_head_success span{color:#000!important;text-decoration:none}#at15s_brand,#at15sptx,#at16_brand{position:absolute}#at15s_brand{top:4px;right:4px}.at15s_brandx{right:20px!important}a#at15sptx{top:4px;right:4px;text-decoration:none;color:#4c4c4c;font-weight:700}#at15sptx:hover{text-decoration:underline}#at16_brand{top:5px;right:30px;cursor:default}#at_hover{padding:4px}#at_hover .at_item,#at_share .at_item{background:#fff!important;float:left!important;color:#4c4c4c!important}#at_share .at_item .at-icon-wrapper{margin-right:5px}#at_hover .at_bold{font-weight:700;color:#000!important}#at_hover .at_item{width:7pc!important;padding:2px 3px!important;margin:1px;text-decoration:none!important}#at_hover .at_item.athov,#at_hover .at_item:focus,#at_hover .at_item:hover{margin:0!important}#at_hover .at_item.athov,#at_hover .at_item:focus,#at_hover .at_item:hover,#at_share .at_item.athov,#at_share .at_item:hover{background:#f2f2f2!important;border:1px solid #e5e5e5;color:#000!important;text-decoration:none}.ipad #at_hover .at_item:focus{background:#fff!important;border:1px solid #fff}.at15t{display:block!important;height:1pc!important;line-height:1pc!important;padding-left:20px!important;background-position:0 0;text-align:left}.addthis_button,.at15t{cursor:pointer}.addthis_toolbox a.at300b,.addthis_toolbox a.at300m{width:auto}.addthis_toolbox a{margin-bottom:5px;line-height:initial}.addthis_toolbox.addthis_vertical_style{width:200px}.addthis_button_facebook_like .fb_iframe_widget{line-height:100%}.addthis_button_facebook_like iframe.fb_iframe_widget_lift{max-width:none}.addthis_toolbox a.addthis_button_counter,.addthis_toolbox a.addthis_button_facebook_like,.addthis_toolbox a.addthis_button_facebook_send,.addthis_toolbox a.addthis_button_facebook_share,.addthis_toolbox a.addthis_button_foursquare,.addthis_toolbox a.addthis_button_linkedin_counter,.addthis_toolbox a.addthis_button_pinterest_pinit,.addthis_toolbox a.addthis_button_tweet{display:inline-block}.addthis_toolbox span.addthis_follow_label{display:none}.addthis_toolbox.addthis_vertical_style span.addthis_follow_label{display:block;white-space:nowrap}.addthis_toolbox.addthis_vertical_style a{display:block}.addthis_toolbox.addthis_vertical_style.addthis_32x32_style a{line-height:2pc;height:2pc}.addthis_toolbox.addthis_vertical_style .at300bs{margin-right:4px;float:left}.addthis_toolbox.addthis_20x20_style span{line-height:20px}.addthis_toolbox.addthis_32x32_style span{line-height:2pc}.addthis_toolbox.addthis_pill_combo_style .addthis_button_compact .at15t_compact,.addthis_toolbox.addthis_pill_combo_style a{float:left}.addthis_toolbox.addthis_pill_combo_style a.addthis_button_tweet{margin-top:-2px}.addthis_toolbox.addthis_pill_combo_style .addthis_button_compact .at15t_compact{margin-right:4px}.addthis_default_style .addthis_separator{margin:0 5px;display:inline}div.atclear{clear:both}.addthis_default_style .addthis_separator,.addthis_default_style .at4-icon,.addthis_default_style .at300b,.addthis_default_style .at300bo,.addthis_default_style .at300bs,.addthis_default_style .at300m{float:left}.at300b img,.at300bo img{border:0}a.at300b .at4-icon,a.at300m .at4-icon{display:block}.addthis_default_style .at300b,.addthis_default_style .at300bo,.addthis_default_style .at300m{padding:0 2px}.at300b,.at300bo,.at300bs,.at300m{cursor:pointer}.addthis_button_facebook_like.at300b:hover,.addthis_button_facebook_like.at300bs:hover,.addthis_button_facebook_send.at300b:hover,.addthis_button_facebook_send.at300bs:hover{opacity:1}.addthis_20x20_style .at15t,.addthis_20x20_style .at300bs{overflow:hidden;display:block;height:20px!important;width:20px!important;line-height:20px!important}.addthis_32x32_style .at15t,.addthis_32x32_style .at300bs{overflow:hidden;display:block;height:2pc!important;width:2pc!important;line-height:2pc!important}.at300bs{overflow:hidden;display:block;background-position:0 0;height:1pc;width:1pc;line-height:1pc!important}.addthis_default_style .at15t_compact,.addthis_default_style .at15t_expanded{margin-right:4px}#at_share .at_item{width:123px!important;padding:4px;margin-right:2px;border:1px solid #fff}#at16p{background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpiZGBgaGAgAjAxEAlGFVJHIUCAAQDcngCUgqGMqwAAAABJRU5ErkJggg==);z-index:10000001;position:absolute;top:50%;left:50%;width:300px;padding:10px;margin:0 auto;margin-top:-185px;margin-left:-155px;font-family:arial,helvetica,tahoma,verdana,sans-serif;font-size:9pt;color:#5e5e5e}#at_share{margin:0;padding:0}#at16pt{position:relative;background:#f2f2f2;height:13px;padding:5px 10px}#at16pt a,#at16pt h4{font-weight:700}#at16pt h4{display:inline;margin:0;padding:0;font-size:9pt;color:#4c4c4c;cursor:default}#at16pt a{position:absolute;top:5px;right:10px;color:#4c4c4c;text-decoration:none;padding:2px}#at15sptx:focus,#at16pt a:focus{outline:thin dotted}#at15s #at16pf a{top:1px}#_atssh{width:1px!important;height:1px!important;border:0!important}.atm{width:10pc!important;padding:0;margin:0;line-height:9pt;letter-spacing:normal;font-family:arial,helvetica,tahoma,verdana,sans-serif;font-size:9pt;color:#444;background:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpiZGBgaGAgAjAxEAlGFVJHIUCAAQDcngCUgqGMqwAAAABJRU5ErkJggg==);padding:4px}.atm-f{text-align:right;border-top:1px solid #ddd;padding:5px 8px}.atm-i{background:#fff;border:1px solid #d5d6d6;padding:0;margin:0;box-shadow:1px 1px 5px rgba(0,0,0,.15)}.atm-s{margin:0!important;padding:0!important}.atm-s a:focus{border:transparent;outline:0;transition:none}#at_hover.atm-s a,.atm-s a{display:block;text-decoration:none;padding:4px 10px;color:#235dab!important;font-weight:400;font-style:normal;transition:none}#at_hover.atm-s .at_bold{color:#235dab!important}#at_hover.atm-s a:hover,.atm-s a:hover{background:#2095f0;text-decoration:none;color:#fff!important}#at_hover.atm-s .at_bold{font-weight:700}#at_hover.atm-s a:hover .at_bold{color:#fff!important}.atm-s a .at-label{vertical-align:middle;margin-left:5px;direction:ltr}.at_PinItButton{display:block;width:40px;height:20px;padding:0;margin:0;background-image:url(//s7.addthis.com/static/t00/pinit00.png);background-repeat:no-repeat}.at_PinItButton:hover{background-position:0 -20px}.addthis_toolbox .addthis_button_pinterest_pinit{position:relative}.at-share-tbx-element .fb_iframe_widget span{vertical-align:baseline!important}#at16pf{height:auto;text-align:right;padding:4px 8px}.at-privacy-info{position:absolute;left:7px;bottom:7px;cursor:pointer;text-decoration:none;font-family:helvetica,arial,sans-serif;font-size:10px;line-height:9pt;letter-spacing:.2px;color:#666}.at-privacy-info:hover{color:#000}.body .wsb-social-share .wsb-social-share-button-vert{padding-top:0;padding-bottom:0}.body .wsb-social-share.addthis_counter_style .addthis_button_tweet.wsb-social-share-button{padding-top:40px}.body .wsb-social-share.addthis_counter_style .addthis_button_facebook_like.wsb-social-share-button{padding-top:21px}@media print{#at4-follow,#at4-share,#at4-thankyou,#at4-whatsnext,#at4m-mobile,#at15s,.at4,.at4-recommended{display:none!important}}@media screen and (max-width:400px){.at4win{width:100%}}@media screen and (max-height:700px) and (max-width:400px){.at4-thankyou-inner .at4-recommended-container{height:122px;overflow:hidden}.at4-thankyou-inner .at4-recommended .at4-recommended-item:first-child{border-bottom:1px solid #c5c5c5}}</style><style type="text/css">.at-branding-logo{font-family:helvetica,arial,sans-serif;text-decoration:none;font-size:10px;display:inline-block;margin:2px 0;letter-spacing:.2px}.at-branding-logo .at-branding-icon{background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAMAAAC67D+PAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAAZQTFRF////+GlNUkcc1QAAAB1JREFUeNpiYIQDBjQmAwMmkwEM0JnY1WIxFyDAABGeAFEudiZsAAAAAElFTkSuQmCC")}.at-branding-logo .at-branding-icon,.at-branding-logo .at-privacy-icon{display:inline-block;height:10px;width:10px;margin-left:4px;margin-right:3px;margin-bottom:-1px;background-repeat:no-repeat}.at-branding-logo .at-privacy-icon{background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAkAAAAKCAMAAABR24SMAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABhQTFRF8fr9ot/xXcfn2/P5AKva////////AKTWodjhjAAAAAd0Uk5T////////ABpLA0YAAAA6SURBVHjaJMzBDQAwCAJAQaj7b9xifV0kUKJ9ciWxlzWEWI5gMF65KUTv0VKkjVeTerqE/x7+9BVgAEXbAWI8QDcfAAAAAElFTkSuQmCC")}.at-branding-logo span{text-decoration:none}.at-branding-logo .at-branding-addthis,.at-branding-logo .at-branding-powered-by{color:#666}.at-branding-logo .at-branding-addthis:hover{color:#333}.at-cv-with-image .at-branding-addthis,.at-cv-with-image .at-branding-addthis:hover{color:#fff}a.at-branding-logo:visited{color:initial}.at-branding-info{display:inline-block;padding:0 5px;color:#666;border:1px solid #666;border-radius:50%;font-size:10px;line-height:9pt;opacity:.7;transition:all .3s ease;text-decoration:none}.at-branding-info span{border:0;clip:rect(0 0 0 0);height:1px;margin:-1px;overflow:hidden;padding:0;position:absolute;width:1px}.at-branding-info:before{content:'i';font-family:Times New Roman}.at-branding-info:hover{color:#0780df;border-color:#0780df}</style><link rel="prefetch" as="style" href="https://c.disquscdn.com/next/embed/styles/lounge.9a11b91b20ca66d0cf6475e0f5c1ab1a.css"><link rel="prefetch" as="script" href="https://c.disquscdn.com/next/embed/common.bundle.f9554506a08a1cc2b021f0dfc3f59ebb.js"><link rel="prefetch" as="script" href="https://c.disquscdn.com/next/embed/lounge.bundle.3130273e39cea7ac6e72980ac388e5e0.js"><link rel="prefetch" as="script" href="https://disqus.com/next/config.js"><script type="text/javascript" charset="utf-8" async="" src="./Lord of the Mysteries - Index - Wuxiaworld_files/layers.ab5cd98fe1b9a38a4a9f.js"></script><style type="text/css">.at-share-dock.atss{top:auto;left:0;right:0;bottom:0;width:100%;max-width:100%;z-index:1000200;box-shadow:0 0 1px 1px #e2dfe2}.at-share-dock.at-share-dock-zindex-hide{z-index:-1!important}.at-share-dock.atss-top{bottom:auto;top:0}.at-share-dock a{width:auto;transition:none;color:#fff;text-decoration:none;box-sizing:content-box;-webkit-box-sizing:content-box;-moz-box-sizing:content-box}.at-share-dock a:hover{width:auto}.at-share-dock .at4-count{height:43px;padding:5px 0 0;line-height:20px;background:#fff;font-family:Helvetica neue,arial}.at-share-dock .at4-count span{width:100%}.at-share-dock .at4-count .at4-share-label{color:#848484;font-size:10px;letter-spacing:1px}.at-share-dock .at4-count .at4-counter{top:2px;position:relative;display:block;color:#222;font-size:22px}.at-share-dock.at-shfs-medium .at4-count{height:36px;line-height:1pc;padding-top:4px}.at-share-dock.at-shfs-medium .at4-count .at4-counter{font-size:18px}.at-share-dock.at-shfs-medium .at-share-btn .at-icon-wrapper,.at-share-dock.at-shfs-medium a .at-icon-wrapper{padding:6px 0}.at-share-dock.at-shfs-small .at4-count{height:26px;line-height:1;padding-top:3px}.at-share-dock.at-shfs-small .at4-count .at4-share-label{font-size:8px}.at-share-dock.at-shfs-small .at4-count .at4-counter{font-size:14px}.at-share-dock.at-shfs-small .at-share-btn .at-icon-wrapper,.at-share-dock.at-shfs-small a .at-icon-wrapper{padding:4px 0}</style><style type="text/css">div.at-share-close-control.ats-dark,div.at-share-open-control-left.ats-dark,div.at-share-open-control-right.ats-dark{background:#262b30}div.at-share-close-control.ats-light,div.at-share-open-control-left.ats-light,div.at-share-open-control-right.ats-light{background:#fff}div.at-share-close-control.ats-gray,div.at-share-open-control-left.ats-gray,div.at-share-open-control-right.ats-gray{background:#f2f2f2}.atss{position:fixed;top:20%;width:3pc;z-index:100020;background:none}.at-share-close-control{position:relative;width:3pc;overflow:auto}.at-share-open-control-left{position:fixed;top:20%;z-index:100020;left:0;width:22px}.at-share-close-control .at4-arrow.at-left{float:right}.atss-left{left:0;float:left;right:auto}.atss-right{left:auto;float:right;right:0}.atss-right.at-share-close-control .at4-arrow.at-right{position:relative;right:0;overflow:auto}.atss-right.at-share-close-control .at4-arrow{float:left}.at-share-open-control-right{position:fixed;top:20%;z-index:100020;right:0;width:22px;float:right}.atss-right .at-share-close-control .at4-arrow{float:left}.atss.atss-right a{float:right}.atss.atss-right .at4-share-title{float:right;overflow:hidden}.atss .at-share-btn,.atss a{position:relative;display:block;width:3pc;margin:0;outline-offset:-1px;text-align:center;float:left;transition:width .15s ease-in-out;overflow:hidden;background:#e8e8e8;z-index:100030;cursor:pointer}.at-share-btn::-moz-focus-inner{border:0;padding:0}.atss-right .at-share-btn{float:right}.atss .at-share-btn{border:0;padding:0}.atss .at-share-btn:focus,.atss .at-share-btn:hover,.atss a:focus,.atss a:hover{width:4pc}.atss .at-share-btn .at-icon-wrapper,.atss a .at-icon-wrapper{display:block;padding:8px 0}.atss .at-share-btn:last-child,.atss a:last-child{border:none}.atss .at-share-btn span .at-icon,.atss a span .at-icon{position:relative;top:0;left:0;display:block;background-repeat:no-repeat;background-position:50% 50%;width:2pc;height:2pc;line-height:2pc;border:none;padding:0;margin:0 auto;overflow:hidden;cursor:pointer;cursor:hand}.at4-share .at-custom-sidebar-counter{font-family:Helvetica neue,arial;vertical-align:top;margin-right:4px;display:inline-block;text-align:center}.at4-share .at-custom-sidebar-count{font-size:17px;line-height:1.25em;color:#222}.at4-share .at-custom-sidebar-text{font-size:9px;line-height:1.25em;color:#888;letter-spacing:1px}.at4-share .at4-share-count-container{position:absolute;left:0;right:auto;top:auto;bottom:0;width:100%;color:#fff;background:inherit}.at4-share .at4-share-count,.at4-share .at4-share-count-container{line-height:1pc;font-size:10px}.at4-share .at4-share-count{text-indent:0;font-family:Arial,Helvetica Neue,Helvetica,sans-serif;font-weight:200;width:100%;height:1pc}.at4-share .at4-share-count-anchor{padding-bottom:8px;text-decoration:none;transition:padding .15s ease-in-out .15s,width .15s ease-in-out}</style><style type="text/css">#at4-drawer-outer-container{top:0;width:20pc;position:fixed}#at4-drawer-outer-container.at4-drawer-inline{position:relative}#at4-drawer-outer-container.at4-drawer-inline.at4-drawer-right{float:right;right:0;left:auto}#at4-drawer-outer-container.at4-drawer-inline.at4-drawer-left{float:left;left:0;right:auto}#at4-drawer-outer-container.at4-drawer-shown,#at4-drawer-outer-container.at4-drawer-shown *{z-index:999999}#at4-drawer-outer-container,#at4-drawer-outer-container .at4-drawer-outer,#at-drawer{height:100%;overflow-y:auto;overflow-x:hidden}.at4-drawer-push-content-right-back{position:relative;right:0}.at4-drawer-push-content-right{position:relative;left:20pc!important}.at4-drawer-push-content-left-back{position:relative;left:0}.at4-drawer-push-content-left{position:relative;right:20pc!important}#at4-drawer-outer-container.at4-drawer-right{left:auto;right:-20pc}#at4-drawer-outer-container.at4-drawer-left{right:auto;left:-20pc}#at4-drawer-outer-container.at4-drawer-shown.at4-drawer-right{left:auto;right:0}#at4-drawer-outer-container.at4-drawer-shown.at4-drawer-left{right:auto;left:0}#at-drawer{top:0;z-index:9999999;height:100%;animation-duration:.4s}#at-drawer.drawer-push.at-right{right:-20pc}#at-drawer.drawer-push.at-left{left:-20pc}#at-drawer .at-recommended-label{padding:0 0 0 20px;color:#999;line-height:3pc;font-size:18px;font-weight:300;cursor:default}#at-drawer-arrow{width:30px;height:5pc}#at-drawer-arrow.ats-dark{background:#262b30}#at-drawer-arrow.ats-gray{background:#f2f2f2}#at-drawer-open-arrow{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAABcCAYAAAC1OT8uAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjk3ODNCQjdERUQ3QjExRTM5NjFGRUZBODc3MTIwMTNCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjk3ODNCQjdFRUQ3QjExRTM5NjFGRUZBODc3MTIwMTNCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OTc4M0JCN0JFRDdCMTFFMzk2MUZFRkE4NzcxMjAxM0IiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OTc4M0JCN0NFRDdCMTFFMzk2MUZFRkE4NzcxMjAxM0IiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7kstzCAAAB4ElEQVR42uyWv0oDQRDGb9dYimgVjliID2Ca9AGfwtZob2Grja1PIFj7EhGCYK99VPBPOkVMp8X5rc6FeN7dfjOksMjAxwXZ3667OzvfLKRr682l5ZV9aDh+fxsnRHhoDzqGLjFBi4XOoFtoAxowoB893o/w7WpAl/+QgQMBwwRdTPhUC2lAV/wDA7qy5WOgq9psHejqTqkKdLE7KYCv0JZjMgBgB58raBG6mP1K6j2pT099T+qMUOeeOss1wDcEIA1PnQXy576rAUI0oFMoC7VCnn40Gs8Pd4lAiXNUKmJ0lh1mPzGEWiyUCqAGW3Pwv4IvUJsFO9CHgP3Zr6Te0xwgAf3LxaAjS241pbikCRkOg+nSJdV4p8HOPl3vvRYI5dtrgVDvvcWovcWovcWovcWovcWovcWovQChWNywNpqvdAKtQp/QNmPUIQ6kwwqt2Xmsxf6GMPM1Pptsbz45CPmXqKb+15Gz4J/LZcDSNIqBlQlbB0afe1mmUDWiCNKFZRq0VKMeXY1CTDq2sJLWsCmoaBBRqNRR6qBKC6qCaj2rDIqaXBGiXHEaom00h1S+K3fVlr6HNuqgvgCh0+owt21bybQn8+mZ78mcEebcM2e5+T2ZX24ZqCph0qn1vgQYAJ/KDpLQr2tPAAAAAElFTkSuQmCC);background-repeat:no-repeat;width:13px;height:23px;margin:28px 0 0 8px}.at-left #at-drawer-open-arrow{background-position:0 -46px}.ats-dark #at-drawer-open-arrow{background-position:0 -23px}.ats-dark.at-left #at-drawer-open-arrow{background-position:0 -69px}#at-drawer-arrow.at4-drawer-modern-browsers{position:fixed;top:40%;background-repeat:no-repeat;background-position:0 0!important;z-index:9999999}.at4-drawer-inline #at-drawer-arrow{position:absolute}#at-drawer-arrow.at4-drawer-modern-browsers.at-right{right:0}#at-drawer-arrow.at4-drawer-modern-browsers.at-left{left:0}.at4-drawer-push-animation-left{transition:left .4s ease-in-out .15s}.at4-drawer-push-animation-right{transition:right .4s ease-in-out .15s}#at-drawer.drawer-push.at4-drawer-push-animation-right{right:0}#at-drawer.drawer-push.at4-drawer-push-animation-right-back{right:-20pc!important}#at-drawer.drawer-push.at4-drawer-push-animation-left{left:0}#at-drawer.drawer-push.at4-drawer-push-animation-left-back{left:-20pc!important}#at-drawer .at4-closebutton.drawer-close{content:'X';color:#999;display:block;position:absolute;margin:0;top:0;right:0;width:3pc;height:45px;line-height:45px;overflow:hidden;opacity:.5}#at-drawer.ats-dark .at4-closebutton.drawer-close{color:#fff}#at-drawer .at4-closebutton.drawer-close:hover{opacity:1}#at-drawer.ats-dark.at4-recommended .at4-logo-container a{color:#666}#at-drawer.at4-recommended .at4-recommended-vertical{padding:0}#at-drawer.at4-recommended .at4-recommended-item .sponsored-label{margin:2px 0 0 21px;color:#ddd}#at-drawer.at4-recommended .at4-recommended-vertical .at4-recommended-item{position:relative;padding:0;width:20pc;height:180px;margin:0}#at-drawer.at4-recommended .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-img a:after{content:'';position:absolute;top:0;left:0;right:0;bottom:0;background:rgba(0,0,0,.65);z-index:1000000;transition:all .2s ease-in-out}#at-drawer.at4-recommended .at4-recommended-vertical .at4-recommended-item.at-hover .at4-recommended-item-img a:after{background:rgba(0,0,0,.8)}#at-drawer .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-img,#at-drawer .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-img a,#at-drawer .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-img img{width:20pc;height:180px;float:none}#at-drawer .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption{width:100%;position:absolute;bottom:0;left:0;height:70px}#at-drawer .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption .at-h4{color:#fff;position:absolute;height:52px;top:0;left:20px;right:20px;margin:0;padding:0;line-height:25px;font-size:20px;font-weight:600;z-index:1000001;text-decoration:none;text-transform:none}#at-drawer.at4-recommended .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption .at-h4 a:hover{text-decoration:none}#at-drawer.at4-recommended .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption .at-h4 a:link{color:#fff}#at-drawer.at4-recommended .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption small{position:absolute;top:auto;bottom:10px;left:20px;width:auto;color:#ccc}#at-drawer.at4-recommended .at4-logo-container{margin-left:20px}#at-drawer.ats-dark.at4-recommended .at4-logo-container a:hover{color:#fff}#at-drawer.at4-recommended .at-logo{margin:0}</style><style type="text/css">.at4-follow.at-mobile{display:none!important}.at4-follow{position:fixed;top:0;right:0;font-weight:400;color:#666;cursor:default;z-index:10001}.at4-follow .at4-follow-inner{position:relative;padding:10px 24px 10px 15px}.at4-follow-inner,.at-follow-open-control{border:0 solid #c5c5c5;border-width:1px 0 1px 1px;margin-top:-1px}.at4-follow .at4-follow-container{margin-left:9pt}.at4-follow.at4-follow-24 .at4-follow-container{height:24px;line-height:23px;font-size:13px}.at4-follow.at4-follow-32 .at4-follow-container{width:15pc;height:2pc;line-height:2pc;font-size:14px}.at4-follow .at4-follow-container .at-follow-label{display:inline-block;height:24px;line-height:24px;margin-right:10px;padding:0;cursor:default;float:left}.at4-follow .at4-follow-container .at-icon-wrapper{height:24px;width:24px}.at4-follow.ats-transparent .at4-follow-inner,.at-follow-open-control.ats-transparent{border-color:transparent}.at4-follow.ats-dark .at4-follow-inner,.at-follow-open-control.ats-dark{background:#262b30;border-color:#000;color:#fff}.at4-follow.ats-dark .at-follow-close-control{background-color:#262b30}.at4-follow.ats-light .at4-follow-inner{background:#fff;border-color:#c5c5c5}.at4-follow.ats-gray .at4-follow-inner,.at-follow-open-control.ats-gray{background:#f2f2f2;border-color:#c5c5c5}.at4-follow.ats-light .at4-follow-close-control,.at-follow-open-control.ats-light{background:#e5e5e5}.at4-follow .at4-follow-inner .at4-follow-close-control{position:absolute;top:0;bottom:0;left:0;width:20px;cursor:pointer;display:none}.at4-follow .at4-follow-inner .at4-follow-close-control div{display:block;line-height:20px;text-indent:-9999em;margin-top:calc(50% + 1px);overflow:hidden}.at-follow-open-control div.at4-arrow.at-left{background-position:0 -2px}.at-follow-open-control{position:fixed;height:35px;top:0;right:0;padding-top:10px;z-index:10002}.at-follow-btn{margin:0 5px 5px 0;padding:0;outline-offset:-1px;display:inline-block;box-sizing:content-box;transition:all .2s ease-in-out}.at-follow-btn:focus,.at-follow-btn:hover{transform:translateY(-4px)}.at4-follow-24 .at-follow-btn{height:25px;line-height:0;width:25px}</style><style type="text/css">.at-follow-tbx-element .at300b,.at-follow-tbx-element .at300m{display:inline-block;width:auto;padding:0;margin:0 2px 5px;outline-offset:-1px;transition:all .2s ease-in-out}.at-follow-tbx-element .at300b:focus,.at-follow-tbx-element .at300b:hover,.at-follow-tbx-element .at300m:focus,.at-follow-tbx-element .at300m:hover{transform:translateY(-4px)}.at-follow-tbx-element .addthis_vertical_style .at300b,.at-follow-tbx-element .addthis_vertical_style .at300m{display:block}.at-follow-tbx-element .addthis_vertical_style .at300b .addthis_follow_label,.at-follow-tbx-element .addthis_vertical_style .at300b .at-icon-wrapper,.at-follow-tbx-element .addthis_vertical_style .at300m .addthis_follow_label,.at-follow-tbx-element .addthis_vertical_style .at300m .at-icon-wrapper{display:inline-block;vertical-align:middle;margin-right:5px}.at-follow-tbx-element .addthis_vertical_style .at300b:focus,.at-follow-tbx-element .addthis_vertical_style .at300b:hover,.at-follow-tbx-element .addthis_vertical_style .at300m:focus,.at-follow-tbx-element .addthis_vertical_style .at300m:hover{transform:none}</style><style type="text/css">.at4-jumboshare .at-share-btn{display:inline-block;margin-right:13px;margin-top:13px}.at4-jumboshare .at-share-btn .at-icon{float:left}.at4-jumboshare .at-share-btn .at300bs{display:inline-block;float:left;cursor:pointer}.at4-jumboshare .at4-mobile .at-share-btn .at-icon,.at4-jumboshare .at4-mobile .at-share-btn .at-icon-wrapper{margin:0;padding:0}.at4-jumboshare .at4-mobile .at-share-btn{padding:0}.at4-jumboshare .at4-mobile .at-share-btn .at-label{display:none}.at4-jumboshare .at4-count{font-size:60px;line-height:60px;font-family:Helvetica neue,arial;font-weight:700}.at4-jumboshare .at4-count-container{display:table-cell;text-align:center;min-width:200px;vertical-align:middle;border-right:1px solid #ccc;padding-right:20px}.at4-jumboshare .at4-share-container{display:table-cell;vertical-align:middle;padding-left:20px}.at4-jumboshare .at4-share-container.at-share-tbx-element{padding-top:0}.at4-jumboshare .at4-title{position:relative;font-size:18px;line-height:18px;bottom:2px}.at4-jumboshare .at4-spacer{height:1px;display:block;visibility:hidden;opacity:0}.at4-jumboshare .at-share-btn{display:inline-block;margin:0 2px;line-height:0;padding:0;overflow:hidden;text-decoration:none;text-transform:none;color:#fff;cursor:pointer;transition:all .2s ease-in-out;border:0;background-color:transparent}.at4-jumboshare .at-share-btn:focus,.at4-jumboshare .at-share-btn:hover{transform:translateY(-4px);color:#fff;text-decoration:none}.at4-jumboshare .at-label{font-family:helvetica neue,helvetica,arial,sans-serif;font-size:9pt;padding:0 15px 0 0;margin:0;height:2pc;line-height:2pc;background:none}.at4-jumboshare .at-share-btn:hover,.at4-jumboshare .at-share-btn:link{text-decoration:none}.at4-jumboshare .at-share-btn::-moz-focus-inner{border:0;padding:0}.at4-jumboshare.at-mobile .at-label{display:none}</style><style type="text/css">.at4-recommendedbox-outer-container{display:inline}.at4-recommended-outer{position:static}.at4-recommended{top:20%;margin:0;text-align:center;font-weight:400;font-size:13px;line-height:17px;color:#666}.at4-recommended.at-inline .at4-recommended-horizontal{text-align:left}.at4-recommended-recommendedbox{padding:0;z-index:inherit}.at4-recommended-recommended{padding:40px 0}.at4-recommended-horizontal{max-height:340px}.at4-recommended.at-medium .at4-recommended-horizontal{max-height:15pc}.at4-recommended.at4-minimal.at-medium .at4-recommended-horizontal{padding-top:10px;max-height:230px}.at4-recommended-text-only .at4-recommended-horizontal{max-height:130px}.at4-recommended-horizontal{padding-top:5px;overflow-y:hidden}.at4-minimal{background:none;color:#000;border:none!important;box-shadow:none!important}@media screen and (max-width:900px){.at4-recommended-horizontal .at4-recommended-item,.at4-recommended-horizontal .at4-recommended-item .at4-recommended-item-img{width:15pc}}.at4-recommended.at4-minimal .at4-recommended-horizontal .at4-recommended-item .at4-recommended-item-caption{padding:0 0 10px}.at4-recommended.at4-minimal .at4-recommended-horizontal .at4-recommended-item-caption{padding:20px 0 0!important}.addthis-smartlayers .at4-recommended .at-h3.at-recommended-label{margin:0;padding:0;font-weight:300;font-size:18px;line-height:24px;color:#464646;width:100%;display:inline-block;zoom:1}.addthis-smartlayers .at4-recommended.at-inline .at-h3.at-recommended-label{text-align:left}#at4-thankyou .addthis-smartlayers .at4-recommended.at-inline .at-h3.at-recommended-label{text-align:center}.at4-recommended .at4-recommended-item{display:inline-block;zoom:1;position:relative;background:#fff;border:1px solid #c5c5c5;width:200px;margin:10px}.addthis_recommended_horizontal .at4-recommended-item{border:none}.at4-recommended .at4-recommended-item .sponsored-label{color:#666;font-size:9px;position:absolute;top:-20px}.at4-recommended .at4-recommended-item-img .at-tli,.at4-recommended .at4-recommended-item-img a{position:absolute;left:0}.at4-recommended.at-inline .at4-recommended-horizontal .at4-recommended-item{margin:10px 20px 0 0}.at4-recommended.at-medium .at4-recommended-horizontal .at4-recommended-item{margin:10px 10px 0 0}.at4-recommended.at-medium .at4-recommended-item{width:140px;overflow:hidden}.at4-recommended .at4-recommended-item .at4-recommended-item-img{position:relative;text-align:center;width:100%;height:200px;line-height:0;overflow:hidden}.at4-recommended .at4-recommended-item .at4-recommended-item-img a{display:block;width:100%;height:200px}.at4-recommended.at-medium .at4-recommended-item .at4-recommended-item-img,.at4-recommended.at-medium .at4-recommended-item .at4-recommended-item-img a{height:140px}.at4-recommended .at4-recommended-item .at4-recommended-item-img img{position:absolute;top:0;left:0;min-height:0;min-width:0;max-height:none;max-width:none;margin:0;padding:0}.at4-recommended .at4-recommended-item .at4-recommended-item-caption{height:74px;overflow:hidden;padding:20px;text-align:left;-ms-box-sizing:content-box;-o-box-sizing:content-box;box-sizing:content-box}.at4-recommended.at-medium .at4-recommended-item .at4-recommended-item-caption{height:50px;padding:15px}.at4-recommended .at4-recommended-item .at4-recommended-item-caption .at-h4{height:54px;margin:0 0 5px;padding:0;overflow:hidden;word-wrap:break-word;font-size:14px;font-weight:400;line-height:18px;text-align:left}.at4-recommended.at-medium .at4-recommended-item .at4-recommended-item-caption .at-h4{font-size:9pt;line-height:1pc;height:33px}.at4-recommended .at4-recommended-item:hover .at4-recommended-item-caption .at-h4{text-decoration:underline}.at4-recommended a:link,.at4-recommended a:visited{text-decoration:none;color:#464646}.at4-recommended .at4-recommended-item .at4-recommended-item-caption .at-h4 a:hover{text-decoration:underline;color:#000}.at4-recommended .at4-recommended-item .at4-recommended-item-caption small{display:block;white-space:nowrap;overflow:hidden;text-overflow:ellipsis;font-size:11px;color:#666}.at4-recommended.at-medium .at4-recommended-item .at4-recommended-item-caption small{font-size:9px}.at4-recommended .at4-recommended-vertical{padding:15px 0 0}.at4-recommended .at4-recommended-vertical .at4-recommended-item{display:block;width:auto;max-width:100%;height:60px;border:none;margin:0 0 15px;box-shadow:none;background:none}.at4-recommended-vertical .at4-recommended-item .at4-recommended-item-img,.at4-recommended-vertical .at4-recommended-item .at4-recommended-item-img img{width:60px;height:60px;float:left}.at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption{border-top:none;margin:0;height:60px;padding:3px 5px}.at4-recommended .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption .at-h4{height:38px;margin:0}.at4-recommended .at4-recommended-vertical .at4-recommended-item .at4-recommended-item-caption small{position:absolute;bottom:0}.at4-recommended .at-recommended-label.at-vertical{text-align:left}.at4-no-image-light-recommended,.at4-no-image-minimal-recommended{background-color:#f2f2f2!important}.at4-no-image-gray-recommended{background-color:#e6e6e5!important}.at4-no-image-dark-recommended{background-color:#4e555e!important}.at4-recommended .at4-recommended-item-placeholder-img{background-repeat:no-repeat!important;background-position:center!important;width:100%!important;height:100%!important}.at4-recommended-horizontal .at4-no-image-dark-recommended .at4-recommended-item-placeholder-img{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAfCAYAAACCox+xAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjlFNUUyQTg3MTI0RDExRTM4NzAwREJDRjlCQzAyMUVFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjlFNUUyQTg4MTI0RDExRTM4NzAwREJDRjlCQzAyMUVFIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OUU1RTJBODUxMjREMTFFMzg3MDBEQkNGOUJDMDIxRUUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OUU1RTJBODYxMjREMTFFMzg3MDBEQkNGOUJDMDIxRUUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6oCfPiAAABfUlEQVR42uyWTU/DMAyGm3bdBxp062hHe+PC//9HCIkDYpNAO7CPAuWN5Eohyhpno2GHWqq8pO78xHHsiLquH4L/l6cwuBAZaOPKs//YBFIJIR59UiAt7huYi90aE/UQakTDLaL26RUEAAJqiefm93T9Bpj1X4O0bY0OIUXCpYBJvYDAUWyAUCWliHGTcnpqRMaM72ImRAJVknYG+eb4YEDIBeU0zGnsBLK1ODogYSsLhDwIJeVVk18lzfNA4ERGZNXi59UCIQhiYDilpSm/jp4awLxDvWhlf4/nGe8+LLuSt+SZul28ggaHG6gNVhDR+IuRFzOoxGKWwG7vVFm5AAQxgcqYpzrjFjR9zwPH5LSuT7XlNr2MQm5LzqjLpncNNaM+s8M27Y60g3FwhoSMzrtUQllgLtRs5pZ2cB4IhbvQbGRZv1NsrhyS8+SI5Mo9RJWpjAI1xqKL+0iEP180vy214JbeR12AyOgsHI7e0NfFyKv0ID1ID+IqPwIMAOeljGQOryBmAAAAAElFTkSuQmCC)!important}.at4-recommended-vertical .at4-no-image-dark-recommended .at4-recommended-item-placeholder-img{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAOCAYAAADwikbvAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjAzREMyNTM2MTI0RjExRTM4NzAwREJDRjlCQzAyMUVFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjAzREMyNTM3MTI0RjExRTM4NzAwREJDRjlCQzAyMUVFIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDNEQzI1MzQxMjRGMTFFMzg3MDBEQkNGOUJDMDIxRUUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDNEQzI1MzUxMjRGMTFFMzg3MDBEQkNGOUJDMDIxRUUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz5GfbtkAAAAxklEQVR42qRSTQvCMAxduk53mEOHKFPP/v8/5cGTiIibivVFUomlG7gFHvloXpKmJefcPhkmNyvGEWj+IOZA6ckPImoxxVwOLvCvXUzkpayNCpRQK64IbOBnAYGAXMeMslNlU+CzrIEdCkxi5DPAoz6BE8ZuVNdKJuL8rS9sv62IXlCHyP0KqKUKZXK9uwkSLVArfwpVR3b225kXwovibcP+jC4jUtfWPZmfqJJnYlkAM128j1z0nHWKSUbIKDL/msHktwADAPptQo+vkZNLAAAAAElFTkSuQmCC)!important}.at4-recommended-horizontal .at4-no-image-gray-recommended .at4-recommended-item-placeholder-img,.at4-recommended-horizontal .at4-no-image-light-recommended .at4-recommended-item-placeholder-img,.at4-recommended-horizontal .at4-no-image-minimal-recommended .at4-recommended-item-placeholder-img{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACIAAAAfCAYAAACCox+xAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjAzREMyNTMyMTI0RjExRTM4NzAwREJDRjlCQzAyMUVFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjAzREMyNTMzMTI0RjExRTM4NzAwREJDRjlCQzAyMUVFIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OUU1RTJBODkxMjREMTFFMzg3MDBEQkNGOUJDMDIxRUUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OUU1RTJBOEExMjREMTFFMzg3MDBEQkNGOUJDMDIxRUUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz6dfDQvAAABg0lEQVR42uyWS0vDQBDH82jaKNW0qUltbl68e/Di98eLBz+CCB5EBaWIpUat/4UJLMuame1j7SEDYbqbKfPLvHbDi8ur8+D/5T4K9kR6xrr27D+xgdS3N9d3PilQFmcNzN6mxkbdhxrQcoGofXkFAUAINcVzrG2vsP8KmJdtg7SlxoRQouBywOReQOAosUDoklPEpEU5XDciqeB/iRAig6pIO4P8CHysBBDqg0palrR2Alkwjj5RsDUDoRqhorpq6quifRkInKiIPLf4eWIgQoLoWbq0stXXn10DmDeoR2PsL/E84N0Hk5Wypc70dMkGGhzOoeb4gpjW34K6GEFljFkGu6XTZJUCEMQBVCHs6kI60MycB47FyUmo20oPvYJCzhVnvIsR3zg5ghoRTNpyHKTBBhIJTt6pFsoZ9iLDZswcB5uBULhnho0a66eazaFDca59Hym1e4guQ4rCO4Fu/T4Sw8Gk+c3MghN6H+8CRKVg4tB6fV8XI6/SgXQgHYir/AowAMU5TskhKVUNAAAAAElFTkSuQmCC)!important}.at4-recommended-vertical .at4-no-image-gray-recommended .at4-recommended-item-placeholder-img,.at4-recommended-vertical .at4-no-image-light-recommended .at4-recommended-item-placeholder-img,.at4-recommended-vertical .at4-no-image-minimal-recommended .at4-recommended-item-placeholder-img{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAOCAYAAADwikbvAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyNpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNS1jMDE0IDc5LjE1MTQ4MSwgMjAxMy8wMy8xMy0xMjowOToxNSAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIChNYWNpbnRvc2gpIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjAzREMyNTNBMTI0RjExRTM4NzAwREJDRjlCQzAyMUVFIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjAzREMyNTNCMTI0RjExRTM4NzAwREJDRjlCQzAyMUVFIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6MDNEQzI1MzgxMjRGMTFFMzg3MDBEQkNGOUJDMDIxRUUiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MDNEQzI1MzkxMjRGMTFFMzg3MDBEQkNGOUJDMDIxRUUiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz65Fr9cAAAA0ElEQVR42qRRuQrCQBDd3SSaIgYNosSrtLew8f+xsfAnYmEVRMR4YHwjExjCbsBk4DHHzptjR2+2u7VqJ3efjTNQ/EEMgbgiv46H/QNTDPnhCv/mYiLPI21EIIaaUEVgBj+oETQQypgRtidsXfNJpsACBXo28gWgUd9AjrEL0TXhiSh/XhWudlZI/kCdLPtFUGMRCni9p6kl+kAq/D5UavmzX2fNd87obsCSfztnrOR0rjvTiRImkoyAQQNRyZ2jhjenGNVBOpF1WZatyV8BBgBJ+irgS/KHdAAAAABJRU5ErkJggg==)!important}#at-drawer.ats-dark,.at4-recommended.ats-dark .at4-recommended-horizontal .at4-recommended-item-caption,.at4-recommended.ats-dark .at4-recommended-vertical .at4-recommended-item-caption{background:#262b30}#at-drawer.ats-gray,.at4-recommended.ats-gray .at4-recommended-horizontal .at4-recommended-item-caption{background:#f2f2f2}#at-drawer.ats-light,.at4-recommended.ats-light .at4-recommended-horizontal .at4-recommended-item-caption{background:#fff}.at4-recommended.ats-dark .at4-recommended-vertical .at4-recommended-item{background:none}.at4-recommended.ats-dark .at4-recommended-item .at4-recommended-item-caption a:hover,.at4-recommended.ats-dark .at4-recommended-item .at4-recommended-item-caption a:link,.at4-recommended.ats-dark .at4-recommended-item .at4-recommended-item-caption a:visited,.at4-recommended.ats-dark .at4-recommended-item .at4-recommended-item-caption small,.at4-recommended.ats-dark .at4-recommended-item-caption,.at4-recommended.ats-dark .at-logo a:hover,.at4-recommended.ats-dark .at-recommended-label.at-vertical{color:#fff}.at4-recommended-vertical-logo{padding-top:0;text-align:left}.at4-recommended-vertical-logo .at4-logo-container{line-height:10px}.at4-recommended-horizontal-logo{text-align:center}.at4-recommended.at-inline .at4-recommended-horizontal-logo{text-align:left}#at4-thankyou .at4-recommended.at-inline .at4-recommended-horizontal{text-align:center}.at4-recommended .at-logo{margin:10px 0 0;padding:0;height:25px;overflow:auto;-ms-box-sizing:content-box;-o-box-sizing:content-box;box-sizing:content-box}.at4-recommended.at-inline .at4-recommended-horizontal .at-logo{text-align:left}.at4-recommended .at4-logo-container a.at-sponsored-link{color:#666}.at4-recommended-class .at4-logo-container a:hover,.at4-recommendedbox-outer-container .at4-recommended-recommendedbox .at4-logo-container a:hover{color:#000}</style><style type="text/css">.at-recommendedjumbo-outer-container{margin:0;padding:0;border:0;background:none;color:#000}.at-recommendedjumbo-footer{position:relative;width:100%;height:510px;overflow:hidden;transition:all .3s ease-in-out}.at-mobile .at-recommendedjumbo-footer{height:250px}.at-recommendedjumbo-footer #bg-link:after{content:'';position:absolute;top:0;left:0;right:0;bottom:0;background:rgba(0,0,0,.75)}.at-recommendedjumbo-footer:hover #bg-link:after{background:rgba(0,0,0,.85)}.at-recommendedjumbo-footer *,.at-recommendedjumbo-footer :after,.at-recommendedjumbo-footer :before{box-sizing:border-box}.at-recommendedjumbo-footer:hover #at-recommendedjumbo-footer-bg{animation:atRecommendedJumboAnimatedBackground 1s ease-in-out 1;animation-fill-mode:forwards}.at-recommendedjumbo-footer #at-recommendedjumbo-top-holder{position:absolute;top:0;padding:0 40px;width:100%}.at-mobile .at-recommendedjumbo-footer #at-recommendedjumbo-top-holder{padding:0 20px}.at-recommendedjumbo-footer .at-recommendedjumbo-footer-inner{position:relative;text-align:center;font-family:helvetica,arial,sans-serif;z-index:2;width:100%}.at-recommendedjumbo-footer #at-recommendedjumbo-label-holder{margin:40px 0 0;max-height:30px}.at-mobile .at-recommendedjumbo-footer #at-recommendedjumbo-label-holder{margin:20px 0 0;max-height:20px}.at-recommendedjumbo-footer #at-recommendedjumbo-label{font-weight:300;font-size:24px;line-height:24px;color:#fff;margin:0}.at-mobile .at-recommendedjumbo-footer #at-recommendedjumbo-label{font-weight:150;font-size:14px;line-height:14px}.at-recommendedjumbo-footer #at-recommendedjumbo-title-holder{margin:20px 0 0;min-height:3pc;max-height:78pt}.at-mobile .at-recommendedjumbo-footer #at-recommendedjumbo-title-holder{margin:10px 0 0;min-height:24px;max-height:54px}.at-recommendedjumbo-footer #at-recommendedjumbo-content-title{font-size:3pc;line-height:52px;font-weight:700;margin:0}.at-mobile .at-recommendedjumbo-footer #at-recommendedjumbo-content-title{font-size:24px;line-height:27px}.at-recommendedjumbo-footer a{text-decoration:none;color:#fff}.at-recommendedjumbo-footer a:visited{color:#fff}.at-recommendedjumbo-footer small{margin:20px 0 0;display:inline-block;height:2pc;line-height:2pc;font-size:14px;color:#ccc;cursor:default}.at-mobile .at-recommendedjumbo-footer small{margin:10px 0 0;height:14px;line-height:14px;font-size:9pt}.at-recommendedjumbo-footer .at-logo-container{position:absolute;bottom:20px;margin:auto;left:0;right:0}.at-mobile .at-recommendedjumbo-footer .at-logo-container{bottom:10px}.at-recommendedjumbo-footer a.at-sponsored-link{color:#ccc}.at-recommendedjumbo-footer div #at-recommendedjumbo-logo-link{padding:2px 0 0 11px;text-decoration:none;line-height:20px;font-family:helvetica,arial,sans-serif;font-size:9px;color:#ccc}@keyframes atRecommendedJumboAnimatedBackground{0%{transform:scale(1,1)}to{transform:scale(1.1,1.1)}}</style><style type="text/css">.at-resp-share-element{position:relative;padding:0;margin:0;font-size:0;line-height:0}.at-resp-share-element:after,.at-resp-share-element:before{content:" ";display:table}.at-resp-share-element.at-mobile .at4-share-count-container,.at-resp-share-element.at-mobile .at-label{display:none}.at-resp-share-element .at-share-btn{display:inline-block;*display:inline;*zoom:1;margin:0 2px 5px;padding:0;overflow:hidden;line-height:0;text-decoration:none;text-transform:none;color:#fff;cursor:pointer;transition:all .2s ease-in-out;border:0;font-family:helvetica neue,helvetica,arial,sans-serif;background-color:transparent}.at-resp-share-element .at-share-btn::-moz-focus-inner{border:0;padding:0}.at-resp-share-element .at-share-btn:focus,.at-resp-share-element .at-share-btn:hover{transform:translateY(-4px);color:#fff;text-decoration:none}.at-resp-share-element .at-share-btn .at-icon-wrapper{float:left}.at-resp-share-element .at-share-btn.at-share-btn.at-svc-compact:hover{transform:none}.at-resp-share-element .at-share-btn .at-label{font-family:helvetica neue,helvetica,arial,sans-serif;font-size:9pt;padding:0 15px 0 0;margin:0 0 0 5px;height:2pc;line-height:2pc;background:none}.at-resp-share-element .at-icon,.at-resp-share-element .at-label{cursor:pointer}.at-resp-share-element .at4-share-count-container{text-decoration:none;float:right;padding-right:15px;font-size:9pt}.at-mobile .at-resp-share-element .at-label{display:none}.at-resp-share-element.at-mobile .at-share-btn{margin-right:5px}.at-mobile .at-resp-share-element .at-share-btn{padding:5px;margin-right:5px}</style><style type="text/css">.at-share-tbx-element{position:relative;margin:0;color:#fff;font-size:0}.at-share-tbx-element,.at-share-tbx-element .at-share-btn{font-family:helvetica neue,helvetica,arial,sans-serif;padding:0;line-height:0}.at-share-tbx-element .at-share-btn{cursor:pointer;margin:0 5px 5px 0;display:inline-block;overflow:hidden;border:0;text-decoration:none;text-transform:none;background-color:transparent;color:inherit;transition:all .2s ease-in-out}.at-share-tbx-element .at-share-btn:focus,.at-share-tbx-element .at-share-btn:hover{transform:translateY(-4px);outline-offset:-1px;color:inherit}.at-share-tbx-element .at-share-btn::-moz-focus-inner{border:0;padding:0}.at-share-tbx-element .at-share-btn.at-share-btn.at-svc-compact:hover{transform:none}.at-share-tbx-element .at-icon-wrapper{vertical-align:middle}.at-share-tbx-element .at4-share-count,.at-share-tbx-element .at-label{margin:0 7.5px 0 2.5px;text-decoration:none;vertical-align:middle;display:inline-block;background:none;height:0;font-size:inherit;line-height:inherit;color:inherit}.at-share-tbx-element.at-mobile .at4-share-count,.at-share-tbx-element.at-mobile .at-label{display:none}.at-share-tbx-element .at_native_button{vertical-align:middle}.at-share-tbx-element .addthis_counter.addthis_bubble_style{margin:0 2px;vertical-align:middle;display:inline-block}.at-share-tbx-element .fb_iframe_widget{display:block}.at-share-tbx-element.at-share-tbx-native .at300b{vertical-align:middle}.at-style-responsive .at-share-btn{padding:5px}.at-style-jumbo{display:table}.at-style-jumbo .at4-spacer{height:1px;display:block;visibility:hidden;opacity:0}.at-style-jumbo .at4-count-container{display:table-cell;text-align:center;min-width:200px;vertical-align:middle;border-right:1px solid #ccc;padding-right:20px}.at-style-jumbo .at4-count{font-size:60px;line-height:60px;font-weight:700}.at-style-jumbo .at4-count-title{position:relative;font-size:18px;line-height:18px;bottom:2px}.at-style-jumbo .at-share-btn-elements{display:table-cell;vertical-align:middle;padding-left:20px}.at_flat_counter{cursor:pointer;font-family:helvetica,arial,sans-serif;font-weight:700;text-transform:uppercase;display:inline-block;position:relative;vertical-align:top;height:auto;margin:0 5px;padding:0 6px;left:-1px;background:#ebebeb;color:#32363b;transition:all .2s ease}.at_flat_counter:after{top:30%;left:-4px;content:"";position:absolute;border-width:5px 8px 5px 0;border-style:solid;border-color:transparent #ebebeb transparent transparent;display:block;width:0;height:0;transform:translateY(360deg)}.at_flat_counter:hover{background:#e1e2e2}</style><style type="text/css">.at4-thankyou-background{top:0;right:0;left:0;bottom:0;-webkit-overflow-scrolling:touch;z-index:9999999;background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpizCuu/sRABGBiIBKMKqSOQoAAAwC8KgJipENhxwAAAABJRU5ErkJggg==);background:hsla(217,6%,46%,.95)}.at4-thankyou-background.at-thankyou-shown{position:fixed}.at4-thankyou-inner{position:absolute;width:100%;top:10%;left:50%;margin-left:-50%;text-align:center}.at4-thankyou-mobile .at4-thankyou-inner{top:5%}.thankyou-description{font-weight:400}.at4-thankyou-background .at4lb-inner{position:relative;width:100%;height:100%}.at4-thankyou-background .at4lb-inner .at4x{position:absolute;top:15px;right:15px;display:block;width:20px;height:20px;padding:20px;margin:0;cursor:pointer;transition:opacity .25s ease-in;opacity:.4;background:url("data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNui8sowAAAAWdEVYdENyZWF0aW9uIFRpbWUAMTEvMTMvMTKswDp5AAAAd0lEQVQ4jb2VQRLAIAgDE///Z3qqY1FAhalHMCsCIkVEAIAkkVgvp2lDBgYAnAyHkWotLccNrEd4A7X2TqIdqLfnWBAdaF5rJdyJfjtPH5GT37CaGhoVq3nOm/XflUuLUto2pY1d+vRKh0Pp+MrAVtDe2JkvYNQ+jVSEEFmOkggAAAAASUVORK5CYII=") no-repeat center center;overflow:hidden;text-indent:-99999em;border:1px solid transparent}.at4-thankyou-background .at4lb-inner .at4x:focus,.at4-thankyou-background .at4lb-inner .at4x:hover{border:1px solid #fff;border-radius:50%;outline:0}.at4-thankyou-background .at4lb-inner #at4-palogo{position:absolute;bottom:10px;display:inline-block;text-decoration:none;font-family:helvetica,arial,sans-serif;font-size:11px;cursor:pointer;-webkit-transition:opacity .25s ease-in;moz-transition:opacity .25s ease-in;transition:opacity .25s ease-in;opacity:.5;z-index:100020;color:#fff;padding:2px 0 0 13px}.at4-thankyou-background .at4lb-inner #at4-palogo .at-branding-addthis,.at4-thankyou-background .at4lb-inner #at4-palogo .at-branding-info{color:#fff}.at4-thankyou-background .at4lb-inner #at4-palogo:hover,.at4-thankyou-background.ats-dark .at4lb-inner a#at4-palogo:hover{text-decoration:none;color:#fff;opacity:1}.at4-thankyou-background.ats-dark{background-image:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAoAAAAKCAYAAACNMs+9AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABtJREFUeNpiZGBgeMZABGBiIBKMKqSOQoAAAwB+cQD6hqlbCwAAAABJRU5ErkJggg==");background:rgba(0,0,0,.85)}.at4-thankyou-background .thankyou-title{color:#fff;font-size:38.5px;margin:10px 20px;line-height:38.5px;font-family:helvetica neue,helvetica,arial,sans-serif;font-weight:300}.at4-thankyou-background.ats-dark .thankyou-description,.at4-thankyou-background.ats-dark .thankyou-title{color:#fff}.at4-thankyou-background .thankyou-description{color:#fff;font-size:18px;margin:10px 0;line-height:24px;padding:0;font-family:helvetica neue,helvetica,arial,sans-serif;font-weight:300}.at4-thankyou-background .at4-thanks-icons{padding-top:10px}.at4-thankyou-mobile *{-webkit-overflow-scrolling:touch}#at4-thankyou .at4-recommended-recommendedbox .at-logo{display:none}.at4-thankyou .at-h3{height:49px;line-height:49px;margin:0 50px 0 20px;padding:1px 0 0;font-family:helvetica neue,helvetica,arial,sans-serif;font-size:1pc;font-weight:700;color:#fff;text-shadow:0 1px #000}.at4-thanks{padding-top:50px;text-align:center}.at4-thanks label{display:block;margin:0 0 15px;font-size:1pc;line-height:1pc}.at4-thanks .at4-h2{background:none;border:none;margin:0 0 10px;padding:0;font-family:helvetica neue,helvetica,arial,sans-serif;font-size:28px;font-weight:300;color:#000}.at4-thanks .at4-thanks-icons{position:relative;height:2pc}.at4-thanks .at4-thanks-icons .at-thankyou-label{display:block;padding-bottom:10px;font-size:14px;color:#666}.at4-thankyou-layer .at-follow .at-icon-wrapper{width:2pc;height:2pc}</style><style type="text/css">.at4-recommended-toaster{position:fixed;top:auto;bottom:0;right:0;z-index:100021}.at4-recommended-toaster.ats-light{border:1px solid #c5c5c5;background:#fff}.at4-recommended-toaster.ats-gray{border:1px solid #c5c5c5;background:#f2f2f2}.at4-recommended-toaster.ats-dark{background:#262b30;color:#fff}.at4-recommended-toaster .at4-recommended-container{padding-top:0;margin:0}.at4-recommended.at4-recommended-toaster div.at-recommended-label{line-height:1pc;font-size:1pc;text-align:left;padding:20px 0 0 20px}.at4-toaster-outer .at4-recommended .at4-recommended-item .at4-recommended-item-caption .at-h4{font-size:11px;line-height:11px;margin:10px 0 6px;height:30px}.at4-recommended.at4-recommended-toaster div.at-recommended-label.ats-gray,.at4-recommended.at4-recommended-toaster div.at-recommended-label.ats-light{color:#464646}.at4-recommended.at4-recommended-toaster div.at-recommended-label.ats-dark{color:#fff}.at4-toaster-close-control{position:absolute;top:0;right:0;display:block;width:20px;height:20px;line-height:20px;margin:5px 5px 0 0;padding:0;text-indent:-9999em}.at4-toaster-open-control{position:fixed;right:0;bottom:0;z-index:100020}.at4-toaster-outer .at4-recommended-item{width:90pt;border:0;margin:9px 10px 0}.at4-toaster-outer .at4-recommended-item:first-child{margin-left:20px}.at4-toaster-outer .at4-recommended-item:last-child{margin-right:20px}.at4-toaster-outer .at4-recommended-item .at4-recommended-item-img{max-height:90pt;max-width:90pt}.at4-toaster-outer .at4-recommended-item .at4-recommended-item-img img{height:90pt;width:90pt}.at4-toaster-outer .at4-recommended-item .at4-recommended-item-caption{height:30px;padding:0;margin:0;height:initial}.at4-toaster-outer .ats-dark .at4-recommended-item .at4-recommended-item-caption{background:#262b30}.at4-toaster-outer .at4-recommended .at4-recommended-item .at4-recommended-item-caption small{width:auto;line-height:14px;margin:0}.at4-toaster-outer .at4-recommended.ats-dark .at4-recommended-item .at4-recommended-item-caption small{color:#fff}.at4-recommended-toaster .at-logo{margin:0 0 3px 20px;text-align:left}.at4-recommended-toaster .at-logo .at4-logo-container.at-sponsored-logo{position:relative}.at4-toaster-outer .at4-recommended-item .sponsored-label{text-align:right;font-size:10px;color:#666;float:right;position:fixed;bottom:6px;right:20px;top:initial;z-index:99999}</style><style type="text/css">.at4-whatsnext{position:fixed;bottom:0!important;right:0;background:#fff;border:1px solid #c5c5c5;margin:-1px;width:390px;height:90pt;overflow:hidden;font-size:9pt;font-weight:400;color:#000;z-index:1800000000}.at4-whatsnext a{color:#666}.at4-whatsnext .at-whatsnext-content{height:90pt;position:relative}.at4-whatsnext .at-whatsnext-content .at-branding{position:absolute;bottom:15px;right:10px;padding-left:9px;text-decoration:none;line-height:10px;font-family:helvetica,arial,sans-serif;font-size:10px;color:#666}.at4-whatsnext .at-whatsnext-content .at-whatsnext-content-inner{position:absolute;top:15px;right:20px;bottom:15px;left:140px;text-align:left;height:105px}.at4-whatsnext .at-whatsnext-content-inner a{display:inline-block}.at4-whatsnext .at-whatsnext-content-inner div.at-h6{text-align:left;margin:0;padding:0 0 3px;font-size:11px;color:#666;cursor:default}.at4-whatsnext .at-whatsnext-content .at-h3{text-align:left;margin:5px 0;padding:0;line-height:1.2em;font-weight:400;font-size:14px;height:3pc}.at4-whatsnext .at-whatsnext-content-inner a:link,.at4-whatsnext .at-whatsnext-content-inner a:visited{text-decoration:none;font-weight:400;color:#464646}.at4-whatsnext .at-whatsnext-content-inner a:hover{color:#000}.at4-whatsnext .at-whatsnext-content-inner small{position:absolute;bottom:15px;line-height:10px;font-size:11px;color:#666;cursor:default;text-align:left}.at4-whatsnext .at-whatsnext-content .at-whatsnext-content-img{position:absolute;top:0;left:0;width:90pt;height:90pt;overflow:hidden}.at4-whatsnext .at-whatsnext-content .at-whatsnext-content-img img{position:absolute;top:0;left:0;max-height:none;max-width:none}.at4-whatsnext .at-whatsnext-close-control{position:absolute;top:0;right:0;display:block;width:20px;height:20px;line-height:20px;margin:0 5px 0 0;padding:0;text-indent:-9999em}.at-whatsnext-open-control{position:fixed;right:0;bottom:0;z-index:100020}.at4-whatsnext.ats-dark{background:#262b30}.at4-whatsnext.ats-dark .at-whatsnext-content .at-h3,.at4-whatsnext.ats-dark .at-whatsnext-content a.at4-logo:hover,.at4-whatsnext.ats-dark .at-whatsnext-content-inner a:link,.at4-whatsnext.ats-dark .at-whatsnext-content-inner a:visited{color:#fff}.at4-whatsnext.ats-light{background:#fff}.at4-whatsnext.ats-gray{background:#f2f2f2}.at4-whatsnext.at-whatsnext-nophoto{width:270px}.at4-whatsnext.at-whatsnext-nophoto .at-whatsnext-content-img{display:none}.at4-whatsnext.at-whatsnext-nophoto .at-whatsnext-content .at-whatsnext-content-inner{top:15px;right:0;left:20px}.at4-whatsnext.at-whatsnext-nophoto .at-whatsnext-content .at-whatsnext-content-inner.addthis_32x32_style{top:0;right:0;left:0;padding:45px 20px 0;font-size:20px}.at4-whatsnext.at-whatsnext-nophoto .at-whatsnext-content .at-whatsnext-content-inner .at4-icon,.at4-whatsnext.at-whatsnext-nophoto .at-whatsnext-content .at-whatsnext-content-inner .at4-icon-fw,.at4-whatsnext.at-whatsnext-nophoto .at-whatsnext-content .at-whatsnext-content-inner .whatsnext-msg{vertical-align:middle}.at-whatsnext-img,.at-whatsnext-img-lnk{position:absolute;left:0}</style><style type="text/css">.at4-whatsnextmobile{position:fixed;bottom:0;right:0;left:0;background:#fff;z-index:9999998;height:170px;font-size:28px}.at4-whatsnextmobile .col-2{height:100%;font-size:1em}.at4-whatsnextmobile .col-2:first-child{max-width:200px;display:inline-block;float:left}.at4-whatsnextmobile .col-2:last-child{position:absolute;left:200px;right:50px;top:0;bottom:0;display:inline-block}.at4-whatsnextmobile .at-whatsnext-content-inner{font-size:1em}.at4-whatsnextmobile .at-whatsnext-content-img img{height:100%;width:100%}.at4-whatsnextmobile .at-close-control{font-size:1em;position:absolute;top:0;right:0;width:50px;height:50px}.at4-whatsnextmobile .at-close-control button{width:100%;height:100%;font-size:1em;font-weight:400;text-decoration:none;opacity:.5;padding:0;cursor:pointer;background:0 0;border:0;-webkit-appearance:none}.at4-whatsnextmobile .at-h3,.at4-whatsnextmobile .at-h6{font-size:1em;margin:0;color:#a1a1a1;margin-left:2.5%;margin-top:25px}.at4-whatsnextmobile .at-h3{font-size:1em;line-height:1em;font-weight:500;height:50%}.at4-whatsnextmobile .at-h3 a{font-size:1em;text-decoration:none}.at4-whatsnextmobile .at-h6{font-size:.8em;line-height:.8em;font-weight:500}.at4-whatsnextmobile .footer{position:absolute;bottom:2px;left:200px;right:0;padding-left:2.5%;font-size:1em;line-height:.6em}.at4-whatsnextmobile .footer small{font-size:.6em;color:#a1a1a1}.at4-whatsnextmobile .footer small:first-child{margin-right:5%;float:left}.at4-whatsnextmobile .footer small:last-child{margin-right:2.5%;float:right}.at4-whatsnextmobile .at-whatsnext-content{height:100%}.at4-whatsnextmobile.ats-dark{background:#262b30;color:#fff}.at4-whatsnextmobile .at-close-control button{color:#bfbfbf}.at4-whatsnextmobile.ats-dark a:link,.at4-whatsnextmobile.ats-dark a:visited{color:#fff}.at4-whatsnextmobile.ats-gray{background:#f2f2f2;color:#262b30}.at4-whatsnextmobile.ats-light{background:#fff;color:#262b30}.at4-whatsnextmobile.ats-dark .footer a:link,.at4-whatsnextmobile.ats-dark .footer a:visited,.at4-whatsnextmobile.ats-gray .footer a:link,.at4-whatsnextmobile.ats-gray .footer a:visited,.at4-whatsnextmobile.ats-light .footer a:link,.at4-whatsnextmobile.ats-light .footer a:visited{color:#a1a1a1}.at4-whatsnextmobile.ats-gray a:link,.at4-whatsnextmobile.ats-gray a:visited,.at4-whatsnextmobile.ats-light a:link,.at4-whatsnextmobile.ats-light a:visited{color:#262b30}@media only screen and (min-device-width:320px) and (max-device-width:480px){.at4-whatsnextmobile{height:85px;font-size:14px}.at4-whatsnextmobile .col-2:first-child{width:75pt}.at4-whatsnextmobile .col-2:last-child{right:25px;left:75pt}.at4-whatsnextmobile .footer{left:75pt}.at4-whatsnextmobile .at-close-control{width:25px;height:25px}.at4-whatsnextmobile .at-h3,.at4-whatsnextmobile .at-h6{margin-top:12.5px}}</style><style type="text/css">.at-custom-mobile-bar{left:0;right:0;width:100%;height:56px;position:fixed;text-align:center;z-index:100020;background:#fff;overflow:hidden;box-shadow:0 0 10px 0 rgba(0,0,0,.2);font:initial;line-height:normal;top:auto;bottom:0}.at-custom-mobile-bar.at-custom-mobile-bar-zindex-hide{z-index:-1!important}.at-custom-mobile-bar.atss-top{top:0;bottom:auto}.at-custom-mobile-bar.atss-bottom{top:auto;bottom:0}.at-custom-mobile-bar .at-custom-mobile-bar-btns{display:inline-block;text-align:center}.at-custom-mobile-bar .at-custom-mobile-bar-counter,.at-custom-mobile-bar .at-share-btn{margin-top:4px}.at-custom-mobile-bar .at-share-btn{display:inline-block;text-decoration:none;transition:none;box-sizing:content-box}.at-custom-mobile-bar .at-custom-mobile-bar-counter{font-family:Helvetica neue,arial;vertical-align:top;margin-left:4px;margin-right:4px;display:inline-block}.at-custom-mobile-bar .at-custom-mobile-bar-count{font-size:26px;line-height:1.25em;color:#222}.at-custom-mobile-bar .at-custom-mobile-bar-text{font-size:9pt;line-height:1.25em;color:#888;letter-spacing:1px}.at-custom-mobile-bar .at-icon-wrapper{text-align:center;height:3pc;width:3pc;margin:0 4px}.at-custom-mobile-bar .at-icon{vertical-align:top;margin:8px;width:2pc;height:2pc}.at-custom-mobile-bar.at-shfs-medium{height:3pc}.at-custom-mobile-bar.at-shfs-medium .at-custom-mobile-bar-counter{margin-top:6px}.at-custom-mobile-bar.at-shfs-medium .at-custom-mobile-bar-count{font-size:18px}.at-custom-mobile-bar.at-shfs-medium .at-custom-mobile-bar-text{font-size:10px}.at-custom-mobile-bar.at-shfs-medium .at-icon-wrapper{height:40px;width:40px}.at-custom-mobile-bar.at-shfs-medium .at-icon{margin:6px;width:28px;height:28px}.at-custom-mobile-bar.at-shfs-small{height:40px}.at-custom-mobile-bar.at-shfs-small .at-custom-mobile-bar-counter{margin-top:3px}.at-custom-mobile-bar.at-shfs-small .at-custom-mobile-bar-count{font-size:1pc}.at-custom-mobile-bar.at-shfs-small .at-custom-mobile-bar-text{font-size:10px}.at-custom-mobile-bar.at-shfs-small .at-icon-wrapper{height:2pc;width:2pc}.at-custom-mobile-bar.at-shfs-small .at-icon{margin:4px;width:24px;height:24px}</style><style type="text/css">.at-custom-sidebar{top:20%;width:58px;position:fixed;text-align:center;z-index:100020;background:#fff;overflow:hidden;box-shadow:0 0 10px 0 rgba(0,0,0,.2);font:initial;line-height:normal;top:auto;bottom:0}.at-custom-sidebar.at-custom-sidebar-zindex-hide{z-index:-1!important}.at-custom-sidebar.atss-left{left:0;right:auto;float:left;border-radius:0 4px 4px 0}.at-custom-sidebar.atss-right{left:auto;right:0;float:right;border-radius:4px 0 0 4px}.at-custom-sidebar .at-custom-sidebar-btns{display:inline-block;text-align:center;padding-top:4px}.at-custom-sidebar .at-custom-sidebar-counter{margin-bottom:8px}.at-custom-sidebar .at-share-btn{display:inline-block;text-decoration:none;transition:none;box-sizing:content-box}.at-custom-sidebar .at-custom-sidebar-counter{font-family:Helvetica neue,arial;vertical-align:top;margin-left:4px;margin-right:4px;display:inline-block}.at-custom-sidebar .at-custom-sidebar-count{font-size:21px;line-height:1.25em;color:#222}.at-custom-sidebar .at-custom-sidebar-text{font-size:10px;line-height:1.25em;color:#888;letter-spacing:1px}.at-custom-sidebar .at-icon-wrapper{text-align:center;margin:0 4px}.at-custom-sidebar .at-icon{vertical-align:top;margin:9px;width:2pc;height:2pc}.at-custom-sidebar .at-icon-wrapper{position:relative}.at-custom-sidebar .at4-share-count,.at-custom-sidebar .at4-share-count-container{line-height:1pc;font-size:10px}.at-custom-sidebar .at4-share-count{text-indent:0;font-family:Arial,Helvetica Neue,Helvetica,sans-serif;font-weight:200;width:100%;height:1pc}.at-custom-sidebar .at4-share-count-anchor .at-icon{margin-top:3px}.at-custom-sidebar .at4-share-count-container{position:absolute;left:0;right:auto;top:auto;bottom:0;width:100%;color:#fff;background:inherit}</style><style type="text/css">.at-image-sharing-mobile-icon{position:absolute;background:#000 url(https://s7.addthis.com/static/44a36d35bafef33aa9455b7d3039a771.png) no-repeat top center;background-color:rgba(0,0,0,.9);background-image:url(https://s7.addthis.com/static/10db525181ee0bbe1a515001be1c7818.svg),none;border-radius:3px;width:50px;height:40px;top:-9999px;left:-9999px}.at-image-sharing-tool{display:block;position:absolute;text-align:center;z-index:9001;background:none;overflow:hidden;top:-9999px;left:-9999px;font:initial;line-height:0}.at-image-sharing-tool.addthis-animated{animation-duration:.15s}.at-image-sharing-tool.at-orientation-vertical .at-share-btn{display:block}.at-image-sharing-tool.at-orientation-horizontal .at-share-btn{display:inline-block}.at-image-sharing-tool.at-image-sharing-tool-size-big .at-icon{width:43px;height:43px}.at-image-sharing-tool.at-image-sharing-tool-size-mobile .at-share-btn{margin:0!important}.at-image-sharing-tool.at-image-sharing-tool-size-mobile .at-icon-wrapper{height:60px;width:100%;border-radius:0!important}.at-image-sharing-tool.at-image-sharing-tool-size-mobile .at-icon{max-width:100%;height:54px!important;width:54px!important}.at-image-sharing-tool .at-custom-shape.at-image-sharing-tool-btns{margin-right:8px;margin-bottom:8px}.at-image-sharing-tool .at-custom-shape .at-share-btn{margin-top:8px;margin-left:8px}.at-image-sharing-tool .at-share-btn{line-height:0;text-decoration:none;transition:none;box-sizing:content-box}.at-image-sharing-tool .at-icon-wrapper{text-align:center;height:100%;width:100%}.at-image-sharing-tool .at-icon{vertical-align:top;width:2pc;height:2pc;margin:3px}</style><style type="text/css">.at-expanding-share-button{box-sizing:border-box;position:fixed;z-index:9999}.at-expanding-share-button[data-position=bottom-right]{bottom:10px;right:10px}.at-expanding-share-button[data-position=bottom-right] .at-expanding-share-button-toggle-bg,.at-expanding-share-button[data-position=bottom-right] .at-expanding-share-button-toggle-btn[data-name]:after,.at-expanding-share-button[data-position=bottom-right] .at-icon-wrapper,.at-expanding-share-button[data-position=bottom-right] [data-name]:after{float:right}.at-expanding-share-button[data-position=bottom-right] [data-name]:after{margin-right:10px}.at-expanding-share-button[data-position=bottom-right] .at-expanding-share-button-toggle-btn[data-name]:after{margin-right:5px}.at-expanding-share-button[data-position=bottom-right] .at-icon-wrapper{margin-right:-3px}.at-expanding-share-button[data-position=bottom-left]{bottom:10px;left:10px}.at-expanding-share-button[data-position=bottom-left] .at-expanding-share-button-toggle-bg,.at-expanding-share-button[data-position=bottom-left] .at-expanding-share-button-toggle-btn[data-name]:after,.at-expanding-share-button[data-position=bottom-left] .at-icon-wrapper,.at-expanding-share-button[data-position=bottom-left] [data-name]:after{float:left}.at-expanding-share-button[data-position=bottom-left] [data-name]:after{margin-left:10px}.at-expanding-share-button[data-position=bottom-left] .at-expanding-share-button-toggle-btn[data-name]:after{margin-left:5px}.at-expanding-share-button *,.at-expanding-share-button :after,.at-expanding-share-button :before{box-sizing:border-box}.at-expanding-share-button .at-expanding-share-button-services-list{display:none;list-style:none;margin:0 5px;overflow:visible;padding:0}.at-expanding-share-button .at-expanding-share-button-services-list>li{display:block;height:45px;position:relative;overflow:visible}.at-expanding-share-button .at-expanding-share-button-toggle-btn,.at-expanding-share-button .at-share-btn{transition:.1s;text-decoration:none}.at-expanding-share-button .at-share-btn{display:block;height:40px;padding:0 3px 0 0}.at-expanding-share-button .at-expanding-share-button-toggle-btn{position:relative;overflow:auto}.at-expanding-share-button .at-expanding-share-button-toggle-btn.at-expanding-share-button-hidden[data-name]:after{display:none}.at-expanding-share-button .at-expanding-share-button-toggle-bg{box-shadow:0 2px 4px 0 rgba(0,0,0,.3);border-radius:50%;position:relative}.at-expanding-share-button .at-expanding-share-button-toggle-bg>span{background-image:url("data:image/svg+xml,%3Csvg%20width%3D%2232px%22%20height%3D%2232px%22%20viewBox%3D%220%200%2032%2032%22%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Ctitle%3Eshare%3C%2Ftitle%3E%3Cg%20stroke%3D%22none%22%20stroke-width%3D%221%22%20fill%3D%22none%22%20fill-rule%3D%22evenodd%22%3E%3Cg%20fill%3D%22%23FFFFFF%22%3E%3Cpath%20d%3D%22M26%2C13.4285714%20C26%2C13.6220248%2025.9293162%2C13.7894338%2025.7879464%2C13.9308036%20L20.0736607%2C19.6450893%20C19.932291%2C19.786459%2019.7648819%2C19.8571429%2019.5714286%2C19.8571429%20C19.3779752%2C19.8571429%2019.2105662%2C19.786459%2019.0691964%2C19.6450893%20C18.9278267%2C19.5037195%2018.8571429%2C19.3363105%2018.8571429%2C19.1428571%20L18.8571429%2C16.2857143%20L16.3571429%2C16.2857143%20C15.6279725%2C16.2857143%2014.9750773%2C16.3080355%2014.3984375%2C16.3526786%20C13.8217977%2C16.3973217%2013.2488868%2C16.477306%2012.6796875%2C16.5926339%20C12.1104882%2C16.7079619%2011.6157015%2C16.8660704%2011.1953125%2C17.0669643%20C10.7749235%2C17.2678581%2010.3824423%2C17.5264121%2010.0178571%2C17.8426339%20C9.65327199%2C18.1588557%209.35565592%2C18.534596%209.125%2C18.9698661%20C8.89434408%2C19.4051361%208.71391434%2C19.9203839%208.58370536%2C20.515625%20C8.45349637%2C21.1108661%208.38839286%2C21.7842224%208.38839286%2C22.5357143%20C8.38839286%2C22.9449425%208.40699386%2C23.4025272%208.44419643%2C23.9084821%20C8.44419643%2C23.9531252%208.45349693%2C24.0405499%208.47209821%2C24.1707589%20C8.4906995%2C24.3009679%208.5%2C24.3995532%208.5%2C24.4665179%20C8.5%2C24.5781256%208.46837829%2C24.6711306%208.40513393%2C24.7455357%20C8.34188956%2C24.8199408%208.25446484%2C24.8571429%208.14285714%2C24.8571429%20C8.02380893%2C24.8571429%207.9196433%2C24.7938994%207.83035714%2C24.6674107%20C7.77827355%2C24.6004461%207.72991094%2C24.5186017%207.68526786%2C24.421875%20C7.64062478%2C24.3251483%207.59040206%2C24.2135423%207.53459821%2C24.0870536%20C7.47879436%2C23.9605648%207.43973225%2C23.87128%207.41741071%2C23.8191964%20C6.47246551%2C21.6986501%206%2C20.0208395%206%2C18.7857143%20C6%2C17.3050521%206.19717065%2C16.0662252%206.59151786%2C15.0691964%20C7.79688103%2C12.0706695%2011.0520568%2C10.5714286%2016.3571429%2C10.5714286%20L18.8571429%2C10.5714286%20L18.8571429%2C7.71428571%20C18.8571429%2C7.52083237%2018.9278267%2C7.35342333%2019.0691964%2C7.21205357%20C19.2105662%2C7.07068382%2019.3779752%2C7%2019.5714286%2C7%20C19.7648819%2C7%2019.932291%2C7.07068382%2020.0736607%2C7.21205357%20L25.7879464%2C12.9263393%20C25.9293162%2C13.067709%2026%2C13.2351181%2026%2C13.4285714%20L26%2C13.4285714%20Z%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E");background-position:center center;background-repeat:no-repeat;transition:transform .4s ease;border-radius:50%;display:block}.at-expanding-share-button .at-icon-wrapper{box-shadow:0 2px 4px 0 rgba(0,0,0,.3);border-radius:50%;display:inline-block;height:40px;line-height:40px;text-align:center;width:40px}.at-expanding-share-button .at-icon{display:inline-block;height:34px;margin:3px 0;vertical-align:top;width:34px}.at-expanding-share-button [data-name]:after{box-shadow:0 2px 4px 0 rgba(0,0,0,.3);transform:translate(0, -50%);transition:.4s;background-color:#fff;border-radius:3px;color:#666;content:attr(data-name);font-family:Helvetica Neue,Helvetica,Arial,sans-serif;font-size:9pt;line-height:9pt;font-weight:500;opacity:0;padding:3px 5px;position:relative;top:20px;white-space:nowrap}.at-expanding-share-button.at-expanding-share-button-show-icons .at-expanding-share-button-services-list{display:block}.at-expanding-share-button.at-expanding-share-button-animate-in .at-expanding-share-button-toggle-bg>span{transform:rotate(270deg);background-image:url("data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20viewBox%3D%220%200%2032%2032%22%3E%3Cg%3E%3Cpath%20d%3D%22M18%2014V8h-4v6H8v4h6v6h4v-6h6v-4h-6z%22%20fill-rule%3D%22evenodd%22%20fill%3D%22white%22%3E%3C%2Fpath%3E%3C%2Fg%3E%3C%2Fsvg%3E");background-position:center center;background-repeat:no-repeat}.at-expanding-share-button.at-expanding-share-button-animate-in [data-name]:after{opacity:1}.at-expanding-share-button.at-hide-label [data-name]:after{display:none}.at-expanding-share-button.at-expanding-share-button-desktop .at-expanding-share-button-toggle{height:50px}.at-expanding-share-button.at-expanding-share-button-desktop .at-icon-wrapper:hover{box-shadow:0 2px 5px 0 rgba(0,0,0,.5)}.at-expanding-share-button.at-expanding-share-button-desktop .at-expanding-share-button-toggle-bg{height:50px;line-height:50px;width:50px}.at-expanding-share-button.at-expanding-share-button-desktop .at-expanding-share-button-toggle-bg>span{height:50px;width:50px}.at-expanding-share-button.at-expanding-share-button-desktop .at-expanding-share-button-toggle-bg:after{box-shadow:0 2px 5px 0 rgba(0,0,0,.2);transition:opacity .2s ease;border-radius:50%;content:'';height:100%;opacity:0;position:absolute;top:0;left:0;width:100%}.at-expanding-share-button.at-expanding-share-button-desktop .at-expanding-share-button-toggle-bg:hover:after{opacity:1}.at-expanding-share-button.at-expanding-share-button-desktop .at-expanding-share-button-toggle-btn[data-name]:after{top:25px}.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-services-list{margin:0}.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-toggle-btn,.at-expanding-share-button.at-expanding-share-button-mobile .at-share-btn{outline:0}.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-toggle{height:40px;-webkit-tap-highlight-color:transparent}.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-toggle-bg,.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-toggle-bg span{height:40px;line-height:40px;width:40px}.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-click-flash{transform:scale(0);transition:transform ease,opacity ease-in;background-color:hsla(0,0%,100%,.3);border-radius:50%;height:40px;opacity:1;position:absolute;width:40px;z-index:10000}.at-expanding-share-button.at-expanding-share-button-mobile .at-expanding-share-button-click-flash.at-expanding-share-button-click-flash-animate{transform:scale(1);opacity:0}.at-expanding-share-button.at-expanding-share-button-mobile+.at-expanding-share-button-mobile-overlay{transition:opacity ease;bottom:0;background-color:hsla(0,0%,87%,.7);display:block;height:auto;left:0;opacity:0;position:fixed;right:0;top:0;width:auto;z-index:9998}.at-expanding-share-button.at-expanding-share-button-mobile+.at-expanding-share-button-mobile-overlay.at-expanding-share-button-hidden{height:0;width:0;z-index:-10000}.at-expanding-share-button.at-expanding-share-button-mobile.at-expanding-share-button-animate-in+.at-expanding-share-button-mobile-overlay{transition:opacity ease;opacity:1}</style><style type="text/css">.at-tjin-element .at300b,.at-tjin-element .at300m{display:inline-block;width:auto;padding:0;margin:0 2px 5px;outline-offset:-1px;transition:all .2s ease-in-out}.at-tjin-element .at300b:focus,.at-tjin-element .at300b:hover,.at-tjin-element .at300m:focus,.at-tjin-element .at300m:hover{transform:translateY(-4px)}.at-tjin-element .addthis_tjin_label{display:none}.at-tjin-element .addthis_vertical_style .at300b,.at-tjin-element .addthis_vertical_style .at300m{display:block}.at-tjin-element .addthis_vertical_style .at300b .addthis_tjin_label,.at-tjin-element .addthis_vertical_style .at300b .at-icon-wrapper,.at-tjin-element .addthis_vertical_style .at300m .addthis_tjin_label,.at-tjin-element .addthis_vertical_style .at300m .at-icon-wrapper{display:inline-block;vertical-align:middle;margin-right:5px}.at-tjin-element .addthis_vertical_style .at300b:focus,.at-tjin-element .addthis_vertical_style .at300b:hover,.at-tjin-element .addthis_vertical_style .at300m:focus,.at-tjin-element .addthis_vertical_style .at300m:hover{transform:none}.at-tjin-element .at-tjin-btn{margin:0 5px 5px 0;padding:0;outline-offset:-1px;display:inline-block;box-sizing:content-box;transition:all .2s ease-in-out}.at-tjin-element .at-tjin-btn:focus,.at-tjin-element .at-tjin-btn:hover{transform:translateY(-4px)}.at-tjin-element .at-tjin-title{margin:0 0 15px}</style><style type="text/css">#addthissmartlayerscssready{color:#bada55!important}.addthis-smartlayers,div#at4-follow,div#at4-share,div#at4-thankyou,div#at4-whatsnext{padding:0;margin:0}#at4-follow-label,#at4-share-label,#at4-whatsnext-label,.at4-recommended-label.hidden{padding:0;border:none;background:none;position:absolute;top:0;left:0;height:0;width:0;overflow:hidden;text-indent:-9999em}.addthis-smartlayers .at4-arrow:hover{cursor:pointer}.addthis-smartlayers .at4-arrow:after,.addthis-smartlayers .at4-arrow:before{content:none}a.at4-logo{background:url(data:image/gif;base64,R0lGODlhBwAHAJEAAP9uQf///wAAAAAAACH5BAkKAAIALAAAAAAHAAcAAAILFH6Ge8EBH2MKiQIAOw==) no-repeat left center}.at4-minimal a.at4-logo{background:url(data:image/gif;base64,R0lGODlhBwAHAJEAAP9uQf///wAAAAAAACH5BAkKAAIALAAAAAAHAAcAAAILFH6Ge8EBH2MKiQIAOw==) no-repeat left center!important}button.at4-closebutton{position:absolute;top:0;right:0;padding:0;margin-right:10px;cursor:pointer;background:transparent;border:0;-webkit-appearance:none;font-size:19px;line-height:1;color:#000;text-shadow:0 1px 0 #fff;opacity:.2}button.at4-closebutton:hover{color:#000;text-decoration:none;cursor:pointer;opacity:.5}div.at4-arrow{background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAAoCAYAAABpYH0BAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAV1JREFUeNrsmesOgyAMhQfxwfrofTM3E10ME2i5Oeppwr9a5OMUCrh1XV+wcvNAAIAA+BiAzrmtUWln27dbjEcC3AdODfo0BdEPhmcO4nIDvDNELi2jggk4/k8dT7skfeKzWIEd4VUpMQKvNB7X+OZSmAZkATWC1xvipbpnLmOosbJZC08CkAeA4E6qFUEMwLAGnlSBPCE8lW8CYnZTcimH2HoT7kSFOx5HBmCnDhTIu1p5s98G+QZrxGPhZVMY1vgyAQaAAAiAAAgDQACcBOD+BvJtBWfRy7NpJK5tBe4FNzXokywV734wPHMQlxvgnSGyNoUP/2ACjv/7iSeYKO3YWKzAjvCqlBiBVxqPa3ynexNJwOsN8TJbzL6JNIYYXWpMv4lIIAZgWANPqkCeEJ7KNwExu8lpLlSpAVQarO77TyKdBsyRPuwV0h0gmoGnTWFYzVkYBoAA+I/2FmAAt6+b5XM9mFkAAAAASUVORK5CYII=);background-repeat:no-repeat;width:20px;height:20px;margin:0;padding:0;overflow:hidden;text-indent:-9999em;text-align:left;cursor:pointer}#at4-recommendedpanel-outer-container .at4-arrow.at-right,div.at4-arrow.at-right{background-position:-20px 0}#at4-recommendedpanel-outer-container .at4-arrow.at-left,div.at4-arrow.at-left{background-position:0 0}div.at4-arrow.at-down{background-position:-60px 0}div.at4-arrow.at-up{background-position:-40px 0}.ats-dark div.at4-arrow.at-right{background-position:-20px -20px}.ats-dark div.at4-arrow.at-left{background-position:0 -20px}.ats-dark div.at4-arrow.at-down{background-position:-60px -20px}.ats-dark div.at4-arrow.at-up{background-position:-40px -20}.at4-opacity-hidden{opacity:0!important}.at4-opacity-visible{opacity:1!important}.at4-visually-hidden{position:absolute;clip:rect(1px,1px,1px,1px);padding:0;border:0;overflow:hidden}.at4-hidden-off-screen,.at4-hidden-off-screen *{position:absolute!important;top:-9999px!important;left:-9999px!important}.at4-show{display:block!important;opacity:1!important}.at4-show-content{opacity:1!important;visibility:visible}.at4-hide{display:none!important;opacity:0!important}.at4-hide-content{opacity:0!important;visibility:hidden}.at4-visible{display:block!important;opacity:0!important}.at-wordpress-hide{display:none!important;opacity:0!important}.addthis-animated{animation-fill-mode:both;animation-timing-function:ease-out;animation-duration:.3s}.slideInDown.addthis-animated,.slideInLeft.addthis-animated,.slideInRight.addthis-animated,.slideInUp.addthis-animated,.slideOutDown.addthis-animated,.slideOutLeft.addthis-animated,.slideOutRight.addthis-animated,.slideOutUp.addthis-animated{animation-duration:.4s}@keyframes fadeIn{0%{opacity:0}to{opacity:1}}.fadeIn{animation-name:fadeIn}@keyframes fadeInUp{0%{opacity:0;transform:translateY(20px)}to{opacity:1;transform:translateY(0)}}.fadeInUp{animation-name:fadeInUp}@keyframes fadeInDown{0%{opacity:0;transform:translateY(-20px)}to{opacity:1;transform:translateY(0)}}.fadeInDown{animation-name:fadeInDown}@keyframes fadeInLeft{0%{opacity:0;transform:translateX(-20px)}to{opacity:1;transform:translateX(0)}}.fadeInLeft{animation-name:fadeInLeft}@keyframes fadeInRight{0%{opacity:0;transform:translateX(20px)}to{opacity:1;transform:translateX(0)}}.fadeInRight{animation-name:fadeInRight}@keyframes fadeOut{0%{opacity:1}to{opacity:0}}.fadeOut{animation-name:fadeOut}@keyframes fadeOutUp{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(-20px)}}.fadeOutUp{animation-name:fadeOutUp}@keyframes fadeOutDown{0%{opacity:1;transform:translateY(0)}to{opacity:0;transform:translateY(20px)}}.fadeOutDown{animation-name:fadeOutDown}@keyframes fadeOutLeft{0%{opacity:1;transform:translateX(0)}to{opacity:0;transform:translateX(-20px)}}.fadeOutLeft{animation-name:fadeOutLeft}@keyframes fadeOutRight{0%{opacity:1;transform:translateX(0)}to{opacity:0;transform:translateX(20px)}}.fadeOutRight{animation-name:fadeOutRight}@keyframes slideInUp{0%{transform:translateY(1500px)}0%,to{opacity:1}to{transform:translateY(0)}}.slideInUp{animation-name:slideInUp}.slideInUp.addthis-animated{animation-duration:.4s}@keyframes slideInDown{0%{transform:translateY(-850px)}0%,to{opacity:1}to{transform:translateY(0)}}.slideInDown{animation-name:slideInDown}@keyframes slideOutUp{0%{transform:translateY(0)}0%,to{opacity:1}to{transform:translateY(-250px)}}.slideOutUp{animation-name:slideOutUp}@keyframes slideOutUpFast{0%{transform:translateY(0)}0%,to{opacity:1}to{transform:translateY(-1250px)}}#at4m-menu.slideOutUp{animation-name:slideOutUpFast}@keyframes slideOutDown{0%{transform:translateY(0)}0%,to{opacity:1}to{transform:translateY(350px)}}.slideOutDown{animation-name:slideOutDown}@keyframes slideOutDownFast{0%{transform:translateY(0)}0%,to{opacity:1}to{transform:translateY(1250px)}}#at4m-menu.slideOutDown{animation-name:slideOutDownFast}@keyframes slideInLeft{0%{opacity:0;transform:translateX(-850px)}to{transform:translateX(0)}}.slideInLeft{animation-name:slideInLeft}@keyframes slideInRight{0%{opacity:0;transform:translateX(1250px)}to{transform:translateX(0)}}.slideInRight{animation-name:slideInRight}@keyframes slideOutLeft{0%{transform:translateX(0)}to{opacity:0;transform:translateX(-350px)}}.slideOutLeft{animation-name:slideOutLeft}@keyframes slideOutRight{0%{transform:translateX(0)}to{opacity:0;transform:translateX(350px)}}.slideOutRight{animation-name:slideOutRight}.at4win{margin:0 auto;background:#fff;border:1px solid #ebeced;width:25pc;box-shadow:0 0 10px rgba(0,0,0,.3);border-radius:8px;font-family:helvetica neue,helvetica,arial,sans-serif;text-align:left;z-index:9999}.at4win .at4win-header{position:relative;border-bottom:1px solid #f2f2f2;background:#fff;height:49px;-webkit-border-top-left-radius:8px;-webkit-border-top-right-radius:8px;-moz-border-radius-topleft:8px;-moz-border-radius-topright:8px;border-top-left-radius:8px;border-top-right-radius:8px;cursor:default}.at4win .at4win-header .at-h3,.at4win .at4win-header h3{height:49px;line-height:49px;margin:0 50px 0 0;padding:1px 0 0;margin-left:20px;font-family:helvetica neue,helvetica,arial,sans-serif;font-size:1pc;font-weight:700;text-shadow:0 1px #fff;color:#333}.at4win .at4win-header .at-h3 img,.at4win .at4win-header h3 img{display:inline-block;margin-right:4px}.at4win .at4win-header .at4-close{display:block;position:absolute;top:0;right:0;background:url("data:image/gif;base64,R0lGODlhFAAUAIABAAAAAP///yH5BAEAAAEALAAAAAAUABQAAAIzBIKpG+YMm5Enpodw1HlCfnkKOIqU1VXk55goVb2hi7Y0q95lfG70uurNaqLgTviyyUoFADs=") no-repeat center center;background-repeat:no-repeat;background-position:center center;border-left:1px solid #d2d2d1;width:49px;height:49px;line-height:49px;overflow:hidden;text-indent:-9999px;text-shadow:none;cursor:pointer;opacity:.5;border:0;transition:opacity .15s ease-in}.at4win .at4win-header .at4-close::-moz-focus-inner{border:0;padding:0}.at4win .at4win-header .at4-close:hover{opacity:1;background-color:#ebeced;border-top-right-radius:7px}.at4win .at4win-content{position:relative;background:#fff;min-height:220px}#at4win-footer{position:relative;background:#fff;border-top:1px solid #d2d2d1;-webkit-border-bottom-right-radius:8px;-webkit-border-bottom-left-radius:8px;-moz-border-radius-bottomright:8px;-moz-border-radius-bottomleft:8px;border-bottom-right-radius:8px;border-bottom-left-radius:8px;height:11px;line-height:11px;padding:5px 20px;font-size:11px;color:#666;-ms-box-sizing:content-box;-o-box-sizing:content-box;box-sizing:content-box}#at4win-footer a{margin-right:10px;text-decoration:none;color:#666}#at4win-footer a:hover{text-decoration:none;color:#000}#at4win-footer a.at4-logo{top:5px;padding-left:10px}#at4win-footer a.at4-privacy{position:absolute;top:5px;right:10px;padding-right:14px}.at4win.ats-dark{border-color:#555;box-shadow:none}.at4win.ats-dark .at4win-header{background:#1b1b1b;-webkit-border-top-left-radius:6px;-webkit-border-top-right-radius:6px;-moz-border-radius-topleft:6px;-moz-border-radius-topright:6px;border-top-left-radius:6px;border-top-right-radius:6px}.at4win.ats-dark .at4win-header .at4-close{background:url("data:image/gif;base64,iVBORw0KGgoAAAANSUhEUgAAABQAAAAUCAYAAACNiR0NAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNui8sowAAAAWdEVYdENyZWF0aW9uIFRpbWUAMTEvMTMvMTKswDp5AAAAd0lEQVQ4jb2VQRLAIAgDE///Z3qqY1FAhalHMCsCIkVEAIAkkVgvp2lDBgYAnAyHkWotLccNrEd4A7X2TqIdqLfnWBAdaF5rJdyJfjtPH5GT37CaGhoVq3nOm/XflUuLUto2pY1d+vRKh0Pp+MrAVtDe2JkvYNQ+jVSEEFmOkggAAAAASUVORK5CYII=") no-repeat center center;background-image:url(https://s7.addthis.com/static/fb08f6d50887bd0caacc86a62bcdcf68.svg),none;border-color:#333}.at4win.ats-dark .at4win-header .at4-close:hover{background-color:#000}.at4win.ats-dark .at4win-header .at-h3,.at4win.ats-dark .at4win-header h3{color:#fff;text-shadow:0 1px #000}.at4win.ats-gray .at4win-header{background:#fff;border-color:#d2d2d1;-webkit-border-top-left-radius:6px;-webkit-border-top-right-radius:6px;-moz-border-radius-topleft:6px;-moz-border-radius-topright:6px;border-top-left-radius:6px;border-top-right-radius:6px}.at4win.ats-gray .at4win-header a.at4-close{border-color:#d2d2d1}.at4win.ats-gray .at4win-header a.at4-close:hover{background-color:#ebeced}.at4win.ats-gray #at4win-footer{border-color:#ebeced}.at4win .clear{clear:both}.at4win ::selection{background:#fe6d4c;color:#fff}.at4win ::-moz-selection{background:#fe6d4c;color:#fff}.at4-icon-fw{display:inline-block;background-repeat:no-repeat;background-position:0 0;margin:0 5px 0 0;overflow:hidden;text-indent:-9999em;cursor:pointer;padding:0;border-radius:50%;-moz-border-radius:50%;-webkit-border-radius:50%}.at44-follow-container a.aticon{height:2pc;margin:0 5px 5px 0}.at44-follow-container .at4-icon-fw{margin:0}</style><style id="at4-share-offset" type="text/css" media="screen">#at4-share,#at4-soc {top:20% !important;bottom:auto}</style><script src="./Lord of the Mysteries - Index - Wuxiaworld_files/alfalfalfa.0823c767a3bc925f628afd9bed26c958.js" async="" charset="UTF-8"></script></head>
<body>
    <div id="wrapper">
<script>login();</script><div style="display:none">
</div>
<div class="ywtop"><div class="ywtop_con"><div class="ywtop_sethome">wuxiaworld</div>
		<div class="ywtop_addfavorite">&nbsp;&nbsp;<font color="red">Mobile Access: m.wuxiaworld.co</font></div>
<div class="nri"><form name="mylogin" id="mylogin" method="post" action="https://www.wuxiaworld.co/Login.php?action=login&amp;usecookie=1&amp;jumpurl=" https:="" www.wuxiaworld.co="" lord-of-the-mysteries=""><div class="frii"></div><div class="ccc"><div class="txtt"><a href="https://www.wuxiaworld.co/Login.php">Login</a></div><div class="txtt"><a href="https://www.wuxiaworld.co/register.php">Sign Up</a></div><div class="txtt"><a href="https://www.wuxiaworld.co/feed/">Rss</a></div></div></form></div></div></div><div style="display:none"><script type="text/javascript" src="./Lord of the Mysteries - Index - Wuxiaworld_files/z_stat.php"></script><script src="./Lord of the Mysteries - Index - Wuxiaworld_files/core.php" charset="utf-8" type="text/javascript"></script><a href="https://www.cnzz.com/stat/website.php?web_id=1277622111" target="_blank" title="站长统计">站长统计</a></div>

<div class="header">
    <div class="header_logo"><a href="http://www.wuxiaworld.co/">Wuxiaworld</a></div>
    <div class="header_search"><script>search();</script></div>
    <div class="userpanel"><script>banner();</script></div>
</div>
<div class="clear"></div>
<div class="nav">
<ul><li><a href="https://www.wuxiaworld.co/">Home</a></li><li><a rel="nofollow" href="https://www.wuxiaworld.co/bookshelf.php">My bookshelf</a></li><li><a href="https://www.wuxiaworld.co/fantasy/">Fantasy</a></li><li><a href="https://www.wuxiaworld.co/xianxia/">Xianxia</a></li>
<li><a href="https://www.wuxiaworld.co/romantic/">Romantic</a></li><li><a href="https://www.wuxiaworld.co/historical/">Historical</a></li><li><a href="https://www.wuxiaworld.co/sci-fi/">Sci-fi</a></li><li><a href="https://www.wuxiaworld.co/game/">Game</a></li>
<li><a href="https://www.wuxiaworld.co/top/">Top</a></li><li><a href="https://www.wuxiaworld.co/completed/">Completed</a></li><li><a rel="nofollow" href="https://www.wuxiaworld.co/reading.php">Reading history</a></li>&gt;<li><a href="https://www.wuxiaworld.co/all/">All</a></li></ul>
</div>
<div class="dahengfu"><script type="text/javascript">list1();</script></div>
        <div class="box_con">
            <div class="con_top">
                <a href="https://www.wuxiaworld.co/">Wuxiaworld</a>  &gt; Fantasy  &gt; Lord of the Mysteries
            </div>
            <div id="maininfo">
                <div id="info">
                    <h1>Lord of the Mysteries</h1>
                    <p>Author：Cuttlefish That Loves Diving</p>
                    <p>Action：<a rel="nofollow" href="javascript:;" onclick="addBookCase(1887);">Add bookshelf</a>、<a rel="nofollow" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/#bottom">To Bottom</a>、<a rel="nofollow" title="Lord of the Mysteries RSS" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/feed/"><font color="red">RSS</font></a></p>
                    <p>UpdateTime：5/9/2020 8:02:03 AM</p>
                    <p>Updates：<a href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2726549.html">1205 A Thousand-Year-Old Trap</a></p>
                </div>
                <div id="intro">
                   With the rising tide of steam power and machinery, who can come close to being a Beyonder? Shrouded in the fog of history and darkness, who or what is the lurking evil that murmurs into our ears?<br><br><br><br>Waking up to be faced with a string of mysteries, Zhou Mingrui finds himself reincarnated as Klein Moretti in an alternate Victorian era world where he sees a world filled with machine...
                </div>
            </div>
            <div id="sidebar">
                <div id="fmimg">
                    <img alt="Lord of the Mysteries" src="./Lord of the Mysteries - Index - Wuxiaworld_files/Lord-of-the-Mysteries.jpg" width="120" height="150"></div>
            </div>
        </div>
        <div class="box_con">
            <div id="list">
                <dl>
                    
                    <dt>《Lord of the Mysteries》 Volume 1</dt>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486806.html">1 Crimson</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486807.html">2 Situation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486808.html">3 Melissa</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486809.html">4 Divination</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486810.html">5 Ritual</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486811.html">6 Beyonder</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2227050.html">7 Code Names</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486813.html">8 A New Era</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486814.html">9 The Notebook</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486815.html">10 The Norm</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486816.html">11 Real Culinary Skills</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486817.html">12 Here Again</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486818.html">13 Nighthawk</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486819.html">14 The Medium</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486820.html">15 The Invitation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486821.html">16 Rat-baiting With Dogs</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486822.html">17 Special Operations Departmen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486823.html">18 Origin and Cause</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486824.html">19 Sealed Artifacts</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486825.html">20 The Forgetful Dunn</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486826.html">21 An Old Friend In A Different World</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486827.html">22 Starting Sequence</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486828.html">23 Side Arm</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486829.html">24 Penny-pincher</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486830.html">25 Cathedral</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486831.html">26 Practice</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617375.html">27 Siblings' Dinner</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486833.html">28 Secret Order</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486834.html">29 "Jobs" and Rentals Are Serious Business</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486835.html">30 Brand New Beginning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486836.html">31 Potion</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486837.html">32 Spirit Vision</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486838.html">33 Switch</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486839.html">34 Advance Paymen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486840.html">35 Exchange of Information</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486841.html">36 A Simple Question</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486842.html">37 The Club</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486843.html">38 Novice Hobbyis</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486844.html">39 Interesting Trick</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1486845.html">40 Mysticism Curriculum</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1487132.html">41 Audrey and Her Susie</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1494434.html">42 Butler Klee</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1494435.html">43 Search</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1495126.html">44 Fate</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1495127.html">45 Returning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1496033.html">46 Portrai</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617376.html">47 Old Neil's Lack of Money</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1496566.html">48 Hanass Vincen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1496567.html">49 Divination Ar</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617377.html">50 Old Neil's Method of Repaymen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1497532.html">51 The Grounded Ritualistic Magic</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1498514.html">52 Spectator</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1498515.html">53 Listener</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1499610.html">54 The First Divination Requester</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1500392.html">55 Revelation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1500393.html">56 Escape from the Sea</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1501141.html">57 Organization and Summary</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1501142.html">58 A Train of Though</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617378.html">59 Roselle's Origins</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1501744.html">60 Second Blasphemy Slate</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1501745.html">61 Strange Symbol</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617379.html">62 The Seer's Suggestion</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1503984.html">63 Dream Interpretation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1503985.html">64 Instigator</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1504718.html">65 Beyonder Information</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1504719.html">66 Demoness Sec</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1505176.html">67 Response</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1505376.html">68 Monster</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1506280.html">69 Protection Amule</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617380.html">70 2-049's Arrival</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1507145.html">71 Sluggish Phenomenon</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1507146.html">72 Tracking</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1508396.html">73 First Battle</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1508397.html">74 Ray Bieber</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1509057.html">75 Saving himself</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1509058.html">76 Dealing With The Aftermath</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1509877.html">77 Remnant Items</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1509878.html">78 Trauma</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1509879.html">79 Another Murmuring</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1509880.html">80 Banquet Invitation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1511008.html">81 Finally Meeting</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1511009.html">82 Herb Store</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1512434.html">83 Carving</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1512435.html">84 Elizabeth</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1512436.html">85 Urgency</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1513187.html">86 Prayer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1513188.html">87 Exhortation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1513887.html">88 Repor</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1513888.html">89 A Simple Mission</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1513889.html">90 Findings By Sigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1514224.html">91 Solution</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1515189.html">92 Psychological Exper</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1515190.html">93 New Diary Page</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1515830.html">94 Hidden Sage</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1515831.html">95 The Supplican</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617381.html">96 Daly's Guess</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1516635.html">97 Combat Teacher</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1521041.html">98 Mr. Azik</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1521042.html">99 Red Chimney</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1521043.html">100 Interpreting Symbols</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1521044.html">101 Unexpected Clue</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1521045.html">102 Cloth Merchan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1521608.html">103 Doing As the Heart Willed</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1521609.html">104 Mr. Z</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1522291.html">105 Spirit Channeling</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1522292.html">106 Artist Klein</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1522972.html">107 Fors</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1525265.html">108 Deep Into The Nigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1525758.html">109 Deduction</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1527702.html">110 Confirmation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1528396.html">111 Letting Slip</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617382.html">112 Azik's Explanation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1529060.html">113 Reques</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1529061.html">114 The Standards of a Member</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1529866.html">115 Chea</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617383.html">116 Lanevus's Child</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1530435.html">117 Contac</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1530436.html">118 Augus</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1531101.html">119 The True Lower Stree</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1531102.html">120 Workhouse</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617384.html">121 Leonard's Hypothesis</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1532118.html">122 Target Building</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1533487.html">123 Beyonder Battle</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1533488.html">124 Wrapping Up Work</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1534853.html">125 Bold Idea</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617385.html">126 Divination Isn't All-Powerful</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1534855.html">127 Laying the Foundations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1535571.html">128 The Impoverished Fool</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1535572.html">129 Rampager</a></dd>
                    
                    
                    <dt>《Lord of the Mysteries》 Volume 2</dt>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617386.html">130 Backlund's Secret Gathering</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1536348.html">131 Transaction</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1536349.html">132 Meeting the Monster Again</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1536369.html">133 Expensive Charms</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617387.html">134 It's Been More Than A Minute</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1539486.html">135 Portrait of a Baron</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1539487.html">136 The Stumped Klein</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1540085.html">137 City of Silver</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1540086.html">138 Giant Pathway</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1541027.html">139 Studying 3-0782</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1541028.html">140 Expert At Courting Death</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1542279.html">141 Nightmare</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1542280.html">142 Association</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617388.html">143 The Fool's Real-time Translator</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1542941.html">144 Three-Way Deal</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1543521.html">145 Request for Cooperation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1543522.html">146 Creeping Hunger</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1544836.html">147 Night Visitor</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1544837.html">148 Messenger</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1545586.html">149 Direct Hin</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617389.html">150 Azik's Discovery</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617390.html">151 Klein's Reques</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1546484.html">152 Nice Attemp</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1546523.html">153 Final Act of Laying the Foundation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1547217.html">154 Sharing "Experience"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1547218.html">155 Urgent Meeting</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1547674.html">156 Melissa Who Takes the Long View</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1547675.html">157 Item of His Dreams</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1561757.html">158 Preparedness Averts Peril</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1561758.html">159 Bestowment and Sacrifice</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1569892.html">160 Seizing the Opportunity</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1569893.html">161 Inverted Mausoleum</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1572682.html">162 Intense Sunligh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1572683.html">163 Various Signs</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1578603.html">164 Miserable Wretches</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1578604.html">165 Epitaph</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1583113.html">166 Examination</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1583114.html">167 Holy Artifac</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1587107.html">168 Clown Potion</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1587108.html">169 New Abilities</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1592639.html">170 Copper Whistle</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1592640.html">171 Promotion and Pay Raise</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1596751.html">172 "Autopsy"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1596752.html">173 Zombiefication</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1600486.html">174 Madam Sharon</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1600487.html">175 Deduction</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1604102.html">176 Letter</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1604103.html">177 Sudden Turn of Events</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1607858.html">178 The Subsequent Ideas</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1607859.html">179 Praising Mr. Fool</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1612377.html">180 A Smart Person Always Overthinks</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1612400.html">181 Different State</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1616122.html">182 Wanderer Klein</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1616123.html">183 A Lesson on Mediumship</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1621350.html">184 Behind the Gate</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1621351.html">185 Spiritual World</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1621966.html">186 The Handsome Captain</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617391.html">187 Azik's Warning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1625804.html">188 Ball</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1625805.html">189 Prayers and Replies</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1626242.html">190 The Assortment of Abilities</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1626243.html">191 Unclear Motives</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1627942.html">192 Attention</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1627943.html">193 Coming To A Close</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1630620.html">194 Infiltration</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1630621.html">195 "Lockpicking Expert" Klein</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1634206.html">196 Spirit Medium Mirror</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1634207.html">197 Operation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1637015.html">198 Appropriating Uniqueness</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1637016.html">199 Successful Toss of the Die</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1641641.html">200 The Demoness of Pleasure</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1641642.html">201 Inquiry</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1645533.html">202 Confirming the Situation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1645535.html">203 Mutan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1649205.html">204 Visitor</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1649206.html">205 Urgent Arrangemen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1652921.html">206 Grade 2 Sealed Artifacts</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1652922.html">207 Guardian</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1656718.html">208 Cry</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1656719.html">209 Ligh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1660419.html">210 Story</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1660420.html">211 Funeral</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1664162.html">212 Avenger</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1664163.html">213 Another Look</a></dd>
                    
                    
                    <dt>《Lord of the Mysteries》 Volume 3</dt>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1667904.html">214 Land of Hope</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1667905.html">215 Mrs. Sammer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1680145.html">216 The Same-old Gathering</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617392.html">217 Mr. Door's Description</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1683918.html">218 Free General Knowledge</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1687641.html">219 Explanation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617393.html">220 Klein's Sacrificial Trial</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1691416.html">221 The Tarot Club at a Higher Level</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1695580.html">222 The First Job</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1698331.html">223 Three Matters</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617394.html">224 Mystic-type Detective</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1701093.html">225 Unnoticeable Guidance</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1701094.html">226 The Terrified Xio</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1704809.html">227 Inventor Leppard</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1704810.html">228 The Mastermind</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1711857.html">229 Lesser of Two Evils</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1711858.html">230 Interrogation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1716348.html">231 Losses</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1716349.html">232 The Bravehearts Bar</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1721349.html">233 A Man Cannot Be Judged By His Looks</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1721350.html">234 The Night of the Full Moon.</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1725796.html">235 The Busy Monday</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1725797.html">236 Internal Commission</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1729528.html">237 Sequence 2</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1729529.html">238 Dragon of Imagination</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1734905.html">239 Each Having Their Own "Gathering"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1734906.html">240 Trying Your Luck?</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1738909.html">241 Language of Foulness</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1738910.html">242 Bakerland</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1742732.html">243 Catching the Adulterous Ac</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1742733.html">244 Appointee</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1754121.html">245 Confirmation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1754122.html">246 Strange Omen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1757972.html">247 The Whole Story</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1757973.html">248 Waiting From Both Sides</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1761785.html">249 The Assassination</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1761786.html">250 Rich Experience in Courting Death</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1765574.html">251 Gains</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1765575.html">252 Epilogue</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1770193.html">253 Night Thoughts</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1770194.html">254 Various Parties</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1774170.html">255 Photography Exper</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1774171.html">256 Meeting the Apothecary Again</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1780935.html">257 "2-081"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1780936.html">258 Murder Case</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1786610.html">259 Underground Structure</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1786611.html">260 Strange Statues</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1790510.html">261 The Innermost Room</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1790511.html">262 Dream</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1794400.html">263 Spiritual Perception and Attemp</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1794401.html">264 A Five-Person Gathering</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1798298.html">265 Cards of Blasphemy</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617395.html">266 The World's Commission</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1802091.html">267 Singing to the Same Tune</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1802092.html">268 The Cemetery and the Hospital</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1806048.html">269 Clues to the Psychology Alchemists</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1806049.html">270 Reporter</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1809933.html">271 Golden Rose</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1809934.html">272 Observing Each Other</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1812642.html">273 Handing Out A Formula</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1812643.html">274 Beyonder Weapon</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1815411.html">275 Pound Family</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1815412.html">276 Rafter Pound</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1819992.html">277 Finalizing a Plan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1819993.html">278 Free?</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1823855.html">279 Extreme Joy Begets Sorrow</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1823856.html">280 The First Step</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1827773.html">281 Clue</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1827774.html">282 This is East Borough</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1831670.html">283 Dock Union</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1831671.html">284 Instinctive Trembling</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1836539.html">285 Midnight Clock Tower</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1836540.html">286 A Taunting Smile</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1840445.html">287 Death Battle</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1840446.html">288 A Scene Filled with Symbolism</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1844138.html">289 Conjecture and Investigation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1844139.html">290 Effecting Change Indirectly</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1848254.html">291 Seller and Notary In One</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1848255.html">292 Exchanging News</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617396.html">293 Derrick's Worry</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1853514.html">294 Admiral of Stars</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1857766.html">295 All Ingredients Gathered</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1857767.html">296 Magician</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617397.html">297 The Full Moon's Ravings</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1867959.html">298 Another Case</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1867960.html">299 Snapping Fingers</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1867961.html">300 Spirit Dance</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1872075.html">301 Awakening</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1872076.html">302 A Clue</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1876122.html">303 Rookie</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1876123.html">304 Feathers</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1880225.html">305 The Detective Exchange</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1880226.html">306 Giant Bishop</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1886583.html">307 Dawn Paladin</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1886584.html">308 A Prepared Magician</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1890602.html">309 Choosing One Out Of Two</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1890603.html">310 National Atmospheric Pollution Council</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1895767.html">311 Observing Each Other</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1895768.html">312 A Failed Disguise</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1899898.html">313 The Ancient Deities</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1899899.html">314 Possible</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1903823.html">315 Return to Harvest Church</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1903824.html">316 Never Perform Unprepared</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1907881.html">317 Roselle Memorial Exhibition</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1907882.html">318 Verification</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617398.html">319 Audrey's "Adventure"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1911902.html">320 Action</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2227051.html">321 Appearance of A Living Person</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1916431.html">322 The Thrilling Nigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1916432.html">323 The Opening Incantation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1916433.html">324 Imagined and Real "Adorer"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617399.html">325 The Equestrian Teacher's Problem</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1920730.html">326 "Professional" Suggestion</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1924935.html">327 Encounter on the Road</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1924936.html">328 He That Touches Pitch Shall Be Defiled</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1929008.html">329 Claw Marks</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617400.html">330 Sharron and Maric's Philosophy</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1933123.html">331 The Accursed</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1933124.html">332 Notary Certificate</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1937372.html">333 The Tracker</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1937373.html">334 Bullets</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1942252.html">335 Exploring Verdi Stree</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1942253.html">336 The Abraham Family</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1951197.html">337 The Search For A Missing Person</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1951198.html">338 The Experienced Klein</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1955399.html">339 Psychiatris</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1955400.html">340 Pas</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1960701.html">341 Private Communication</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1960702.html">342 The Old Fox Alger</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1965005.html">343 By Oneself</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1965006.html">344 Spending Lavishly</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1972778.html">345 Sun Brooch</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1972779.html">346 A Magician Before Going Onstage</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1976954.html">347 Zombie and Werewolf</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1976955.html">348 Terrifying Wraith</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1981984.html">349 Poison Bottle</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1981985.html">350 Splendid Fireworks</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1986909.html">351 "Game" of Question and Answer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1986910.html">352 Breakfas</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1990533.html">353 Today Is Quite Different From Yesterday</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1990534.html">354 Tales of an "Adventure" in East Borough</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1994767.html">355 Outsider</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1994768.html">356 Informant Fee</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/1998863.html">357 Happenings at the Banque</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617401.html">358 The Sun's Worry</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2003540.html">359 The Blasphemer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2003541.html">360 A Difficult Conundrum</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2008896.html">361 Translucent Worm</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2008897.html">362 The Secret Deed Ritual</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2013880.html">363 High-level Authority</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2013881.html">364 Cathedral of Serenity</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2018712.html">365 Vampire With A Unique Hobby</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617402.html">366 The Hanged Man's Ambition</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2022894.html">367 Threatening Letter</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2022895.html">368 Taking Him By Surprise</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2026775.html">369 Inception</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617403.html">370 The Audience's Applause</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617404.html">371 Detective Moriarty's First Fan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2030935.html">372 Missing Case</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2035883.html">373 Search</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2035884.html">374 Artificial Sleepwalking</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617405.html">375 An "Evil God's" Impressive Ac</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2038754.html">376 Mr. Harras</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617406.html">377 Capim's Dinner</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2040373.html">378 The Show Begins</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617407.html">379 Magic's Key Segmen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2046982.html">380 Curtain Call</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2048572.html">381 Conjectures</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2048573.html">382 Hero Bandi</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2048574.html">383 Returning Home</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2050173.html">384 On Your Own You Check Yourself</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2052781.html">385 A Story About Love</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2052782.html">386 A Nightmare</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2054394.html">387 The Uniqueness of a Spirit World</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2069607.html">388 Exploring the Dreamland</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2069608.html">389 Nighthawk</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2072952.html">390 Anticipation!</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2072958.html">391 The Great Seafarer Roselle</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617408.html">392 Listening to Little Sun's Story</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2074077.html">393 The Worm of Time</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2074080.html">394 Sights in the Spirit World</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2074082.html">395 The Sheriff Beyonder Characteristics</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2074818.html">396 Great White Brotherhood</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2074827.html">397 Apocalyptic Prophecy</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2076365.html">398 Alluring Mushrooms</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2076369.html">399 The Corrupted</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079442.html">400 The Growth of the "Rookie"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079444.html">401 Divine Epiphany</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079445.html">402 Digging</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079451.html">403 The Fate of a Private Detective</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079452.html">404 Entrus</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079453.html">405 "Cult"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079454.html">406 A Joyous But Extremely Helpless Reality</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079455.html">407 The True Adorer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079456.html">408 A Bold Assumption</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079457.html">409 December</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079458.html">410 Framis Cage</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079459.html">411 Coming In Throngs</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079460.html">412 Letter</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079461.html">413 Visitors</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079462.html">414 Desire Apostle</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079463.html">415 The Ring</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079464.html">416 Two Rights Make A Wrong</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079465.html">417 Arrodes</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079467.html">418 The Power of the Mind</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079468.html">419 Wishing</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2079469.html">420 Devil Family</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2081272.html">421 Expensive Materials</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2084440.html">422 The True Jason</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2084441.html">423 Winds Arise</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2089904.html">424 The Problem of Change</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2089905.html">425 Under the Roses</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2095818.html">426 1-42</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2095819.html">427 The Choice of The Times</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2099999.html">428 The Scapegoa</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2100000.html">429 Various Parties</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2104929.html">430 A Brand New Day</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2104930.html">431 A Drowning Man Shouldn’t Desperately Clutch at Straws</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2110550.html">432 Contracted Creatures</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2110551.html">433 Intelligence Peddler</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2116196.html">434 Tomb and Bounty</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2116197.html">435 Aggregation Effec</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617409.html">436 Klein's Reques</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2120449.html">437 Obituary</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2125434.html">438 Invitation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2125435.html">439 The Generous Prince</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2129729.html">440 Angel Family</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2129730.html">441 "Channels" Increase</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2134059.html">442 Exploration Mission</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2134060.html">443 A One-man Performance</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2139773.html">444 Confession</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2139774.html">445 Live Broadcas</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617410.html">446 Machinery Hivemind's Combat Style</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2144142.html">447 Portrai</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617411.html">448 Amon's Possible Origins</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2147298.html">449 Better Choice</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2153373.html">450 Distribution of Money</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2153374.html">451 Faceless</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617412.html">452 Benson's Decision</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2157705.html">453 "Stairs"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2162147.html">454 Who Am I?</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2162148.html">455 Help Seeker</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2169096.html">456 Playing Ghosts</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2169097.html">457 Information Provided by the Evil Spiri</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2174792.html">458 An Ancient Sanguine</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2174793.html">459 Letting A Chance Slip</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2179295.html">460 Cyclic Explorations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2179296.html">461 The Kind and Enthusiastic Hanged Man</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2183915.html">462 What is a Miracle?</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2183916.html">463 Answering His Own Questions</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2188333.html">464 Consultation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617413.html">465 Emlyn's Determination</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2191250.html">466 Tail-devourer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2191486.html">467 The Delayed Response</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2191674.html">468 The Moon</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2193332.html">469 Vampire Queen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2197741.html">470 Name</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617414.html">471 Klein's Preparations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2203502.html">472 Latent Danger</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2203503.html">473 Faceless</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617415.html">474 Edessak's Story</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2207025.html">475 Lady Despair</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2211505.html">476 The Straw Men</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2211506.html">477 The Many Considerations of The Fool</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2217615.html">478 Grade 0 Sealed Artifact in Operation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2217616.html">479 The Inexplicable Smile</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586947.html">480 Rewards for Honesty</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2224051.html">481 Statistics and People</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2227041.html">482 Ring out the Old, Ring in the New</a></dd>
                    
                    
                    <dt>《Lord of the Mysteries》 Volume 4</dt>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2227042.html">483 New Identity</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2228957.html">484 Asymmetrical</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2228958.html">485 Rich Information</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617416.html">486 Klein's Conjectures</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2233491.html">487 The Growing Tarot Club</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2241799.html">488 Living Expenses</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2241800.html">489 Held Up Suggestion</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2248787.html">490 Warning of a Former Sailor</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2248788.html">491 The Sherlock Moriarty in the Investigation Repor</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2256026.html">492 Adventurer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2256027.html">493 Hunting</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2264713.html">494 A Bite of Murloc</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2264714.html">495 Legend of Treasure</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2264930.html">496 The Promising Sea</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2265121.html">497 The Wall of Rewards</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586949.html">498 Persona</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2265577.html">499 Solicitation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2265578.html">500 Interrogating White Shark</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2266052.html">501 Bai</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617419.html">502 The Scene in Azik's Memories</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2266491.html">503 Hostage</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2266492.html">504 Red Skull Pirate Crew</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2266969.html">505 A Servant Worth 3,000 Pounds</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2266970.html">506 The Weather Museum</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2267291.html">507 Bansy Harbor in the Wind</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617420.html">508 Don't Go Ou</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2267599.html">509 Reques</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2267600.html">510 The Returning Bishop</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2267601.html">511 "Snitch Halo"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2267659.html">512 The End of Things?</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2269133.html">513 Intimidation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2269134.html">514 Legends of Ancient Gods</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2269504.html">515 Harvests for Everyone</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617421.html">516 The Hanged Man's Guess</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2269902.html">517 City of Generosity</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2269903.html">518 On the Brink of Death</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2270328.html">519 Naming</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2270329.html">520 Admiral of Blood</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2270784.html">521 Bold Assumption</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2270785.html">522 Colony</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2271200.html">523 Cooperation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2271201.html">524 Meeting</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2271624.html">525 The Calm Squall</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2271625.html">526 Dream of Eternity</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2271626.html">527 A Quick Battle</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2271680.html">528 Grazing</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2272268.html">529 Tacitness</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2272269.html">530 Chronicles</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2272926.html">531 Great Acting</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2272927.html">532 Lord "Caucasian"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2273424.html">533 Mr. 4,200 Pounds</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2273425.html">534 A Dream Lesson</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2273813.html">535 Fond of Teaching</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2273814.html">536 Local Faith</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2274198.html">537 Excessive Spiritual Perception</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2274199.html">538 Dispel</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2274572.html">539 Late-night Operations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2274573.html">540 Suppression</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2274798.html">541 Visitor</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2274799.html">542 Bayam Under Curfew</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2274990.html">543 The Reality That Exceeds Expectations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2276373.html">544 Exper</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2276374.html">545 The Enraged Kalvetua</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2277183.html">546 Spirit World</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2277184.html">547 Pries</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2278161.html">548 Trick to Dealing with Large Creatures</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2278162.html">549 High Elf</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2278524.html">550 The Negative Effects of the Sealed Artifac</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2278525.html">551 The Ten Commandments</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2278980.html">552 Pomp</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617422.html">553 Danitz's Hard Work</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2279346.html">554 Acting as God</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2279347.html">555 The Message Late at Nigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2279758.html">556 Discriminatory Treatmen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2279759.html">557 Planting a Cue on Herself</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2280322.html">558 Search for Abnormalities</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2280323.html">559 Meeting on the Way</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2281102.html">560 The Hanged Man Gets Fleeced</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2281103.html">561 "Recruitment Fair"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2281481.html">562 Helene</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617423.html">563 The Fool's "Blessing"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586955.html">564 Knowledge as a Pursuer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2281893.html">565 Eye of Mystery Prying</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2282248.html">566 The Details of Clothes</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2282431.html">567 Source of the Matter</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2282702.html">568 Subsidiary "Gods"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2282846.html">569 A Straw Will Show Which Way the Wind Blows</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2283295.html">570 Not Admitting or Denying</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2283296.html">571 Huge Pressure</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2283704.html">572 Recite My Name</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2283705.html">573 Teaching a Lesson in Reality</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2284603.html">574 Fail to Accomplish</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2284604.html">575 Golden Dream</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617424.html">576 Vice Admiral Iceberg's Collector's Room</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2284989.html">577 Book from More Than 3,000 Years Ago</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2285650.html">578 "Abduction"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2285651.html">579 Academic Question</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2286072.html">580 Wormtongue</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2286073.html">581 Both Getting into Character and Detachmen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2287010.html">582 "Provocation"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2287011.html">583 Contingency Plan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2287378.html">584 Scapegoa</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2287379.html">585 Buying Medicine</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586957.html">586 Farewell and Reunion</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2288515.html">587 Confusing</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2288878.html">588 Old Acquaintance</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2288879.html">589 "Sowing Discord"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617425.html">590 Arrodes's Bottom Line</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2289249.html">591 Making Good Use of Things</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2289588.html">592 Three Advancements in a Week</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2289589.html">593 Solution</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617426.html">594 The World's Commission</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2290108.html">595 Domain</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2290564.html">596 Clue</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617427.html">597 Klein's Plan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2290939.html">598 Windfall</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2290940.html">599 Dragon Migh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2291344.html">600 Clearing the Place</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2291345.html">601 Scaring Oneself</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2294106.html">602 Becoming Famous</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2294107.html">603 Conditions</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2294559.html">604 A Different Enemy</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2294560.html">605 "Judgment"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2294924.html">606 Intimidation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2294925.html">607 Discovering an Abnormality</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2296376.html">608 Professional</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2296377.html">609 Paying a Visi</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2296701.html">610 Throwing the Die</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2296702.html">611 Fate Councilor</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2297084.html">612 In Hand</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617428.html">613 Leonard's Investigation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2297500.html">614 Adventurer Association</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2297501.html">615 Grayish-white Fog</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2297905.html">616 Silver Lining</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2297906.html">617 The Mysterious Adam</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2298200.html">618 Volunteer Work</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2298201.html">619 Unable to Speak</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2298574.html">620 Stand-in Mission</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617429.html">621 Governor-general's Office Banque</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2298941.html">622 Temporary Contrac</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2298942.html">623 First Nigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2299332.html">624 Warning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2299333.html">625 Successful First Day</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617430.html">626 Amyrius's Decision</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2299690.html">627 Late into the Nigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2300061.html">628 Prohibition</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586964.html">629 Naturism Sec</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2300444.html">630 Timeline</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2300445.html">631 Three Days of Absence</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2300801.html">632 Finishing Up</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2300802.html">633 Two Types of Parasitizing</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2304051.html">634 City of White</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2304052.html">635 Meeting</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2304500.html">636 Slaughterer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2304501.html">637 The Future</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2304884.html">638 Poison Exper</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2304885.html">639 Name Lis</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2305299.html">640 Female Pirate</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617431.html">641 A Well Mouth That Humans Can't Pass</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2305796.html">642 Killing Three Birds With One Stone</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617432.html">643 Klein's Version of "Magic Mirror Divination"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2306214.html">644 Laughter</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2306215.html">645 Calming Method</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617433.html">646 Leymano's Spellbook</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2306916.html">647 Sea of Ruins</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2317634.html">648 Noon and Nigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2317635.html">649 Black Cloister</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2317932.html">650 The Unlucky Anderson</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2317933.html">651 Meeting Again</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2319210.html">652 Mermaid Clues</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2319211.html">653 Black-faced, Black-handed</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2319585.html">654 Prisoner and Guard</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2319586.html">655 Dream Analysis</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2319587.html">656 Crazy Mutations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2319588.html">657 Terrifying Vitality</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2319589.html">658 55,000 Pounds</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2319590.html">659 This Beyonder Power is Very Powerful</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2319634.html">660 Behind the Mask</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2320025.html">661 Approaching</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2320026.html">662 Powerful Aura</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2320364.html">663 False Alarm</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2320365.html">664 Every Second Counts</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2320706.html">665 Spirit Body Threads</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2320707.html">666 Afternoon Town</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2321223.html">667 Prayer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2321743.html">668 Confidence Might Also Be a Weakness</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2321939.html">669 Exchange of Information</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2322108.html">670 Repenter</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2322440.html">671 The Fourth Name</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2322441.html">672 Bystander</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2322823.html">673 Blood Tex</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2322824.html">674 Leaving the Ship</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2323177.html">675 Building Ties</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2323178.html">676 Tripartite Transaction</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2327184.html">677 Falling to the Ground</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2327185.html">678 Punishmen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2327226.html">679 Murder Reques</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2327461.html">680 New Thoughts</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2327462.html">681 Indirect Answer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2327546.html">682 Seeking "Food"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2328086.html">683 So You Are Here as Well</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2328087.html">684 Battle Encounter</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2328553.html">685 From A Delay to Disconnection</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2328554.html">686 A Hard-to-complete Ritual</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2328969.html">687 Blatherer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2328970.html">688 Fruitless Wai</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617434.html">689 That's It?</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2330614.html">690 Miss Messenger</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2330615.html">691 Meeting</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2330616.html">692 Suspec</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2330617.html">693 Attemp</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2330618.html">694 Burn Upon Chanting</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2330927.html">695 Stark Contras</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2330928.html">696 Giant Guardian</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617435.html">697 Story's Progress</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2331246.html">698 Fifth King of Angels</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2331621.html">699 Boss Figh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2331622.html">700 Excellent Teamwork</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2332545.html">701 Giants Never Retrea</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2332546.html">702 Epilogue</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2333474.html">703 Thank-you Presen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2333475.html">704 Origins</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2333788.html">705 Mythical Creature</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2333789.html">706 That Man</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617436.html">707 Danitz's Reques</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2334191.html">708 Revolver Worth 9,000 Pounds</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617439.html">709 Elland's Warning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2334654.html">710 Plan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2334655.html">711 The Storm Attacks</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2334656.html">712 The Loyal Alger</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2334657.html">713 Three Questions</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2334658.html">714 New Diary Pages</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2334659.html">715 Madness in His Later Years</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2335097.html">716 Island and Ruins</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2335098.html">717 The World and Hero Bandi</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2335546.html">718 Characters in the Book</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2335547.html">719 Dream Tour</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2429942.html">720 Philosopher</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617440.html">721 Klein's Guidance</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2431348.html">722 An Unpeaceful Nigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2431349.html">723 Another Good Deed Today</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2431814.html">724 The True Meaning Behind the Warning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2431815.html">725 Morning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2432176.html">726 Preparations Are Very Importan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2432177.html">727 Lucky One</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2432178.html">728 Triple Combo</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2432179.html">729 Chaos</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2432255.html">730 Handling the Latent Risk</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2432697.html">731 Gains</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2432698.html">732 Destination</a></dd>
                    
                    
                    <dt>《Lord of the Mysteries》 Volume 5</dt>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2433100.html">733 The Return</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2433101.html">734 Old Friends</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2433455.html">735 Another Visi</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2433456.html">736 Third Chair</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2433811.html">737 Official Appearance</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2433812.html">738 Life of a Tycoon</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2434162.html">739 The Encountered and the Yet-To-Be-Encountered</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2434163.html">740 Self-recommendation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2434546.html">741 Butler</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2434547.html">742 Decided</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2435273.html">743 A Diary Page</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2435274.html">744 Sale</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2435275.html">745 Knowledge is Money</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2435276.html">746 Same Nigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2435277.html">747 First Blood</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2435278.html">748 A Due</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617441.html">749 The Moon's Authority</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2435693.html">750 Attraction?</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2435694.html">751 Loen-styled Euphemism</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2436044.html">752 Warning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2436045.html">753 Bishop Visits</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2436311.html">754 Invitation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2436312.html">755 "Switchboard Receptionist"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2436575.html">756 Grand Mass</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2436576.html">757 Dream Encounter</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2474045.html">758 Efforts Will Ultimately Pay Off</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2507365.html">759 First Dance</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2553627.html">760 What a Small Circle</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2562435.html">761 Good People and Good Deeds</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2562470.html">762 Nation Reestablishment Society</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2562647.html">763 Mr. X</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2562792.html">764 First Investigation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2563148.html">765 Monday Again</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617442.html">766 The Deities' Anchor</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2563431.html">767 Passing a Message</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2563432.html">768 "Conversation" Between Smart People</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2563815.html">769 Sacrificing to Oneself</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2563816.html">770 A Child Should Act like a Child</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617443.html">771 Fate Siphon</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617444.html">772 Walter's Abnormality</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2564481.html">773 Additional Developmen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2564482.html">774 Clues</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2564870.html">775 Making Use</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2564871.html">776 Preparations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2565223.html">777 Sick and Crazy Setup</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2565224.html">778 1 + 1 &gt; 2</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2565506.html">779 One-Sho</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2565507.html">780 Extrac</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2565792.html">781 Negative Effects</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2565793.html">782 Saturday Nigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2566077.html">783 The Trick to Communication</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2566078.html">784 Character Assassination</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617445.html">785 Trissy's Discovery</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2566348.html">786 Accounting Fraud</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2566640.html">787 Dorian's Warning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2566641.html">788 Hidden Passage</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2566999.html">789 Each Person's Monday</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2567001.html">790 The End of the Diary</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2567003.html">791 New Model</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2567004.html">792 The Fool's Authority</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2567006.html">793 Surprise Visitor</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2567008.html">794 Short-Term Investmen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2567089.html">795 Patience</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2567556.html">796 Slowly Becoming Proficien</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2567557.html">797 Reward</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2568018.html">798 Revisiting an Old Haun</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2568019.html">799 Spying</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2568242.html">800 Psychologically Becoming "Better"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2568441.html">801 Plea</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2569131.html">802 Follow-up Solutions</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2569132.html">803 Name Rectification</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2569434.html">804 Archaeological Team</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2569435.html">805 Meeting Up</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2569947.html">806 Entering the Island in the Middle of the Nigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2569948.html">807 Mediocre Luck</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2570228.html">808 Awful Singing</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2570229.html">809 The Danger Amidst the Darkness</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2570230.html">810 Whose Cathedral</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2570231.html">811 The Picture in the Catacombs</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2570232.html">812 Myth from Another Perspective</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2570296.html">813 Tyran</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2570484.html">814 Disappeared</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2570485.html">815 After-action review</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2570486.html">816 Completing the Transaction</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2570565.html">817 Guests</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2570889.html">818 Warning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2570890.html">819 Gif</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2571162.html">820 Two Dazed Instances</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2571163.html">821 Soul Imprin</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2571555.html">822 Another</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2571556.html">823 The Maturing Tarot Club</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2572027.html">824 Conflic</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2572028.html">825 Reservation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2572382.html">826 The Thought of Being Forgotten</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2572383.html">827 Plenty of People Coming and Going</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2572755.html">828 Movement of the Nigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2572756.html">829 Arrival of June</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2572757.html">830 Infiltration</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2572758.html">831 Just Inches Away</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2572759.html">832 Town</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2572760.html">833 Things to Take Note Of</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2572761.html">834 Good Luck</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2572762.html">835 The Figures Coming and Going</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2573122.html">836 "Tossing Food"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2573123.html">837 The Hangers</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2573833.html">838 Scene from the Historical Void</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2573834.html">839 Descendant of An Ancient God</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2574103.html">840 Using His Advantage</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2574104.html">841 Keeping Each Other in Check</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2574379.html">842 Behind the Door</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2574380.html">843 Magical Mushroom</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2575197.html">844 Which Symbol</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2575198.html">845 Return</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2575991.html">846 Find the Targe</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2575992.html">847 The Name Hidden in the Dossiers</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576300.html">848 Getting Caught in the Crossfire</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576301.html">849 Consultation Fees</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576302.html">850 The Devil is in the Details</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576303.html">851 Dwayne Dantès's New Business</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576304.html">852 Straight to the Poin</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576305.html">853 Comparison of Experience in Sophistry</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576306.html">854 Confession</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576307.html">855 New Visitor</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576308.html">856 Bodyguards Arrive</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576309.html">857 Poignan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576310.html">858 Generous</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576311.html">859 New Mushrooms</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576312.html">860 Runaway Horse</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576313.html">861 Fors's Dream</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576314.html">862 Kind Warning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576315.html">863 Charity Party</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576316.html">864 Actor and Spectator</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576317.html">865 Earl Hall's Suggestion</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576606.html">866 Home</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576607.html">867 Investigation Mission</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576907.html">868 Shared Identity</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2576908.html">869 Report Him!</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2577213.html">870 A Question That Strikes The Hear</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2577214.html">871 Director</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2577577.html">872 Results of Mediumship</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2577578.html">873 Undetectable Communication</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2577857.html">874 I Didn't Say Anything</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2577858.html">875 Mummy</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2578193.html">876 Art of Diverting Trouble</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2578194.html">877 Whose Trap</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2578376.html">878 Arrodes's Question</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2578377.html">879 Dual Purpose</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2578378.html">880 The Silent One</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2578379.html">881 A Play</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2578485.html">882 Core of Mischief</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2578544.html">883 Demigod's Appraisal</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2578686.html">884 Destined Encounter</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2578687.html">885 Two Letters</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2578754.html">886 Preparations Before Leaving</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2578788.html">887 Familiar Figure</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2579030.html">888 A Shocking Glance</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2579031.html">889 Warning to Everyone</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2579341.html">890 Ignore "Him"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2579342.html">891 Strange Chapel</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2579615.html">892 Individual "Comprehension"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2579616.html">893 Hair-Resembling Plants</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2579958.html">894 Meeting</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2579959.html">895 Finally At Ease</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2580175.html">896 Daly's Probe</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2580176.html">897 The Chief's Hin</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2580410.html">898 Response</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2580411.html">899 Berserk Sea's Spirit World</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2580412.html">900 "Self-Recommendation"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2580413.html">901 The Mutated Paper Figurine</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2580590.html">902 Shadow</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2580632.html">903 Scholar-type Bishop</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2580746.html">904 Analysis</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2580747.html">905 Psychological Blind Spo</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2580754.html">906 Leonard's Warning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2580818.html">907 The Power of Mysticism</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2581098.html">908 Not Leaving Any Problems</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2581099.html">909 Serving Good Luck</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2581210.html">910 Monster Pathway</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2581375.html">911 Strange Scene</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2581556.html">912 Origins of the Artisan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2581698.html">913 Klein's Preparatory Work</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2581785.html">914 The Calling Deep Inside the Mausoleum</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2581951.html">915 Another "Me"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2582068.html">916 Irresistible Approach</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2582259.html">917 Three Choices</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2582557.html">918 Guesses and Ideas</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2582558.html">919 "Perfect" Inference</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2583008.html">920 Calderón's Origins</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2583009.html">921 Politeness Firs</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2583248.html">922 Mushrooms and Fish</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2583249.html">923 After Effects</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2583554.html">924 First Key Factor at Carrying out Risky Operations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2583555.html">925 Choosing "Clothes"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2584256.html">926 Spiraling City</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2584257.html">927 Tyrant's Migh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2585136.html">928 Misdirection</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2585137.html">929 True Soul Body</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2585564.html">930 Former Organization</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2585565.html">931 New Method to Acquiring Intel</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2585729.html">932 Thin-Skinned</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2585730.html">933 Evil Spirits' Common Trai</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2585731.html">934 1 + 1 &gt; 2</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2585732.html">935 "Meeting Up"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2585848.html">936 Don't Want to Miss Ou</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2585916.html">937 Several Days Later</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586025.html">938 Writing</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586026.html">939 Bai</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586045.html">940 A Story</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586123.html">941 Development That Adheres to Logic</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586124.html">942 Deity's Curse</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586400.html">943 The Third Ac</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586401.html">944 The Fourth Ac</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586667.html">945 The Story's Ending</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586668.html">946 A Bestowment Or A Curse</a></dd>
                    
                    
                    <dt>《Lord of the Mysteries》 Volume 6</dt>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586899.html">947 House Call</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2586900.html">948 Meaning of Existence</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2587293.html">949 Direction of Investigations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2587294.html">950 Keeping Secrets</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2587771.html">951 Drawing A Card</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2587772.html">952 What a Small World</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2587980.html">953 Prophecy</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2587981.html">954 Strange Ancient Castle</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2587982.html">955 Ancient Wraith</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2587983.html">956 The Things Behind the Door</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2588060.html">957 Getting to Know Each Other</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2588144.html">958 Labeling</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2588259.html">959 Even Newcomers Are Different Amongst Themselves</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2588260.html">960 The Fool's Sigh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2588301.html">961 Warning By Informing</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2588337.html">962 Being Known</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2588591.html">963 Problem with Intelligence</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2588592.html">964 Medici's Cause of Death</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2588853.html">965 Brief Crisis</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2588854.html">966 Train</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2589154.html">967 "Revelation"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2589155.html">968 "Descending" Sain</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2589425.html">969 Duke</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2589426.html">970 Talent at Soliciting Donations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2589619.html">971 Restrain</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2589620.html">972 Nast's Memories</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2590008.html">973 New "Angel"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2590009.html">974 Mind World</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2590010.html">975 A Familiar Feeling</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2590011.html">976 Zealo</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2590012.html">977 First Sermon</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2590063.html">978 Gif</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2590229.html">979 Joy of Life</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2590230.html">980 Choice of Parasitic Targe</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2590268.html">981 Hazel's Decision</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2590437.html">982 Bizarro Sorcerer vs Parasite</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2590438.html">983 In Your Name</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2590495.html">984 Active Response</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2591004.html">985 Blessed of Concealmen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2591005.html">986 The "Infectiousness" of Parasitizing</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2591290.html">987 "Warehouse Clearance"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2591291.html">988 Joint Operation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2591520.html">989 Mentor Alger</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2591521.html">990 Inner Fears</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2592116.html">991 A Ritual Without A "Reply"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2592117.html">992 Arrodes's Congratulations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2592373.html">993 Another Possibility</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2592374.html">994 Prelude</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2592375.html">995 "Conjoined Person"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2592376.html">996 Card Game</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2592398.html">997 "Gambling God" Dwayne</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2592866.html">998 Establishing Relations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2592867.html">999 Instigation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2592868.html">1000 Prelude</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2593070.html">1001 First Movemen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2593071.html">1002 Second Movemen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2593875.html">1003 Ritornello</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2594271.html">1004 Third Movemen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2594435.html">1005 Fourth Movemen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2594491.html">1006 Finale and Ending</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2594539.html">1007 Dealing With The Aftermath</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2594918.html">1008 Splitting</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2595038.html">1009 Payment Is Always Exacted for What's Bestowed</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2595133.html">1010 Consultant Fee</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2595221.html">1011 Roselle's Other Warning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2595449.html">1012 First Day of September</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2595530.html">1013 Individual Growth</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2595645.html">1014 The Growing Group</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2595646.html">1015 March of War</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2595647.html">1016 News from the Numinous Episcopate</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2595684.html">1017 Maygur Manor</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2595809.html">1018 Unexpected</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2595937.html">1019 Patience</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2596177.html">1020 Means of a Demoness</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2596490.html">1021 Cross</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2596612.html">1022 Answer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2596812.html">1023 Mason Dere's Death</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2596994.html">1024 Traditional Skills</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2597308.html">1025 Random Anomalies</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2597309.html">1026 Two Restrictions</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2597460.html">1027 Decei</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2597908.html">1028 Saving Himself</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2598023.html">1029 Ruins No. 1</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2598210.html">1030 Joint Operations</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2598385.html">1031 Possibility</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2598492.html">1032 Forceful Purifier</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2598587.html">1033 Let There Be Ligh</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2598717.html">1034 Gains</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2598718.html">1035 Diary Page in Advance</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2598719.html">1036 "Expected" Developmen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2598787.html">1037 Gehrman's Problem</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2598825.html">1038 Name Lis</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2598897.html">1039 Hope</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2605597.html">1040 The Tranquil Surface of the Sea</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2605667.html">1041 King’s Daughter</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2605716.html">1042 Plan From a Different Angle</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2606303.html">1043 Each Having Their Own Plans</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2606350.html">1044 Placing Herself in the Tides</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2606415.html">1045 Dreamwalker</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2606550.html">1046 A "Test"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2606667.html">1047 Brainstorm</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2606844.html">1048 Real and Fake "Spy"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607015.html">1049 Spectator's Intuition</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607125.html">1050 Good At Using Hypnosis</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607276.html">1051 Different Styles of Different Pathways</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607375.html">1052 3v1</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607511.html">1053 Presiden</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607575.html">1054 Tailored-Made Ritual</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607576.html">1055 Train of Though</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607577.html">1056 The Real "Devil"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607616.html">1057 The Extraordinary and the Ordinary</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607683.html">1058 A Tool</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607730.html">1059 The Authority of the Moon</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607858.html">1060 Covered-up Secre</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2607974.html">1061 Whose Dream</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2608076.html">1062 "Teaching" Online</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2608193.html">1063 The Lord's Left-Hand</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2608260.html">1064 Drawing Closer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2608329.html">1065 Cosmos Wanderer</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2608592.html">1066 Familiar Name</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2608742.html">1067 The Elven Version of History</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2608846.html">1068 Illogical Details</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2608983.html">1069 "Under the Sea"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2609122.html">1070 Maybe It's Real</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2609279.html">1071 Hall of Truth</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2609416.html">1072 The Call From Behind the Door</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2609531.html">1073 Three Possibilities</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2609532.html">1074 The Answer to Questions</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2609533.html">1075 No Response</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2609745.html">1076 Dorian's Reques</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2609788.html">1077 Four Choices</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2609853.html">1078 The Hidden Secre</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2610080.html">1079 The Hardworking Gardeners</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2610133.html">1080 The Line of Thinking for Acting</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2610225.html">1081 The Returnee</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2610347.html">1082 Sudden Turn of Events</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2610418.html">1083 Backlund in Chaos</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2610480.html">1084 The People in War</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2610732.html">1085 Scrutinizing the Situation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2610823.html">1086 A Simple Inference</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2610927.html">1087 The Deities' Attitudes</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2611016.html">1088 Hiding in Secre</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2611184.html">1089 A Difficult Decision</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2611280.html">1090 Haunted Tales</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2611415.html">1091 Asking Himself</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2611499.html">1092 Ridiculous People</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2611746.html">1093 The Angels of The Fool</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2611747.html">1094 Breakthrough Poin</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2611748.html">1095 Acting as Himself</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2611771.html">1096 Cooperation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2611908.html">1097 Different Conundrum</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2611974.html">1098 Contribution Accumulation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2612095.html">1099 1368</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2612183.html">1100 One Book</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2612264.html">1101 Special Reward</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2612404.html">1102 Engaging Tigers to Hunt Wolves</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2612449.html">1103 Hin</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2612491.html">1104 Contradiction</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2612590.html">1105 The Key Diary Entry</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2612725.html">1106 Making Contac</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2612896.html">1107 Relax</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2617348.html">1108 Messed Up Family</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2620159.html">1109 Patience</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2620915.html">1110 "Spirit" Channeling</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2621967.html">1111 "Poker Expert"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2622225.html">1112 Traveling Notebook</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2622338.html">1113 Powerful Guardian</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2622339.html">1114 The Remnant Will</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2622340.html">1115 Inside the King's Cour</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2624475.html">1116 Familiar Gaze</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2625674.html">1117 The Strongest Organization</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2626157.html">1118 Klein's Fear</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2626316.html">1119 Tacit Cooperation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2626448.html">1120 Court Chaser</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2626517.html">1121 Weakness</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2633075.html">1122 The Instructions of The Fool</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2635282.html">1123 After the Expedition</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2636126.html">1124 Enhanced Teammates</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2637345.html">1125 Opportunity</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2637589.html">1126 "Unexpected"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2637769.html">1127 Sefirah Castle</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2637882.html">1128 Finally An Outcome</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2651057.html">1129 Pressure</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2651646.html">1130 News Storm</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2651732.html">1131 An Indescribable Transaction</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2652647.html">1132 Interlude</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2652704.html">1133 Chan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2652705.html">1134 Mr. Door</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2652706.html">1135 Fragran</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2653191.html">1136 Rumors from Ancient Times</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2653413.html">1137 Amidst History</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2653493.html">1138 "Scholar of Yore"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2654605.html">1139 A Different Form of Companionship</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2654642.html">1140 Plans</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2655139.html">1141 Deep Winter</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2656063.html">1142 Warning</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2656098.html">1143 Reasonable Developmen</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2656195.html">1144 Narrowly</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2656658.html">1145 Three Arrows At The Same Time</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2668133.html">1146 A Real Charlatan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2669637.html">1147 Chaos</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2669725.html">1148 Not Late</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2669944.html">1149 Escape</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2670078.html">1150 Mad Dash</a></dd>
                    
                    
                    <dt>《Lord of the Mysteries》 Volume 7</dt>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2676644.html">1151 Decei</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2677836.html">1152 "Error"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2677936.html">1153 The Sunset Tunnel</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2677937.html">1154 I'll Give You a Chance</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2677938.html">1155 Walking in the Dark</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2681672.html">1156 Thinking</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2681729.html">1157 Poise</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2681795.html">1158 Coming to Terms</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2681953.html">1159 Validation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2682011.html">1160 Improving</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2693017.html">1161 Countdown</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2696864.html">1162 Prophecy</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2696937.html">1163 Approaching</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2697008.html">1164 Cheating</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2697042.html">1165 The Grand Lineup</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2697043.html">1166 Its Name</a></dd>
                    
                    
                    <dt>《Lord of the Mysteries》 Text</dt>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2699802.html">1167 When the Stars Are Righ</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2700358.html">1168 My Anchors</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2700463.html">1169 Klein's Plan</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2701198.html">1170 Ritual</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2701321.html">1171 The Third One</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2701524.html">1172 "Unperturbed"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2703196.html">1173 Advice</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2704092.html">1174 Joint Operation</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2704093.html">1175 Ideas Are Very Importan</a></dd>
                    
                    <dd id="bottom"> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2704094.html">1176 Plo</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2704095.html">1177 Substitute</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2704096.html">1178 I Have a Blessed</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2704097.html">1179 Preparations Both Ways</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2704098.html">1180 Different Effects</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2704099.html">1181 Nois Ancient City</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2704216.html">1182 The Holy Word</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2704246.html">1183 Klein's Advantage</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2704247.html">1184 "Record"</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2704248.html">1185 Reappearing</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2705428.html">1186 Opportunity and Danger Are Two Sides of the Same Coin</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2706293.html">1187 Change</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2707410.html">1188 Thick-Skinned</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2707530.html">1189 Winter Gifts Day</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2707614.html">1190 Resonance</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2707732.html">1191 Grasping One's Mentality</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2708456.html">1192 Each Serving their Duty</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2711876.html">1193 Heading Eas</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2713653.html">1194 A Lion's Hunt of a Rabbi</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2715668.html">1195 Grade 0</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2715760.html">1196 The Ugly Duckling</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2715852.html">1197 Mind Storm</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2716020.html">1198 Frenzy</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2717055.html">1199 Inauspicious Box</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2717167.html">1200 Randomness</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2717210.html">1201 Professional</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2717211.html">1202 The Tarot Club</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2717212.html">1203 Harves</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2726490.html">1204 Putting Life and Death Aside</a></dd>
                    
                    <dd> <a style="" href="https://www.wuxiaworld.co/Lord-of-the-Mysteries/2726549.html">1205 A Thousand-Year-Old Trap</a></dd>
                    
                    
                </dl>
                </div>
<script>disqus();</script><div id="disqus_thread"><iframe id="dsq-app7924" name="dsq-app7924" allowtransparency="true" frameborder="0" scrolling="no" tabindex="0" title="Disqus" width="100%" src="./Lord of the Mysteries - Index - Wuxiaworld_files/saved_resource(1).html" style="width: 1px !important; min-width: 100% !important; border: none !important; overflow: hidden !important; height: 1541px !important;" horizontalscrolling="no" verticalscrolling="no"></iframe></div>
<script>
/**
* RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
* LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables
*/
/*
var disqus_config = function () {
this.page.url = PAGE_URL; // Replace PAGE_URL with your page's canonical URL variable
this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
};
*/
(function() { // DON'T EDIT BELOW THIS LINE
var d = document, s = d.createElement('script');

s.src = '//wuxiaworldco.disqus.com/embed.js';

s.setAttribute('data-timestamp', +new Date());
(d.head || d.body).appendChild(s);
})();
</script>
<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
<script id="dsq-count-scr" src="./Lord of the Mysteries - Index - Wuxiaworld_files/count.js" async=""></script>

</div>
<div id="footer" name="footer">
 <div class="footer_cont">
<p><b>Lord of the Mysteries</b> is a Fantasy novels, some original, some translated from Chinese. Themes of heroism, of valor, of ascending to Immortality, of combat, of magic, of Eastern mythology and legends. Updated with awesome new content daily. Come join us for a relaxing read that will take you to brave new worlds! A paradise for readers!</p>
<p> Copyright ? 2016 Wuxiaworld All Rights Reserved.</p>
</div>
</div><script>footer();</script><script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-100839621-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript" src="./Lord of the Mysteries - Index - Wuxiaworld_files/addthis_widget.js"></script>
<div id="downdiv" style="position: fixed;z-index: 1000;bottom: 10px;right: 10px;overflow: hidden;">
	<ins class="adsbygoogle" style="display:inline-block;width:336px;height:280px" data-ad-client="ca-pub-2853920792116568" data-ad-slot="7458734492" data-adsbygoogle-status="done"><ins id="aswift_1_expand" style="display:inline-table;border:none;height:280px;margin:0;padding:0;position:relative;visibility:visible;width:336px;background-color:transparent;"><ins id="aswift_1_anchor" style="display:block;border:none;height:280px;margin:0;padding:0;position:relative;visibility:visible;width:336px;background-color:transparent;"><iframe id="aswift_1" name="aswift_1" style="left:0;position:absolute;top:0;border:0;width:336px;height:280px;" sandbox="allow-forms allow-pointer-lock allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" width="336" height="280" frameborder="0" src="./Lord of the Mysteries - Index - Wuxiaworld_files/ads.html" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" data-google-container-id="a!1" data-google-query-id="CIKO7KrdpekCFS0S0wodNFkLCQ" data-load-complete="true"></iframe></ins></ins></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>
	</div>

    </div><ins class="adsbygoogle adsbygoogle-noablate" data-adsbygoogle-status="done" style="display: none !important;"><ins id="aswift_0_expand" style="display:inline-table;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:0px;background-color:transparent;"><ins id="aswift_0_anchor" style="display:block;border:none;height:0px;margin:0;padding:0;position:relative;visibility:visible;width:0px;background-color:transparent;"><iframe id="aswift_0" name="aswift_0" style="left:0;position:absolute;top:0;border:0;width:undefinedpx;height:undefinedpx;" sandbox="allow-forms allow-pointer-lock allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation" frameborder="0" src="./Lord of the Mysteries - Index - Wuxiaworld_files/ads(1).html" marginwidth="0" marginheight="0" vspace="0" hspace="0" allowtransparency="true" scrolling="no" allowfullscreen="true" data-google-container-id="a!0" data-load-complete="true"></iframe></ins></ins></ins><iframe id="google_osd_static_frame_9700518210840" name="google_osd_static_frame" style="display: none; width: 0px; height: 0px;" src="./Lord of the Mysteries - Index - Wuxiaworld_files/saved_resource(2).html"></iframe><div id="_atssh" style="visibility: hidden; height: 1px; width: 1px; position: absolute; top: -9999px; z-index: 100000;"><iframe id="_atssh451" title="AddThis utility frame" style="height: 1px; width: 1px; position: absolute; top: 0px; z-index: 100000; border: 0px; left: 0px;" src="./Lord of the Mysteries - Index - Wuxiaworld_files/sh.f48a1a04fe8dbf021b4cda1d.html"></iframe></div><style id="service-icons-0"></style>
<script type="text/javascript">
$(function(){
    var num = $("#list dd").length;
	a = num - '30';
	$("#list dd:eq("+a+")").attr('id','bottom');
});
</script>

<iframe style="display: none;" src="./Lord of the Mysteries - Index - Wuxiaworld_files/saved_resource(3).html"></iframe><div class="addthis-smartlayers addthis-smartlayers-desktop" aria-labelledby="at4-share-label" role="region"><div id="at4-share-label">AddThis Sharing Sidebar</div><div id="at4-share" class="at4-share addthis_32x32_style atss atss-right addthis-animated slideInRight"><a role="button" tabindex="0" class="at-share-btn at-svc-facebook"><span class="at4-visually-hidden">Share to Facebook</span><span class="at-icon-wrapper" style="background-color: rgb(59, 89, 152);"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-facebook-1" style="fill: rgb(255, 255, 255);" class="at-icon at-icon-facebook"><title id="at-svg-facebook-1">Facebook</title><g><path d="M22 5.16c-.406-.054-1.806-.16-3.43-.16-3.4 0-5.733 1.825-5.733 5.17v2.882H9v3.913h3.837V27h4.604V16.965h3.823l.587-3.913h-4.41v-2.5c0-1.123.347-1.903 2.198-1.903H22V5.16z" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-share-btn at-svc-twitter"><span class="at4-visually-hidden">Share to Twitter</span><span class="at-icon-wrapper" style="background-color: rgb(29, 161, 242);"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-twitter-2" style="fill: rgb(255, 255, 255);" class="at-icon at-icon-twitter"><title id="at-svg-twitter-2">Twitter</title><g><path d="M27.996 10.116c-.81.36-1.68.602-2.592.71a4.526 4.526 0 0 0 1.984-2.496 9.037 9.037 0 0 1-2.866 1.095 4.513 4.513 0 0 0-7.69 4.116 12.81 12.81 0 0 1-9.3-4.715 4.49 4.49 0 0 0-.612 2.27 4.51 4.51 0 0 0 2.008 3.755 4.495 4.495 0 0 1-2.044-.564v.057a4.515 4.515 0 0 0 3.62 4.425 4.52 4.52 0 0 1-2.04.077 4.517 4.517 0 0 0 4.217 3.134 9.055 9.055 0 0 1-5.604 1.93A9.18 9.18 0 0 1 6 23.85a12.773 12.773 0 0 0 6.918 2.027c8.3 0 12.84-6.876 12.84-12.84 0-.195-.005-.39-.014-.583a9.172 9.172 0 0 0 2.252-2.336" fill-rule="evenodd"></path></g></svg></span></a><a role="button" tabindex="0" class="at-share-btn at-svc-compact"><span class="at4-visually-hidden">More AddThis Share options</span><span class="at-icon-wrapper" style="background-color: rgb(255, 101, 80);"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 32 32" version="1.1" role="img" aria-labelledby="at-svg-addthis-3" style="fill: rgb(255, 255, 255);" class="at-icon at-icon-addthis"><title id="at-svg-addthis-3">AddThis</title><g><path d="M18 14V8h-4v6H8v4h6v6h4v-6h6v-4h-6z" fill-rule="evenodd"></path></g></svg></span></a><div id="at4-scc" class="at-share-close-control ats-transparent at4-hide-content at4-show" title="Hide"><div class="at4-arrow at-right">Hide</div></div></div><div id="at4-soc" class="at-share-open-control at-share-open-control-right ats-transparent at4-hide" title="Show" style="padding-right: 15px;"><div class="at4-arrow at-left">Show</div></div></div><div id="at4-thankyou" class="at4-thankyou at4-thankyou-background at4-hide ats-transparent at4-thankyou-desktop addthis-smartlayers addthis-animated fadeIn at4-show" role="dialog" aria-labelledby="at-thankyou-label"><div class="at4lb-inner"><button class="at4x" title="Close">Close</button><a id="at4-palogo"><div><a class="at-branding-logo" href="https://www.addthis.com/website-tools/overview?utm_source=AddThis%20Tools&amp;utm_medium=image" title="Powered by AddThis" target="_blank"><div class="at-branding-icon"></div><span class="at-branding-addthis">AddThis</span></a></div></a><div class="at4-thankyou-inner"><div id="at-thankyou-label" class="thankyou-title"></div><div class="thankyou-description"></div><div class="at4-thankyou-layer"></div></div></div></div></body><iframe id="google_esf" name="google_esf" src="./Lord of the Mysteries - Index - Wuxiaworld_files/zrt_lookup.html" data-ad-client="ca-pub-2853920792116568" style="display: none;"></iframe></html>
"""

class saveToPdf():

	def __init__(self, text = '', chaptername='', title = "Hello world",pageinfo = "", image='',info=""):

		self.PAGE_HEIGHT=defaultPageSize[1]
		self.PAGE_WIDTH=defaultPageSize[0]
		self.styles = getSampleStyleSheet()
		self.Title =title
		self.pageinfo = pageinfo if pageinfo else "Download Ebook(s) at (www.wuxiaebooks.com)"
		self.bogustext= text if text else ("This is Paragraph numbers.") *200
		self.chapter= 0
		self.chaptername= chaptername if chaptername else 'chapter'+str(self.chapter)
		self.image= image
		self.info= info
		self.doc = ''
		self.Story = []
		self.toc = ''

	def myFirstPage(self,canvas, doc):
		canvas.saveState()
		canvas.setAuthor("Oluwamo Shadrach")
		canvas.setTitle(self.Title.title())		
		canvas.setFont('Times-Bold',16)
		canvas.drawCentredString(self.PAGE_WIDTH/2.0, self.PAGE_HEIGHT-60, self.Title)
        # 
		if self.image:
			canvas.drawImage(self.image, inch,inch,width=6*inch, height=9*inch, mask=None)
        # canvas.setFont('Times-Bold',14)
        # canvas.drawString(self.PAGE_WIDTH/2.0, self.PAGE_HEIGHT-90, self.info)
		canvas.setFont('Times-Roman',9)
		canvas.drawString(inch, 0.75 * inch, "First Page / %s" % self.pageinfo)
		canvas.restoreState()

	def myLaterPages(self, canvas, doc):
		canvas.saveState()
		canvas.setFont('Times-Roman',9)
		canvas.drawString(inch, 0.75 * inch, "Page %d %s" % (doc.page, self.pageinfo))
		canvas.restoreState()

	def add_paras(self, Story, toc, text, chaptername=''):
		pass
	
	def add_para(self, text, chaptername=''):
		self.chaptername= chaptername
		
		self.bogustext = text
		H2 = ParagraphStyle(alignment=TA_CENTER, fontSize=24, name='TOCHeading2',leftIndent=0, firstLineIndent=0, spaceAfter=15, leading=26)
		NOR = ParagraphStyle(fontName='Times-Bold', alignment=TA_JUSTIFY, fontSize=13, name='para1',leftIndent=0, firstLineIndent=30, spaceBefore=2, leading=16)
		
		self.Story.append(PageBreak())
		# toc.addEntry()
		
		
		# self.Story.append(PageBreak())
		# print(style2)
		if self.chaptername:
			chapter = Paragraph(self.chaptername, H2)
			self.Story.append(chapter)
		# toc.addEntry(1, "chapter "+str(i),i,key=1)
		p = Paragraph(str(self.bogustext), NOR)
		# Story.append(Spacer(1,0.2*inch))
		self.Story.append(p)
		self.Story.append(Spacer(1,0.2*inch))
		

	def create(self):
		
		self.doc = SimpleDocTemplate(str(self.Title.replace(' ', '_')+'.pdf'))
		self.toc = TableOfContents()
		PS = ParagraphStyle
		self.toc.levelstyles = [PS(fontName='Times-Bold', fontSize=14, name='Heading1',leftIndent=20, firstLineIndent=-20, spaceBefore=5, leading=16),PS(fontSize=18, name='Heading2',leftIndent=40, firstLineIndent=-20, spaceBefore=0, leading=12),]	
		self.toc.addEntry(1, "chapter "+str(1),1,key=7)
		self.Story.append(self.toc)
		self.Story = [Spacer(1,2*inch)]

		

	def saved(self):
		self.doc.build(self.Story, onFirstPage = self.myFirstPage, onLaterPages = self.myLaterPages)

Title="Renegade Era"

def get_image_url(img_url, locate=False):
	BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
	location = os.path.join(BASE_DIR, "image/")

	image_dir = 'image/'+Title.replace(" ","_")+'.png'


	# # image, headers = urlretrieve('{}{}'.format(self.host_url,img_url), location+self.ebookinfo['title'].replace(" ","_")+'.png')
	# if image:
	# 	print('isimage: ',image)

	if locate:
		print('image url', str(location+Title.replace(" ","_")+'.png'))
		return location+Title.replace(" ","_")+'.png'
	else:
		return image_dir
	
		# else:
		# 	return img_url


# d = requests.get("http://www.wuxiaworld.co/Gambit-of-the-Living-Weapon/")

u = bs(html, 'html.parser')

get_content = u.body.find(class_="con_top").get_text(strip=True).split('>')[1]

# image_url =get_image_url(Title,locate=True)

# pdf = saveToPdf(title=Title, image=image_url)
# pdf.create()
# info ='description <br/>'*6
# pdf.add_para(info)

# for x in range(3):
# 	pdf.add_para(get_content.prettify(formatter='minimal'), chaptername='chapter_'+str(x))

# pdf.saved()
	
	


print('content', get_content)


