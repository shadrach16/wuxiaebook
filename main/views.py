from django.shortcuts import render
from django.views import View

from django.urls import reverse
from main.models import ebook
from django.core.exceptions import ObjectDoesNotExist
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger, InvalidPage
import random
from django.db.models import Q
from django.http import HttpResponse
from create_pdf.signals import createPdf


# Create your views here.


class mainView(View):

    def get(self,request):

        get_data=ebook.objects.order_by('views')
        filter = set(list(map(lambda x:x.genre, get_data))[0:8])
        data =self.getPagination(request, get='page', objectData=list(get_data),count=12)
        recommendation = ebook.objects.order_by('-views')[0:6]

        return render(request, 'main.html', {'filter':filter,'data':data, 'reco':recommendation})

    def getPagination(self, request, **kwarg):
        getPage = request.GET.get(kwarg['get'], 1)
        try:
            pageData = Paginator(list(kwarg['objectData']), kwarg['count'])
            page = pageData.page(getPage)
        except EmptyPage:
            page = pageData.page(1)
        except InvalidPage:
            page = pageData.page(1)
        except PageNotAnInteger:
            page = pageData.page(1)
            
        return page


	# get items will be used for method needing the model
	

	# @method_decorator(login_required(login_url = "auth/login/"))




# class filterView(View):
# 	def get(self, request,filter):
# 		self.filter = filter
# 		self.location = mainView().get_user_location(request.user)

# 		data = swapitem.objects.filter(
# 			filtertype = self.filter).order_by('-id')
# 		locn_sort_data = data.filter(
# 			user__address__icontains=self.location)

# 		return render(request, 'main.html', {'filter':self.filter.title(),
# 			'user':request.user, 'data':set(locn_sort_data).union(set(data)), 'reco':mainView().getReco()})


class searchView(View):

	def searchObjects(self, search, request = None):
		searchWords = search.split()
		data = []

		
		for x in searchWords:
			
			item = ebook.objects.filter(Q(title__icontains=x)|Q(
				genretype__icontains=x)|Q(
				genre__icontains=x
				))

			
			data+=list(item)

		# print('dataLen', len(data))
		return list(set(data))
	
	# def dummySearch(self, search, filter, request=None):
	# 	searchWords = 'all'

	# 	data = []
	# 	for x in searchWords:
	# 		item = swapitem.objects.filter(Q(
	# 			filtertype__icontains=filter if not 'all' else '')|Q(
	# 			filtertype__icontains='shad'))
	# 		data.append(item)

	# 	return data
	def getPagination(self, request, **kwarg):
		getPage = kwarg['get']
		try:
			pageData = Paginator(kwarg['objectData'], kwarg['count'])
			page = pageData.page(getPage)
		except EmptyPage:
			page = pageData.page(1)
		except InvalidPage:
			page = pageData.page(1)
		except PageNotAnInteger:
			page = pageData.page(1)

		# print('pages ', pageData.page_range)
            
		return page

	def get(self, request, filter=''):
		getPage = request.GET.get('page', 1)
		search = request.GET.get('search','')

		self.search = search if search else filter
		
		mydata = self.searchObjects(self.search, request = request)

		# Page = request.GET.get('page', 1)

		# data =self.getPagination(request, get=Page, objectData=mydata,count=2)
		# data = self.dummySearch(self.search, self.filter,request=request)

		# print('data value', mydata)
		get_data = ebook.objects.order_by('-views')
		
		
		try:
			pageData = Paginator(mydata, 12)
			page = pageData.page(getPage)
		except EmptyPage:
			page = pageData.page(1)
		except InvalidPage:
			page = pageData.page(1)
		except PageNotAnInteger:
			page = pageData.page(1)

		# data1 = self.getPagination(request, get='page', objectData=list(data), count=12)
		filter = set(list(map(lambda x:x.genre, get_data))[0:8])
		recommendation = ebook.objects.order_by('-views')[0:6]

		return render(request, 'search.html', {
			'data':page, 
			'reco':recommendation, 'search':self.search,
			'filter':filter, 'len':len(mydata) })


class ebookView(View):
	def get(self,request,pk):
		getData = ebook.objects.get(pk__exact=pk)
		getData.views += 1
		getData.save()
		
		createPdf.send(sender=self.__class__, name="Daniel")

		get_data = ebook.objects.order_by('-views')
		filter = set(list(map(lambda x:x.genre, get_data))[0:8])
		recommendation = ebook.objects.order_by('-views')[0:6]
		
		return render(request, 'ebookView.html', {'filter':filter,'data':getData, 'reco':recommendation})

		
